Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Permissions

' general Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Drawing Chart Library")> 
<Assembly: AssemblyDescription("Drawing Chart Library")> 
<Assembly: AssemblyProduct("Drawing.Chart.Library.2014")> 
<Assembly: CLSCompliant(True)> 

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: ComVisible(False)> 

' The following gUID is for the ID of the type library if this project is exposed to COM
<Assembly: guid("74230B96-9BD3-4575-8001-56DA039EB0D6")> 
