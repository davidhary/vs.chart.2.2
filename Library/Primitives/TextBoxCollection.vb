''' <summary>Contains a list of <see cref="TextBox"/> objects to display on the graph.</summary>
''' <remarks>
''' Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the constructor.
''' </remarks>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public NotInheritable Class TextBoxCollection
  Inherits System.Collections.ObjectModel.Collection(Of TextBox)
  Implements ICloneable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>Default constructor for the <see cref="TextBoxCollection"/> collection class.</summary>
  ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
  Public Sub New(ByVal drawingPane As Pane)
    MyBase.new()
    If drawingPane Is Nothing Then
      Throw New ArgumentNullException("drawingPane")
    End If
    Me._pane = drawingPane
  End Sub

  ''' <summary>The Copy Constructor</summary>
  ''' <param name="model">The TextBoxCollection object from which to copy</param>
  Public Sub New(ByVal model As TextBoxCollection)
    MyBase.new()
    If model Is Nothing Then
      Throw New ArgumentNullException("model")
    End If
    Dim item As isr.Drawing.TextBox
    For Each item In model
      Me.Add(New isr.Drawing.TextBox(item))
    Next item
    Me._pane = model._pane
  End Sub

#End Region

#Region " CUSTOM COLLECTION METHODS "

  ''' <summary>Deep-copy clone routine</summary>
  ''' <returns>A new, independent copy of the TextBoxCollection</returns>
  Public Function Clone() As Object Implements ICloneable.Clone
    Return Me.Copy()
  End Function

  ''' <summary>Deep-copy clone routine</summary>
  ''' <returns>A new, independent copy of the TextBoxCollection</returns>
  Public Function Copy() As TextBoxCollection
    Return New TextBoxCollection(Me)
  End Function

  ''' <summary>Renders all the <see cref="TextBox">Test Boxes</see> to the
  ''' specified <see cref="graphics"/> device.</summary>
  ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
  '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
  Public Sub Draw(ByVal graphicsDevice As Graphics)

    ' validate argument.
    If graphicsDevice Is Nothing Then
      Throw New ArgumentNullException("graphicsDevice")
    End If

    ' Clip everything to the panel area
    graphicsDevice.SetClip(Me._pane.PaneArea)

    ' Loop for each curve
    For Each textBox As TextBox In Me
      textBox.Draw(graphicsDevice)
    Next textBox

  End Sub

  ''' <summary>Gets or sets reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></summary>
  Private _pane As Pane

#End Region

End Class

