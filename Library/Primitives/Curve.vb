''' <summary>Contains the data and methods for an individual curves within
'''   a graph pane.  It carries the settings for the curve including colors, symbols and sizes, 
'''   line types, etc.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x"> Created </history>
''' <history date="05/28/04" by="David" revision="1.0.1609.x"> Add time series point tag </history>
''' <history date="06/02/04" by="David" revision="1.0.1614.x"> Add time series book marks </history>
Public Class Curve

    Implements ICloneable, IDisposable

#Region " SHARED "

    ''' <summary>Draw a series of points with symbols.</summary>
    ''' <param name="graphicsDevice">The graphics context.</param>
    ''' <param name="graphBrush">graph brush.</param>
    ''' <param name="graphPen">graph pen.</param>
    ''' <param name="dataSpace">The data space rectangle.</param>
    ''' <param name="screenSpace">The screen space rectangle.</param>
    ''' <param name="graphScale">The scale conversion 'point', specifying the 
    '''   horizontal (X) and vertical (Y) conversion scales from the data to the screen 
    '''   space</param>
    ''' <param name="dataPoints">An array of data points which to draw.</param>
    ''' <param name="PointSize">Defines the size of the symbol.</param>
    ''' <remarks>The default symbol is a rectangle</remarks>
    Public Shared Sub DrawPoints(ByVal graphicsDevice As Graphics, ByVal graphBrush As Brush, ByVal graphPen As Pen,
                                 ByVal dataSpace As RectangleF, ByVal screenSpace As Rectangle,
                                 ByVal graphScale As PointF,
                                 ByVal dataPoints() As PointF, ByVal pointSize As Size)

        ' validate arguments.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If
        If dataPoints Is Nothing Then
            Throw New ArgumentNullException("dataPoints")
        End If
        If graphBrush Is Nothing Then
            Throw New ArgumentNullException("graphBrush")
        End If

        Dim screenSymbol As Rectangle = New Rectangle(0, 0, 0, 0)
        screenSymbol.Size = pointSize

        ' Draw the points.
        Dim pt As Integer
        Dim xOffset As Single = screenSpace.X - Convert.ToSingle(pointSize.Width) / 2
        Dim yOffset As Single = screenSpace.Y - Convert.ToSingle(pointSize.Height) / 2
        For pt = 0 To dataPoints.GetUpperBound(0)
            screenSymbol.X = Convert.ToInt32((dataPoints(pt).X - dataSpace.X) * graphScale.X + xOffset)
            screenSymbol.Y = Convert.ToInt32((dataPoints(pt).Y - dataSpace.Y) * graphScale.Y + yOffset)
            graphicsDevice.FillRectangle(graphBrush, screenSymbol)
            graphicsDevice.DrawRectangle(graphPen, screenSymbol)
        Next pt

    End Sub

    ''' <summary>Draws a series of zero-based rectangles.</summary>
    ''' <param name="graphicsDevice">The graphics context.</param>
    ''' <param name="graphBrush">graph brush.</param>
    ''' <param name="graphPen">graph pen.</param>
    ''' <param name="dataSpace">The data space rectangle.</param>
    ''' <param name="screenSpace">The screen space rectangle.</param>
    ''' <param name="graphScale">The scale conversion 'point', specifying the 
    '''   horizontal (X) and vertical (Y) conversion scales from the data to the screen 
    '''   space</param>
    ''' <param name="dataPoints">An array of data points which to draw.</param>
    ''' <remarks>Use this method to draw a series of rectangles.</remarks>
    Public Shared Sub DrawRectangles(ByVal graphicsDevice As Graphics, ByVal graphBrush As Brush, ByVal graphPen As Pen,
                                     ByVal dataSpace As RectangleF, ByVal screenSpace As Rectangle,
                                     ByVal graphScale As PointF, ByVal dataPoints() As PointF)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If
        If dataPoints Is Nothing Then
            Throw New ArgumentNullException("dataPoints")
        End If
        If graphBrush Is Nothing Then
            Throw New ArgumentNullException("graphBrush")
        End If

        ' Make the transformed rectangles.
        Dim numRectangles As Integer
        numRectangles = dataPoints.GetUpperBound(0) - 1
        Dim rectangles(numRectangles) As RectangleF

        Dim pt As Integer
        For pt = 0 To numRectangles
            With rectangles(pt)
                .X = (dataPoints(pt).X - dataSpace.X) * graphScale.X + screenSpace.X
                '       .Y = (0 - dataSpace.Y) * graphScale.Y + screenSpace.Y
                .Y = screenSpace.Y - dataSpace.Y * graphScale.Y
                .Width = (dataPoints(pt + 1).X - dataPoints(pt).X) * graphScale.X
                .Height = dataPoints(pt).Y * graphScale.Y
                If .Width < 0 Then
                    .X = .X + .Width
                    .Width = -.Width
                End If
                If .Height < 0 Then
                    .Y = .Y + .Height
                    .Height = -.Height
                End If
            End With
        Next pt

        ' Draw the rectangles.
        graphicsDevice.FillRectangles(graphBrush, rectangles)
        graphicsDevice.DrawRectangles(graphPen, rectangles)

    End Sub

    ''' <summary>Draws a line segment between two points in the data coordinate space.</summary>
    ''' <param name="graphicsDevice">The graphics context.</param>
    ''' <param name="graphPen">graph pen.</param>
    ''' <param name="dataSpace">The data space rectangle.</param>
    ''' <param name="screenSpace">The screen space rectangle.</param>
    ''' <param name="graphScale">The scale conversion 'point', specifying the 
    '''   horizontal (X) and vertical (Y) conversion scales from the data to the screen 
    '''   space</param>
    ''' <param name="segmentOrigin">The origin point of the segment.</param>
    ''' <param name="segmentTermination">The termination point of the segment.</param>
    ''' <remarks>Draw a segment in the data coordinate space..</remarks>
    Public Shared Sub DrawSegment(ByVal graphicsDevice As Graphics, ByVal graphPen As Pen,
                                  ByVal dataSpace As RectangleF, ByVal screenSpace As Rectangle,
                                  ByVal graphScale As PointF, ByVal segmentOrigin As PointF, ByVal segmentTermination As PointF)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        Dim sgmtOrigin As PointF = New PointF(0, 0)
        Dim sgmtTermination As PointF = New PointF(0, 0)
        sgmtOrigin.X = (segmentOrigin.X - dataSpace.X) * graphScale.X + screenSpace.X
        sgmtOrigin.Y = (segmentOrigin.Y - dataSpace.Y) * graphScale.Y + screenSpace.Y
        sgmtTermination.X = (segmentTermination.X - dataSpace.X) * graphScale.X + screenSpace.X
        sgmtTermination.Y = (segmentTermination.Y - dataSpace.Y) * graphScale.Y + screenSpace.Y
        graphicsDevice.DrawLine(graphPen, sgmtOrigin, sgmtTermination)
    End Sub

    ''' <summary>Draws a series of connected segments.</summary>
    ''' <param name="graphicsDevice">The graphics context.</param>
    ''' <param name="graphPen">graph pen.</param>
    ''' <param name="dataSpace">The data space rectangle.</param>
    ''' <param name="screenSpace">The screen space rectangle.</param>
    ''' <param name="graphScale">The scale conversion 'point', specifying the 
    '''   horizontal (X) and vertical (Y) conversion scales from the data to the screen 
    '''   space</param>
    ''' <param name="dataPoints">An array of data points which to draw.</param>
    ''' <remarks>Draw a series of connected points.</remarks>
    Public Shared Sub DrawSegments(ByVal graphicsDevice As Graphics, ByVal graphPen As Pen,
                                   ByVal dataSpace As RectangleF, ByVal screenSpace As Rectangle,
                                   ByVal graphScale As PointF, ByVal dataPoints() As PointF)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If
        If dataPoints Is Nothing Then
            Throw New ArgumentNullException("dataPoints")
        End If

        ' Transform the points.
        Dim numPoints As Integer
        numPoints = dataPoints.GetUpperBound(0)

        Dim screenPoints() As PointF
        ReDim screenPoints(numPoints)
        Dim pt As Integer

        For pt = 0 To numPoints
            screenPoints(pt).X = 
                (dataPoints(pt).X - dataSpace.X) * graphScale.X + screenSpace.X
            screenPoints(pt).Y = 
                (dataPoints(pt).Y - dataSpace.Y) * graphScale.Y + screenSpace.Y
        Next pt

        ' Draw the transformed points.
        graphicsDevice.DrawLines(graphPen, screenPoints)

    End Sub

    ''' <summary>Returns the data space rectangle.</summary>
    ''' <param name="xMin">The minimum horizontal data space.</param>
    ''' <param name="xMax">The maximum horizontal data space.</param>
    ''' <param name="yMin">The minimum vertical coordinate space.</param>
    ''' <param name="yMax">The maximum vertical coordinate space.</param>
    ''' <remarks>get the standard data space for drawing.  Unlike the original data space, the standard data space for graphics is referenced at the top left (xMin,yMax) corresponding to the screen space origin.  Consequently, a typical data space will have a negative height.</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Shared Function GetDataSpace(ByVal xMin As Single, ByVal xMax As Single,
                                        ByVal yMin As Single, ByVal yMax As Single) As RectangleF
        Return New RectangleF(xMin, yMax, xMax - xMin, yMin - yMax)
    End Function

    ''' <summary>Compute the graph scale coefficients.</summary>
    ''' <param name="dataSpace">The data space rectangle.</param>
    ''' <param name="screenSpace">The screen space rectangle.</param>
    ''' <remarks>The graph scale coefficients convert from data to screen space 
    '''   coordinates.  The data space coordinates are set for the standard graphing 
    '''   with a top left corner at (xMin,yMax).</remarks>
    Public Shared Function GetGraphScale( 
          ByVal dataSpace As RectangleF, ByVal screenSpace As Rectangle) As PointF

        Dim graphScale As PointF = New PointF(0, 0)
        If dataSpace.Width <> 0 Then
            graphScale.X = screenSpace.Width / dataSpace.Width
        End If
        If dataSpace.Height <> 0 Then
            ' note that we have inverted the data space by placing Y max at the 
            ' top left corner.  Consequently, the data space height needs to be treated 
            ' as negative
            graphScale.Y = screenSpace.Height / dataSpace.Height
        End If
        Return graphScale

    End Function

    ''' <summary>Returns the rectangle of screen coordinates (pixels) allotted for the graph</summary>
    ''' <param name="screenSpace">The screen space rectangle.</param>
    ''' <param name="graphMargins">The screen space rectangle.</param>
    ''' <remarks>The margins x, y elements specify the top left offsets of the graph space from 
    '''   the chart control border.  The width and height accommodates the combined 
    '''   margins (left+right) and (top+bottom).  The graph margin provides an effective
    '''   way of setting the graph space independent of the control size.</remarks>
    Public Shared Function GetGraphSpace( 
        ByVal screenSpace As Rectangle, ByVal graphMargins As Rectangle) As Rectangle
        Dim graphSpace As Rectangle = screenSpace
        graphSpace.Inflate(New Size(-graphMargins.Width, -graphMargins.Height))
        graphSpace.Location = graphMargins.Location
        Return graphSpace
    End Function

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a <see cref="Curve"/> without specifying the X and Y 
    '''   data sets.  This permits setting a curve for time series update-able data.</summary>
    ''' <param name="type">A <see cref="Drawing.CurveType">Curve Type</see> value.</param>
    ''' <param name="label">A <see cref="System.String">String</see> label (legend entry) for this curve</param>
    ''' <param name="xAxis">Reference to the X <see cref="Axis"/></param>
    ''' <param name="yAxis">Reference to the Y <see cref="Axis"/></param>
    ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub New(ByVal type As CurveType, ByVal label As String, ByVal xAxis As Axis, ByVal yAxis As Axis, ByVal drawingPane As Pane)

        MyBase.new()

        If String.IsNullOrWhiteSpace(label) Then
            label = String.Empty
        End If
        If xAxis Is Nothing Then
            Throw New ArgumentNullException("xAxis")
        End If
        If yAxis Is Nothing Then
            Throw New ArgumentNullException("yAxis")
        End If
        If drawingPane Is Nothing Then
            Throw New ArgumentNullException("drawingPane")
        End If
        Me._timeSeriesLength = 1200
        Me._pane = drawingPane
        Me._cord = New Cord
        With CurveDefaults.[Get]
            Me._IgnoreMissing = .IgnoreMissing
        End With
        Me._symbol = New Symbol(Me._pane)
        Me._label = label
        Me._xAxis = xAxis
        Me._yAxis = yAxis
        ' superfluous per Code Analysis: Me._x = Nothing
        ' superfluous per Code Analysis: Me._y = Nothing
        Me.CurveType = type
        If Me.CurveType = CurveType.StripChart Then
            ' instantiate a stylus
            Me._stylus = New Stylus(Me._pane)
            Me._stylus.LineColor = Me._cord.LineColor
            Me._timeSeriesBookmarks = New TimeSeriesBookmarkCollection(Me)
            Me.ClearTimeSeries()
        End If

    End Sub

    ''' <summary>Constructs a <see cref="Curve"/> with given and default values as defined in the 
    '''   <see cref="CurveDefaults"/> class.</summary>
    ''' <param name="label">A <see cref="System.String">String</see> label (legend entry) for this curve</param>
    ''' <param name="x">A array of <see cref="System.Double">Double Precision</see> values that define
    '''   the independent (X axis) values for this curve</param>
    ''' <param name="y">A array of <see cref="System.Double">Double Precision</see> values that define
    ''' the dependent (Y axis) values for this curve</param>
    ''' <param name="xAxis">Reference to the X <see cref="Axis"/></param>
    ''' <param name="yAxis">Reference to the Y <see cref="Axis"/></param>
    ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub New(ByVal label As String, ByVal x() As Double, ByVal y() As Double,
                   ByVal xAxis As Axis, ByVal yAxis As Axis, ByVal drawingPane As Pane)

        Me.new(CurveType.XY, label, xAxis, yAxis, drawingPane)
        Me._x = x
        Me._y = y

    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The Curve object from which to copy</param>
    Public Sub New(ByVal model As Curve)

        MyBase.new()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Me._symbol = model._symbol.Copy()
        Me._cord = model._cord.Copy()
        Me._IgnoreMissing = model._IgnoreMissing
        Me._Label = model._Label
        Me._xAxis = model._xAxis
        Me._yAxis = model._yAxis
        Me._CurveType = model._CurveType
        Me._useTimeSeriesTag = model._useTimeSeriesTag
        Me._useBookmark = model._useBookmark
        Me._timeSeriesBookmark = model._timeSeriesBookmark
        If model._stylus IsNot Nothing Then
            Me._stylus = model._stylus.Copy
        End If
        If model._x IsNot Nothing Then
            Me._x = CType(model._x.Clone(), Double())
        End If
        If model._y IsNot Nothing Then
            Me._y = CType(model._y.Clone(), Double())
        End If
        Me._pane = model._pane
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _disposed As Boolean
    ''' <summary>Gets or sets (private) the dispose status sentinel.</summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._disposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._x = Nothing
                    Me._y = Nothing
                    Me._xAxis = Nothing
                    Me._yAxis = Nothing
                    Me._timeSeriesBookmarks = Nothing
                    Me._timeSeriesBookmark = Nothing

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Adds a new time series book mark. </summary>
    ''' <param name="bookmark"> The new <see cref="isr.Drawing.TimeSeriesBookMark">time series book
    ''' mark</see> </param>
    ''' <returns> The added <see cref="isr.Drawing.TimeSeriesBookMark">time series book mark</see> with
    ''' its serial number set to the index location in the collection. </returns>
    Public Function AddBookmark(ByVal bookmark As isr.Drawing.TimeSeriesBookmark) As TimeSeriesBookmark
        bookmark.SerialNumber = Me._timeSeriesBookmarks.Count
        Me._timeSeriesBookmarks.Add(bookmark)
        Return bookmark
    End Function

    ''' <summary> Adds a new time series points and shifts the time series axis and origin of time
    ''' series tick marks. </summary>
    ''' <param name="timeSeriesPoint"> The new <see cref="isr.Drawing.TimeSeriesPointR">time series
    ''' data point</see> </param>
    ''' <returns> The new <see cref="isr.Drawing.TimeSeriesPointR">time series data point</see> </returns>
    Private Function AddDataPoint(ByVal timeSeriesPoint As isr.Drawing.TimeSeriesPointR) As isr.Drawing.TimeSeriesPointR

        ' update the time series saved point
        Me.UpdateDataPoint(timeSeriesPoint)

        ' update time series.
        Me._timeSeries(Me._timeSeriesPointer) = Me._timeSeriesPoint

        ' increment and roll over the pointer
        Me._timeSeriesPointer = (Me._timeSeriesPointer + 1) Mod Me._timeSeriesLength
        Me._timeSeriesPointerKeeper = Me._timeSeriesPointer
        Me._timeSeriesCount = Math.Min(Me._timeSeriesCount + 1, Me._timeSeriesLength)

        ' increment the axis origins
        Me._XAxis.IncrementTickOrigin()

        ' return the time series point
        Return Me._timeSeriesPoint

    End Function

    ''' <summary>Updates the time series curve data and adjusts the amplitude and
    '''   time ranges.</summary>
    ''' <param name="time">The time (X or horizontal axis) value.</param>
    ''' <param name="amplitude">The amplitude (Y, or vertical axis) value.</param>
    ''' <param name="tag">Specifies the <see cref="TimeSeriesPointTags">time series tag</see></param>
    ''' <remarks>Use this method to add a new value to the strip chart.  Shifts 
    '''   the buffer one notch every time a new data points comes in thus 'scrolling' the
    '''   data along.</remarks>
    ''' <returns>The new <see cref="isr.Drawing.TimeSeriesPointR">time series data point</see></returns>
    Public Function AddDataPoint(ByVal [time] As Date, ByVal amplitude As Double, ByVal tag As TimeSeriesPointTags) As isr.Drawing.TimeSeriesPointR

        ' add the time series point and 'shift' time series axis and tick
        ' positions
        Me.AddDataPoint(New TimeSeriesPointR([time], amplitude, tag))

        If Me._YAxis.Max.AutoScale OrElse Me._YAxis.Min.AutoScale Then

            ' rescale the y axis.  We assume at this point that we have only a single
            ' strip chart.  Later on, we need to call the Curves.Rescale to rescale
            ' all their axes.
            'Dim range As isr.Drawing.RangeR = Me.getTimeSeriesAmplitudeRange()
            '_dataRange = PlanarRangeR.Empty
            Me._dataRange = New PlanarRangeR(isr.Drawing.RangeR.Empty, Me.getTimeSeriesAmplitudeRange())

            Me._YAxis.Rescale(Me._dataRange.Y.Min, Me._dataRange.Y.Max)

        End If

        ' set the time axis major and minor tick positions
        ' this can only be done when drawing!
        ' Me._xAxis.SetTimeSeriesTicks(Me._timeSeriesPointer, Me._timeSeries)

        ' return the time series point
        Return Me._timeSeriesPoint

    End Function

    ''' <summary>Updates the time series curve data and adjusts the amplitude and
    '''   time ranges.</summary>
    ''' <param name="time">The time (X or horizontal axis) value.</param>
    ''' <param name="amplitude">The amplitude (Y, or vertical axis) value.</param>
    ''' <remarks>Use this method to add a new value to the strip chart.  Shifts 
    '''   the buffer one notch every time a new data points comes in thus 'scrolling' the
    '''   data along.</remarks>
    ''' <returns>The new <see cref="isr.Drawing.TimeSeriesPointR">time series data point</see></returns>
    Public Function AddDataPoint(ByVal [time] As Date, ByVal amplitude As Double) As isr.Drawing.TimeSeriesPointR

        ' add the time series point and 'shift' time series axis and tick
        ' positions
        Me.AddDataPoint(New TimeSeriesPointR([time], amplitude, TimeSeriesPointTags.None))

        If Me._YAxis.Max.AutoScale OrElse Me._YAxis.Min.AutoScale Then

            ' rescale the y axis.  We assume at this point that we have only a single
            ' strip chart.  Later on, we need to call the Curves.Rescale to rescale
            ' all their axes.
            Dim range As isr.Drawing.RangeR = Me.getTimeSeriesAmplitudeRange()

            Me._YAxis.Rescale(range.Min, range.Max)

        End If

        ' set the time axis major and minor tick positions
        ' this can only be done when drawing!
        ' Me._xAxis.SetTimeSeriesTicks(Me._timeSeriesPointer, Me._timeSeries)

        ' return the time series point
        Return Me._timeSeriesPoint

    End Function

    ''' <summary>Updates the time series curve data and adjusts the amplitude and
    '''   time ranges.</summary>
    ''' <param name="time">The time (X or horizontal axis) value.</param>
    ''' <param name="amplitude">The amplitude (Y, or vertical axis) value.</param>
    ''' <param name="amplitudeRange">The amplitude <see cref="isr.Drawing.RangeR">range</see>.</param>
    ''' <returns>The new <see cref="isr.Drawing.TimeSeriesPointR">time series data point</see></returns>
    Public Function AddDataPoint(ByVal [time] As Date, ByVal amplitude As Double, ByVal amplitudeRange As isr.Drawing.RangeR) As isr.Drawing.TimeSeriesPointR

        If amplitudeRange Is Nothing Then
            Throw New ArgumentNullException("amplitudeRange")
        End If

        ' add the time series point and 'shift' time series axis and tick
        ' positions
        Me.AddDataPoint(New TimeSeriesPointR([time], amplitude))

        If Me._YAxis.Max.AutoScale OrElse Me._YAxis.Min.AutoScale Then

            ' rescale the y axis.  to the given range.
            Me._YAxis.Rescale(amplitudeRange.Min, amplitudeRange.Max)

        Else

            ' set the range to the given range.
            Me._YAxis.SetRange(amplitudeRange.Min, amplitudeRange.Max)

            ' set spacing -- this will not auto scale because auto scaling is off
            Me._YAxis.Rescale(amplitudeRange.Min, amplitudeRange.Max)

        End If

        ' return the time series point
        Return Me._timeSeriesPoint

    End Function

    ''' <summary>Clears the time series.</summary>
    ''' <remarks>Use this method to re-size and clear the buffer.  The Y value is set to zero and
    '''   the X value to the current time.</remarks>
    Public Sub ClearTimeSeries()

        ' clear the time series book marks
        Me._timeSeriesBookmarks.Clear()

        ' clear the count of data points
        Me._timeSeriesCount = 0
        Me._timeSeriesPointer = 0
        Me._timeSeriesPointerKeeper = Me._timeSeriesPointer

        ' set the buffer size to match the primary screen length
        Me._timeSeriesLength = Math.Max(Screen.PrimaryScreen.WorkingArea.Width, Me._timeSeriesLength)
        Dim zeroPoint As TimeSeriesPointR = New TimeSeriesPointR(Date.Now, 0)
        Me._timeSeries = New TimeSeriesPointR(Me._timeSeriesLength - 1) {}
        For i As Integer = 0 To Me._timeSeriesLength - 1
            Me._timeSeries(i) = zeroPoint
        Next

    End Sub

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the Curve</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the Curve</returns>
    Public Function Copy() As Curve
        Return New Curve(Me)
    End Function

    ''' <summary>Renders this <see cref="Curve"/> to the specified
    '''   <see cref="graphics"/> device.  This method is normally only
    '''   called by the Draw method of the parent <see cref="isr.Drawing.CurveCollection"/>
    '''   collection object.  This version is optimized for speed and should be much
    '''   faster than the regular Draw() routine.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    Private Sub drawLineChart(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        Dim numPoints As Integer = Me.PointCount

        If numPoints <= 0 Then
            Exit Sub
        End If

        Dim axisArea As RectangleF = Me._pane.AxisArea
        ' Dim scaleFactor As Double = Me._pane.ScaleFactor

        ' set clip area
        graphicsDevice.SetClip(axisArea)

        Dim tmpX() As Single
        Dim tmpY() As Single
        ReDim tmpX(numPoints - 1)
        ReDim tmpY(numPoints - 1)

        ' Loop over each point in the curve
        For i As Integer = 0 To numPoints - 1
            If (Not Me._IgnoreMissing) AndAlso (Me._x(i) = System.Double.MaxValue OrElse
                                                Me._y(i) = System.Double.MaxValue OrElse
                                                (Me._XAxis.CoordinateScale.IsLog AndAlso Me._x(i) <= 0.0) OrElse
                                                (Me._YAxis.CoordinateScale.IsLog AndAlso Me._y(i) <= 0.0)) Then
                tmpX(i) = System.Single.MaxValue
                tmpY(i) = System.Single.MaxValue
            Else
                ' Transform the current point from user scale units to
                ' screen coordinates
                tmpX(i) = Me._XAxis.Transform(Me._x(i))
                tmpY(i) = Me._YAxis.Transform(Me._y(i))
                If False Then
                    ' use the draw area to contain the range for preventing overflows.?
                    If Not Me._pane.DrawArea.Contains(tmpX(i), tmpY(i)) Then
                        If i = 0 Then
                            tmpX(i) = axisArea.X
                            tmpY(i) = axisArea.Y
                        Else
                            tmpX(i) = tmpX(i - 1)
                            tmpY(i) = tmpY(i - 1)
                        End If
                    End If
                End If
            End If
        Next i

        Me._cord.Draw(graphicsDevice, tmpX, tmpY, Me._ignoreMissing)
        Me._symbol.Draw(graphicsDevice, tmpX, tmpY, Me._pane.ScaleFactor, Me._ignoreMissing)

    End Sub

    ''' <summary>Renders this <see cref="Curve"/> to the specified
    '''   <see cref="graphics"/> device.  This method is normally only
    '''   called by the Draw method of the parent <see cref="isr.Drawing.CurveCollection"/>
    '''   collection object.  This version is optimized for speed and should be much
    '''   faster than the regular Draw() routine.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    Private Sub drawStripChart(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        Dim axisArea As RectangleF = Me._pane.AxisArea
        Dim scaleFactor As Double = Me._pane.ScaleFactor

        Dim numPoints As Integer = Me.PointCount
        If numPoints = 0 Then
            If Me._stylus IsNot Nothing Then
                Dim pixY As Single = 0.5F * (axisArea.Top + axisArea.Bottom)
                Me._stylus.Draw(graphicsDevice, pixY, axisArea, scaleFactor)
            End If
            Return
        End If

        ' draw the stylus
        If Me._stylus IsNot Nothing Then
            Dim y As Double = Me.getClippedTimeSeriesAmplitude()
            Dim pixY As Single = Me.YAxis.Transform(y)
            Me._stylus.Draw(graphicsDevice, pixY, axisArea, scaleFactor)
        End If

        ' set clip area
        graphicsDevice.SetClip(axisArea)

        Dim tmpX() As Single
        Dim tmpY() As Single
        ReDim tmpX(numPoints - 1)
        ReDim tmpY(numPoints - 1)

        ' get the first data point to draw.
        Dim j As Integer
        If Me._useBookmark Then
            j = Me._timeSeriesBookmark.FromPoint.Index
        Else
            j = Me._timeSeriesPointer - numPoints
            If j < 0 Then
                j += Me._timeSeriesCount
            End If
        End If

        ' set initial x value
        Dim x As Single = Me._xAxis.ScreenScaleRange.Max - numPoints + 1

        ' set the values from the earliest to the latest value
        For i As Integer = 0 To numPoints - 1

            ' get the pointer to the next data point
            j = j Mod Me._timeSeriesLength

            ' set the X coordinate to map onto the axis range
            tmpX(i) = x

            ' Transform the time series amplitude from user scale units to screen coordinates
            tmpY(i) = Me._yAxis.Transform(Me._timeSeries(j).Y)

            ' get the pointer to the next data point
            j += 1

            ' increment x position
            x += 1

        Next i

        Me._cord.Draw(graphicsDevice, tmpX, tmpY, Me._ignoreMissing)
        Me._symbol.Draw(graphicsDevice, tmpX, tmpY, Me._pane.ScaleFactor, Me._ignoreMissing)

    End Sub

    ''' <summary>Renders this <see cref="Curve"/> to the specified
    '''   <see cref="graphics"/> device.  This method is normally only
    '''   called by the Draw method of the parent <see cref="isr.Drawing.CurveCollection"/>
    '''   collection object.  This version is optimized for speed and should be much
    '''   faster than the regular Draw() routine.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    Public Sub Draw(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        If CurveType.StripChart = Me.CurveType Then

            drawStripChart(graphicsDevice)

        Else

            drawLineChart(graphicsDevice)

        End If

    End Sub

    ''' <summary>Checks if the <see cref="X"/> or <see cref="Y"/> data arrays are missing
    '''   for this <see cref="Curve"/>.  If so, provide a suitable default
    '''   array using ordinal values.</summary>
    Public Sub CreateDefaultData()

        If CurveType.StripChart = Me.CurveType Then

        Else

            ' See if a default X array is required
            If Me._x Is Nothing Then
                ' if a Y array is available, just make the same number of elements
                If Me._y Is Nothing Then
                    Me._x = Me._xAxis.MakeDefaultArray()
                Else
                    Me._x = Curve.MakeDefaultArray(Me._y.Length)
                End If
            End If ' see if a default Y array is required
            If Me._y Is Nothing Then
                ' if an X array is available, just make the same number of elements
                If Me._x Is Nothing Then
                    Me._y = Me._yAxis.MakeDefaultArray()
                Else
                    Me._y = Curve.MakeDefaultArray(Me._x.Length)
                End If
            End If

        End If

    End Sub

    ''' <summary>Adjusts the <see cref="Pane.AxisArea"/> for the 
    '''   <see cref="isr.Drawing.Stylus">Stylus</see> size.</summary>
    ''' <param name="scaleFactor">The scaling factor for the chart with reference
    '''   to the chart <see cref="Pane.BaseDimension"/>.  This scaling factor is 
    '''   calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''   The scale factor is applied to fonts, symbols, etc.</param>		
    ''' <param name="axisArea">The rectangle that contains the area bounded by the axes,
    '''   in pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso></param>
    ''' <returns>Adjusted <see cref="Pane.AxisArea"/></returns>
    Friend Function getAxisArea(ByVal scaleFactor As Double, ByVal axisArea As RectangleF) As RectangleF

        ' Leave room for the stylus
        If Me._stylus IsNot Nothing Then

            ' adjust the stylus 
            Return Me._stylus.GetAxisArea(scaleFactor, axisArea)

        End If

        ' return adjusted axis rectangle
        Return axisArea

    End Function

    ''' <summary>get the range of this curve</summary>
    ''' <param name="ignoreInitial">Affects how initial zero y values are treated
    '''   for setting the range of X values.  If True, then initial zero data points are 
    '''   ignored when determining the X range.  All data after the first non-zero Y value 
    '''   are included.</param>
    Friend Function getRange(ByVal ignoreInitial As Boolean) As PlanarRangeR

        ' empty the range
        Me._dataRange = PlanarRangeR.Empty

        If CurveType.StripChart = Me.CurveType Then

            Me._dataRange.ExtendRange(New PlanarRangeR(Me._dataRange.X, getTimeSeriesAmplitudeRange()))

        Else

            ' generate default arrays of ordinal values if any data arrays are missing
            ' 1.0.2118 no way.  This just causes problems.  Me.CheckData()

            ' Call the getRange() member function for the current
            ' curve to get the min and max values
            Me._dataRange.ExtendRange(PlanarRangeR.GetRange(Me._x, Me._y, ignoreInitial))

        End If

        If Me._dataRange.Equals(PlanarRangeR.Empty) Then
            ' Use unit range if no data were available
            Return PlanarRangeR.Unity
        Else
            Return Me._dataRange
        End If

    End Function

    ''' <summary>Returns the time series amplitude clipped to the axis range.</summary>
    Private Function getClippedTimeSeriesAmplitude() As Double

        Dim y As Double = Me._timeSeriesPoint.Y
        y = Math.Max(y, Me._yAxis.Min.Value)
        y = Math.Min(y, Me._yAxis.Max.Value)
        Return y

    End Function

    ''' <summary>gets the amplitude range for the <see cref="isr.Drawing.TimeSeriesPointR">Time Series</see>
    '''   data array</summary>
    Private Function getTimeSeriesAmplitudeRange() As isr.Drawing.RangeR

        ' return the unit range if no data
        If Me._timeSeries Is Nothing Then
            Return isr.Drawing.RangeR.Unity
        End If

        Dim NumPoints As Integer
        If Me._xAxis.ScreenScaleRange Is Nothing Then
            NumPoints = Me._timeSeriesCount
        Else
            NumPoints = Convert.ToInt32(Math.Min(Me._XAxis.ScreenScaleRange.Span, Me._timeSeriesCount))
        End If
        If NumPoints = 0 Then
            Return isr.Drawing.RangeR.Unity
        End If

        ' get the first data point to draw.
        Dim j As Integer = Me._timeSeriesPointer - NumPoints
        If j < 0 Then
            j += Me._timeSeriesCount
        End If

        Dim yTemp As Double
        Dim yMin As Double
        Dim yMax As Double

        If Me._useTimeSeriesTag Then

            ' initialize arbitrary data just so that we have some range values
            Dim tempPoint As TimeSeriesPointR = Me._timeSeries(j)
            Dim hasRangePoints As Boolean = False
            yTemp = tempPoint.Y
            yMin = yTemp
            yMax = yTemp

            ' set the values from the earliest to the latest value
            For i As Integer = 0 To NumPoints - 1

                ' get the pointer to the next data point
                j = j Mod Me._timeSeriesLength

                ' Transform the time series amplitude from user scale units to screen coordinates
                tempPoint = Me._timeSeries(j)

                ' check if we already have range points
                If hasRangePoints Then
                    ' if so, check if this is a new range point
                    If tempPoint.IsRangePoint Then
                        ' if so, update the range
                        yTemp = tempPoint.Y
                        If yTemp < yMin Then
                            yMin = yTemp
                        ElseIf yTemp > yMax Then
                            yMax = yTemp
                        End If
                    End If
                Else
                    ' if no range point yet, check if this one is a range point
                    If tempPoint.IsRangePoint Then
                        ' if first range point than initialize values
                        hasRangePoints = True
                        yTemp = tempPoint.Y
                        yMin = yTemp
                        yMax = yTemp
                    End If
                End If

                ' get the pointer to the next data point
                j += 1

            Next i

        Else
            ' ignore range points. 
            ' initialize the values 
            yTemp = Me._timeSeries(j).Y
            yMin = yTemp
            yMax = yTemp

            ' set the values from the earliest to the latest value
            For i As Integer = 0 To NumPoints - 1

                ' get the pointer to the next data point
                j = j Mod Me._timeSeriesLength

                ' Transform the time series amplitude from user scale units to screen coordinates
                yTemp = Me._timeSeries(j).Y
                If yTemp < yMin Then
                    yMin = yTemp
                ElseIf yTemp > yMax Then
                    yMax = yTemp
                End If

                ' get the pointer to the next data point
                j += 1

            Next i

        End If

        Return New isr.Drawing.RangeR(yMin, yMax)

    End Function

    ''' <summary>gets the time range for the <see cref="isr.Drawing.TimeSeriesPointR">Time Series</see>
    '''   data array</summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")>
    Public Function GetTimeSeriesTimeRange() As isr.Drawing.RangeR

        ' return the unit range if no data
        If Me._timeSeries Is Nothing Then
            Return isr.Drawing.RangeR.Unity
        End If

        Dim NumPoints As Integer = Convert.ToInt32(Math.Min(Me._XAxis.ScreenScaleRange.Span, Me._timeSeriesCount))
        If NumPoints < 2 Then
            Return isr.Drawing.RangeR.Unity
        End If

        ' get the last data point to draw.
        Dim j As Integer = Me._timeSeriesPointer - 1
        If j < 0 Then
            j += Me._timeSeriesCount
        End If

        ' get the first data point to draw.
        Dim i As Integer = Me._timeSeriesPointer - NumPoints
        If i < 0 Then
            i += Me._timeSeriesCount
        End If

        ' get time scale in seconds since 1/1/0001
        Dim startTime As Double = Me._timeSeries(i).Seconds
        Dim endTime As Double = Me._timeSeries(j).Seconds
        Dim timeRange As Double = endTime - startTime

        If Me._XAxis.ScreenScaleRange.Span > Me._timeSeriesCount Then

            ' if we have not collected a full range, extrapolate the range from the
            ' given data.
            timeRange *= Me._XAxis.ScreenScaleRange.Span / Me._timeSeriesCount
            Return New isr.Drawing.RangeR(endTime - timeRange, endTime)

        Else

            ' if we have collected all data than the range is determined by
            ' the first and last data points
            Return New isr.Drawing.RangeR(startTime, endTime)

        End If

    End Function

    ''' <summary>generate a default array of ordinal values.</summary>
    ''' <param name="length">The number of values to generate.</param>
    ''' <returns>a floating point double type array of default ordinal values</returns>
    Public Shared Function MakeDefaultArray(ByVal length As Integer) As Double()

        Dim defaultArray(length) As Double
        For i As Integer = 0 To length - 1
            defaultArray(i) = i + 1.0R
        Next i
        Return defaultArray

    End Function

    ''' <summary>Pans the time series to display the onset of the book mark at
    '''   the beginning of the time series chart.  If the time series has fewer
    '''   points than the screen range, the last points of the time series will show time 
    '''   earlier than the first points reflecting the circular nature of the time 
    '''   series.</summary>
    ''' <param name="bookmark">The new <see cref="isr.Drawing.TimeSeriesBookMark">time series book mark</see></param>
    ''' <param name="indexLeftShift">The left shift to add to the pan</param>
    Public Sub PanTimeSeries(ByVal bookmark As isr.Drawing.TimeSeriesBookmark,
                             ByVal indexLeftShift As Integer)

        ' adjust the time series pointer to display the Bookmark at the beginning
        ' of the current chart span.
        Me._timeSeriesPointer = bookmark.FromPoint.Index + Me.PointCount - 1 - indexLeftShift
        ' Me._timeSeriesPointer = Bookmark.FromPoint.Index + Convert.ToInt32(Me._xAxis.ScreenScaleRange.Range) - 1 - indexLeftShift
        '    If Bookmark.FromPoint.Index < Me._timeSeriesPointerKeeper Then
        '   Me._timeSeriesPointer = Math.Min(Me._timeSeriesPointer, Me._timeSeriesPointerKeeper)
        '  End If
        If Me._timeSeriesPointer >= Me._timeSeriesLength Then
            Me._timeSeriesPointer -= Me._timeSeriesLength
        End If

    End Sub

    ''' <summary>Restores the time series time reference.  This
    '''   should be done after pan and zoom operations.</summary>
    Public Sub RestoreTimeSeries()
        Me._timeSeriesPointer = Me._timeSeriesPointerKeeper
    End Sub

    ''' <summary>Updates the time series curve data and adjusts the amplitude and
    '''   time ranges.</summary>
    ''' <remarks>Use this method to add a new value to the strip chart.  Shifts 
    '''   the buffer one notch every time a new data points comes in thus 'scrolling' the
    '''   data along.</remarks>
    Public Sub ScaleTimeSeriesAxis()

        If Me._xAxis.CoordinateScale.CoordinateScaleType = CoordinateScaleType.StripChart Then
            ' this can only be done when drawing!
            ' set major tick locations and values
            Me._xAxis.MajorTick.SetLocations(Me._xAxis)
            Me._xAxis.MajorTick.SetValues(Me._xAxis, Me._timeSeriesPointer, Me._timeSeries)
        End If

    End Sub

    ''' <summary>Sets the Cartesian X, Y arrays for the given time series indexes.</summary>
    ''' <param name="fromIndex">The starting index</param>
    ''' <param name="toIndex">The ending index</param>
    Public Sub SetCartesianTimeSeries(ByVal fromIndex As Integer, ByVal toIndex As Integer)

        If fromIndex < 0 Then
            fromIndex += Me._timeSeriesLength
        End If
        If toIndex < 0 Then
            toIndex += Me._timeSeriesLength
        End If
        Dim length As Integer = toIndex - fromIndex + 1
        If length <= 0 Then
            length += Me._timeSeriesLength
        End If
        ReDim _x(length - 1)
        ReDim _y(length - 1)
        For i As Integer = 0 To length - 1
            Me._x(i) = Me._timeSeries(fromIndex).Seconds
            Me._y(i) = Me._timeSeries(fromIndex).Y
            fromIndex += 1
            If fromIndex >= Me._timeSeriesLength Then
                fromIndex = 0
            End If
        Next i

    End Sub

    ''' <summary>Updates a new time series point without adding it to the time series.</summary>
    ''' <param name="timeSeriesPoint">The <see cref="isr.Drawing.TimeSeriesPointR">time series data point</see></param>
    ''' <returns>A <see cref="isr.Drawing.TimeSeriesPointR">time series data point</see>
    '''   updated with current time series information including the time series index</returns>
    Public Function UpdateDataPoint(ByVal timeSeriesPoint As isr.Drawing.TimeSeriesPointR) As isr.Drawing.TimeSeriesPointR

        ' update the time series saved point
        Me._timeSeriesPoint = timeSeriesPoint

        ' record the time series index
        Me._timeSeriesPoint.Index = Me._timeSeriesPointer

        ' return the time series point
        Return Me._timeSeriesPoint

    End Function

    ''' <summary>Updates the references to the X, Y data arrays.</summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub UpdateData(ByVal x() As Double, ByVal y() As Double)

        Me._x = x
        Me._y = y

        ' set the point count so that it would be ignored
        Me._pointCount = ignorePointCount

    End Sub

    ''' <summary>Updates the references to the X, Y data arrays and
    '''   sets the point count which to plot.</summary>
    ''' <param name="pointCount">The number of points to plot.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub UpdateData(ByVal x() As Double, ByVal y() As Double, ByVal pointCount As Integer)

        Me._x = x
        Me._y = y
        Me._pointCount = pointCount

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>Private field that stores a reference to the <see cref="isr.Drawing.Cord"/>
    '''   class defined for this <see cref="Curve"/>.  Use the public
    '''   property <see cref="Cord"/> to access this value.</summary>
    Private _cord As Cord

    ''' <summary>gets a reference to the <see cref="isr.Drawing.Cord"/> class defined
    '''   for this <see cref="Curve"/>.</summary>
    Public ReadOnly Property Cord() As Cord
        Get
            Return Me._cord
        End Get
    End Property

    ''' <summary>Gets or sets the missing value mode for drawing graphs.  When true, graphs are
    '''   drawing assuming all values are valid.  This is important for quick graphics.</summary>
    ''' <value>A <see cref="System.Boolean">True</see></value>
    Public Property IgnoreMissing() As Boolean

    ''' <summary>Gets or sets the condition for the curve aught to use time series tag for such methods as
    '''   fixing its range.</summary>
    Public Property UseTimeSeriesTag() As Boolean

    ''' <summary>Gets or sets the book mark drawing option</summary>
    Public Property UseBookmark() As Boolean

    ''' <summary>Determines if this <see cref="Curve"/> is assigned to the 
    ''' <see cref="AxisType.Y2"/>.</summary>
    ''' <value>True if the curve is assigned to the <see cref="isr.Drawing.AxisType.Y2"/>,
    '''   False is the curve is assigned to the <see cref="isr.Drawing.AxisType.Y"/></value>
    Public ReadOnly Property IsY2Axis() As Boolean
        Get
            Return (Me._yAxis.AxisType = AxisType.Y2)
        End Get
    End Property

    ''' <summary>A <see cref="System.String">String</see> that represents the <see cref="isr.Drawing.Legend"/>
    ''' entry for the this
    ''' <see cref="Curve"/> object</summary>
    Public Property Label() As String

    ''' <summary>Gets or sets the value telling the curve use array size when 
    '''   plotting.</summary>
    Private Const ignorePointCount As Integer = -1

    Private _pointCount As Integer = ignorePointCount
    ''' <summary>Gets or sets the number of points that define this <see cref="Curve"/>.
    '''   Returns the internal point count cache if it is non-negative.  Otherwise
    '''   the number of points in the <see cref="X"/> and <see cref="Y"/> data arrays 
    '''   is returned.</summary>
    Public ReadOnly Property PointCount() As Integer
        Get
            If CurveType.StripChart = Me.CurveType Then
                Return Convert.ToInt32(Math.Min(Me._XAxis.ScreenScaleRange.Span, Me._timeSeriesCount))
            Else
                If Me._pointCount <> ignorePointCount Then
                    Return Me._pointCount
                Else
                    If Me._x Is Nothing OrElse Me._y Is Nothing Then
                        Return 0
                    Else
                        Return Convert.ToInt32(Math.Min(Me._x.Length, Me._y.Length))
                    End If
                End If
            End If
        End Get
    End Property

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></summary>
    Private _pane As Pane

    Private _dataRange As PlanarRangeR = PlanarRangeR.Empty
    ''' <summary>Returns the data range calculated when updating strip chart data
    '''   or with <see cref="M:getRange"/>.  With time series only the amplitude
    '''   range is valid.</summary>
    ''' <value>A <see cref="isr.Drawing.PlanarRangeR">range</see> value</value>
    Public ReadOnly Property DataRange() As PlanarRangeR
        Get
            Return Me._dataRange
        End Get
    End Property

    ''' <summary>Gets or sets the one-based serial order of the curve in the group.</summary>
    ''' <value>An <see cref="System.integer">integer</see> value</value>
    Public Property SerialNumber() As Integer

    Private _stylus As Stylus
    ''' <summary>Gets or sets the strip chart <see cref="isr.Drawing.Stylus">Stylus</see></summary>
    ''' <value>A reference to the strip chart <see cref="isr.Drawing.Stylus">Stylus</see></value>
    Public ReadOnly Property Stylus() As isr.Drawing.Stylus
        Get
            Return Me._stylus
        End Get
    End Property

    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A System.String value.</value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

    Private _symbol As Symbol
    ''' <summary>gets a reference to the <see cref="isr.Drawing.Symbol"/> class defined
    ''' for this <see cref="Curve"/>.</summary>
    Public ReadOnly Property Symbol() As Symbol
        Get
            Return Me._symbol
        End Get
    End Property

    Private _timeSeries() As TimeSeriesPointR

    ''' <summary>Gets or sets the current book mark to use for drawing if 
    '''   <see cref="P:UseBookmark"/> is set.</summary>
    ''' <value>A <see cref="TimeSeriesBookmark">time series book mark</see> value</value>
    Public Property TimeSeriesBookmark() As TimeSeriesBookmark

    Private _timeSeriesBookmarks As TimeSeriesBookmarkCollection
    ''' <summary>Gets or sets the book marks collection for a time series curve.</summary>
    Public ReadOnly Property TimeSeriesBookmarks() As TimeSeriesBookmarkCollection
        Get
            Return Me._timeSeriesBookmarks
        End Get
    End Property

    ''' <summary>Gets or sets the number of points collected since the last reset</summary>
    Private _timeSeriesCount As Integer

    Private _timeSeriesLength As Integer
    ''' <summary>Gets or sets the length of the time series.  This value is set automatically to
    '''   at least the width of the active screen area.</summary>
    Public Property TimeSeriesLength() As Integer
        Get
            Return Me._timeSeriesLength
        End Get
        Set(ByVal value As Integer)
            Me._timeSeriesLength = Math.Max(Screen.PrimaryScreen.WorkingArea.Width, value)
        End Set
    End Property

    ''' <summary>Gets or sets the current time timer series pointer during pan and
    '''   zoom operations.  This value is refreshed only when resetting or adding 
    '''   time series points thus keeping the time-series point
    '''   irrespective of changes to the operational time series points.</summary>
    Private _timeSeriesPointerKeeper As Integer

    ''' <summary>Gets or sets the pointer to the first time series data point.</summary>
    Private _timeSeriesPointer As Integer

    Private _timeSeriesPoint As TimeSeriesPointR
    ''' <summary>Gets or sets the last data point added to the time series</summary>
    ''' <value>A <see cref="isr.Drawing.TimeSeriesPointR">TimeSeriesPointF</see> value</value>
    Public ReadOnly Property TimeSeriesPoint() As TimeSeriesPointR
        Get
            Return Me._timeSeriesPoint
        End Get
    End Property

    ''' <summary>Determines how this curve is drawn.</summary>
    ''' <value>A <see cref="Drawing.CurveType"/> value.</value>
    ''' <history date="10/15/07" by="David" revision="1.0.2844.x">
    ''' Rename to CurveType
    ''' </history>
    Public Property CurveType() As Drawing.CurveType

    Private _x() As Double = {}
    ''' <summary>Returns the array of independent (X Axis) values that define this
    '''   <see cref="Curve"/>. The size of this array determines the number of points 
    '''   that are plotted.  <see cref="System.Double.maxValue"/> values are 
    '''   considered "missing" values, and are not plotted.  The curve will have a break 
    '''   at these points to indicate values are missing.</summary>
    Public Function X() As Double()
        Return Me._x
    End Function

    Private _y() As Double = {}
    ''' <summary>Returns the array of dependent (Y Axis) values that define this
    '''   <see cref="Curve"/>. The size of this array determines the number of points 
    '''   that are plotted.  Note that values defined as System.Double.maxValue are 
    '''   considered "missing" values, and are not plotted.  The curve will have 
    '''   a break at these points to indicate values are missing.</summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Y")>
    Public Function Y() As Double()
        Return Me._y
    End Function

    ''' <summary>Gets or sets a reference to the horizontal <see cref="Axis"/> associated
    '''   with this curve.</summary>
    ''' <value>An <see cref="Axis"/> reference.</value>
    Public Property XAxis() As Axis

    ''' <summary>Gets or sets a reference to the vertical <see cref="Axis"/> associated
    '''   with this curve.</summary>
    ''' <value>An <see cref="Axis"/> reference.</value>
    Public Property YAxis() As Axis

#End Region

End Class

#Region " DEFAULTS "

''' <summary>A simple subclass of the <see cref="Curve"/> class that defines the
'''   default property values for the <see cref="Curve"/> class.</summary>
Public NotInheritable Class CurveDefaults

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs this class.
    ''' This constructor is private to ensure only a single instance of this class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
        Me._CurveType = isr.Drawing.CurveType.XY
        Me._IgnoreMissing = True
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared instance As CurveDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As CurveDefaults
        If CurveDefaults.instance Is Nothing Then
            SyncLock CurveDefaults.syncLocker
                CurveDefaults.instance = New CurveDefaults()
            End SyncLock
        End If
        Return CurveDefaults.instance
    End Function

#End Region

    ''' <summary>Default value for the curve type property (<see cref="Drawing.CurveType"/>).</summary>
    ''' <value>A <see cref="Drawing.CurveType"/> value.</value>
    Public Property CurveType() As isr.Drawing.CurveType

    ''' <summary>Default value for the way the curve handles missing values.</summary>
    ''' <value>A <see cref="Curve.IgnoreMissing"/> property.</value>
    Public Property IgnoreMissing() As Boolean

End Class

#End Region



