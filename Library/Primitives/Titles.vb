''' <summary>Handles specification and drawing of title.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public Class Title

    Implements ICloneable, IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Default constructor for <see cref="Pane"/> Title sets all title properties
    '''   to default values as defined in the <see cref="TitleDefaults"/> class.</summary>
    ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
    Public Sub New(ByVal drawingPane As Pane)

        MyBase.new()
        If drawingPane Is Nothing Then
            Throw New ArgumentNullException("drawingPane")
        End If

        Me._Caption = String.Empty
        With TitleDefaults.[Get]
            Me._Visible = .Visible
            Me._Appearance = New TextAppearance(.Font, .FontColor)
            Me._Appearance.Frame.Filled = .Filled
            Me._Appearance.Frame.IsOutline = .Framed
            Me._Appearance.Frame.Visible = .Framed Or .Filled
        End With

        Me._Pane = drawingPane

    End Sub

    ''' <summary>Default constructor for <see cref="Pane"/> Title sets all title properties
    '''   to default values as defined in the <see cref="titleDefaults"/> class.</summary>
    ''' <param name="title">The title string</param>
    ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
    Public Sub New(ByVal title As String, ByVal drawingPane As Pane)

        Me.new(drawingPane)
        If String.IsNullOrWhiteSpace(title) Then
            title = String.Empty
        End If
        Me._caption = title

    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The Title object from which to copy</param>
    Public Sub New(ByVal model As Title)

        MyBase.new()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Me._appearance = model._appearance.Copy()
        Me._visible = model._visible
        Me._caption = model._caption
        Me._pane = model._pane

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _disposed As Boolean
    ''' <summary>Gets or sets (private) the dispose status sentinel.</summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._disposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed = True Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me._appearance IsNot Nothing Then
                        Me._appearance.Dispose()
                        Me._appearance = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the Title</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of Title</returns>
    Public Function Copy() As Title
        Return New Title(Me)
    End Function

    ''' <summary>Renders the title to the specified <see cref="graphics"/> device.  
    '''   The text, frame, and fill options will be rendered as required.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    ''' <param name="text">A <see cref="System.String">String</see> value containing the text to be
    '''   displayed.  This can be multiple lines, separated by new line ('\n')
    '''   characters</param>
    ''' <param name="x">The X location to display the text, in screen
    '''   coordinates, relative to the horizontal (<see cref="HorizontalAlignment"/>)
    '''   alignment parameter <paramref name="alignH"/></param>
    ''' <param name="y">The Y location to display the text, in screen
    '''   coordinates, relative to the vertical (<see cref="VerticalAlignment"/>
    '''   alignment parameter <paramref name="alignV"/></param>
    ''' <param name="alignH">A <see cref="HorizontalAlignment"/> alignment value</param>
    ''' <param name="alignV">A <see cref="VerticalAlignment"/> alignment value</param>
    ''' <param name="scaleFactor">The scaling factor to be used for rendering 
    '''   objects.  This is calculated and passed down by the parent 
    '''   <see cref="Pane"/> object using the <see cref="Pane.getScaleFactor"/>
    '''   method, and is used to proportionally adjust font sizes, etc. 
    '''   according to the actual size of the graph.</param>
    ''' <remarks>Employs the transform matrix in order to position the
    '''   test at any angle while also allowing the location, or anchor point,
    '''   to be at a user-specified alignment.  A caption can be located
    '''   at a given point on the graph such that the left or center or right and top 
    '''   or middle or bottom can be the anchor point.  After thus shifting the origin
    '''   so that the anchor point will be at the (x,y), the method rotates the 
    '''   coordinate system to accommodate the text angle.  It then makes a translation 
    '''   to account for the fact that the text rendering method expects to draw the
    '''   test based on a top-center location while the user may have specified another 
    '''   alignment.</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Overridable Sub Draw(ByVal graphicsDevice As Graphics, ByVal [text] As String,
                                ByVal x As Single, ByVal y As Single,
                                ByVal alignH As HorizontalAlignment, ByVal alignV As VerticalAlignment,
                                ByVal scaleFactor As Double)

        If Not Me._visible OrElse String.IsNullOrWhiteSpace(text) Then
            Return
        End If

        ' validate arguments.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        Me._appearance.Draw(graphicsDevice, [text], x, y, alignH, alignV, scaleFactor)

    End Sub

    ''' <summary>Renders the title to the specified <see cref="graphics"/> device.  
    '''   The text, frame, and fill options will be rendered as required.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    Public Overridable Sub Draw(ByVal graphicsDevice As Graphics)

        If Not Me._visible OrElse String.IsNullOrWhiteSpace(Me._caption) Then
            Return
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        ' Clip everything to the PaneArea
        graphicsDevice.SetClip(Me._pane.PaneArea)

        Me.Draw(graphicsDevice, Me._caption,
                Convert.ToSingle(Me._pane.DrawArea.Left + Me._pane.DrawArea.Right) / 2, Me._pane.DrawArea.Top,
                HorizontalAlignment.Center, VerticalAlignment.Top, Me._pane.ScaleFactor)

    End Sub

    ''' <summary>Adjusts the <see cref="Pane.AxisArea"/> for the title size.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    ''' <param name="scaleFactor">The scaling factor for the chart with reference
    '''   to the chart <see cref="Pane.BaseDimension"/>.  This scaling factor is 
    '''   calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''   The scale factor is applied to fonts, symbols, etc.</param>		
    ''' <param name="axisArea">The rectangle that contains the area bounded by the axes,
    '''   in pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso></param>
    ''' <returns>Adjusted <see cref="Pane.AxisArea"/></returns>
    Friend Function getAxisArea(ByVal graphicsDevice As Graphics, ByVal scaleFactor As Double, ByVal axisArea As RectangleF) As RectangleF

        If Not Me._visible Then
            Return axisArea
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        ' Leave room for the pane title

        ' get scaled values for the pane gap and character height
        ' Dim halfCharHeight As Single = Me._appearance.ScaleFont(scaleFactor).Height / 2.0F
        Dim titleSize As SizeF = Me._appearance.MeasureString(graphicsDevice, Me._caption, scaleFactor)

        ' Leave room for the title height, plus a line spacing of charHeight/2
        axisArea.Y += titleSize.Height ' + halfCharHeight
        axisArea.Height -= titleSize.Height ' + halfCharHeight

        ' return adjusted axis rectangle
        Return axisArea

    End Function

    ''' <summary>get a <see cref="System.Drawing.SizeF">Size</see> structure representing the width and height
    '''   of the title caption based on the scaled font size</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    ''' <param name="scaleFactor">The scaling factor for the chart with reference
    '''   to the chart <see cref="Pane.BaseDimension"/>.  This scaling factor is 
    '''   calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''   The scale factor is applied to fonts, symbols, etc.</param>		
    ''' <returns>Scaled <see cref="System.Drawing.SizeF">Size</see> dimensions in pixels,</returns>
    Friend Function MeasureString(ByVal graphicsDevice As Graphics, ByVal scaleFactor As Double) As SizeF

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        Return Me._appearance.MeasureString(graphicsDevice, Me._caption, scaleFactor)
    End Function

    ''' <summary>Return the title caption</summary>
    ''' <returns>A <see cref="System.String"/> value</returns>
    Public Overloads Overrides Function ToString() As String

        Return Me._caption

    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>gets a reference to the <see cref="isr.Drawing.TextAppearance">TextAppearance</see> class used to 
    '''   render the title.</summary>
    ''' <value>A <see cref="isr.Drawing.TextAppearance">TextAppearance</see> instance</value>
    Public Property Appearance() As TextAppearance

    ''' <summary>Gets or sets the title text.  This text can be multiple lines, separated by new line characters. 
    '''   For <see cref="AxisTitle"/> this normally shows the basis and 
    '''   dimensions of the scale range, such as "Time, [Years]"</summary>
    ''' <value>A <see cref="System.String"/> property</value>
    Public Property Caption() As String

    ''' <summary>Determines if the value <see cref="Labels"/> will be drawn</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see></value>
    Public Property Visible() As Boolean

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></summary>
    Protected Property Pane() As Pane

    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A System.String value.</value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>A simple subclass of the <see cref="Labels"/> class that defines the
'''   default property values for the <see cref="Labels"/> class.</summary>
Public NotInheritable Class TitleDefaults
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs this class.
    ''' This constructor is private to ensure only a single instance of this class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
        Me._Visible = True
        Me._Font = New Font("Arial", 16, FontStyle.Regular Or FontStyle.Bold)
        Me._FontColor = Color.Black
        Me._Filled = False
        Me._Framed = False
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared instance As TitleDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As TitleDefaults
        If TitleDefaults.instance Is Nothing OrElse TitleDefaults.instance.IsDisposed Then
            SyncLock TitleDefaults.syncLocker
                TitleDefaults.instance = New TitleDefaults()
            End SyncLock
        End If
        Return TitleDefaults.instance
    End Function

#Region "IDisposable Support"
    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is disposed.
    ''' </summary>
    ''' <value><c>True</c> if this instance is disposed; otherwise, <c>False</c>.</value>
    Private Property IsDisposed As Boolean ' To detect redundant calls

    ''' <summary>
    ''' Releases unmanaged and - optionally - managed resources.
    ''' </summary>
    ''' <param name="disposing"><c>True</c> to release both managed and unmanaged resources; <c>False</c> to release only unmanaged resources.</param>
    Private Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    ' dispose managed state (managed objects).
                    If Me._Font IsNot Nothing Then
                        Me._Font.Dispose()
                        Me._Font = Nothing
                    End If
                End If
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

#End Region

    ''' <summary>Gets or sets the default display mode for the axis value <see cref="Labels"/>
    '''   (<see cref="Labels.Visible"/> property). True to show the labels, False 
    '''   to hide them.
    '''</summary>
    Public Property Visible() As Boolean

    ''' <summary>Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    '''   of the <see cref="Title"/> caption</summary>
    Public Property Font() As Font

    ''' <summary>Gets or sets the default font color for the <see cref="Legend"/> entries
    '''   (<see cref="TextAppearance.FontColor"/> property).</summary>
    ''' <value>A <see cref="System.Drawing.Color">Color</see></value>
    Public Property FontColor() As Color

    ''' <summary>Gets or sets the default font filled mode for the <see cref="Title"/> caption
    ''' font specification <see cref="Title.Appearance"/>
    '''   (<see cref="Frame.Filled"/> property). True
    '''   for filled, False otherwise.
    '''</summary>
    Public Property Filled() As Boolean

    ''' <summary>Gets or sets the default font framed mode for the <see cref="Title"/> caption
    ''' font specification <see cref="Title.Appearance"/>
    '''   (<see cref="Frame.IsOutline"/> property). True
    '''   for framed, False otherwise.
    '''</summary>
    Public Property Framed() As Boolean

End Class

#End Region

''' <summary>Handles specification and drawing of the axis title.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public Class AxisTitle

    Inherits Title

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Default constructor for <see cref="Axis"/> Title sets all title properties
    '''   to default values as defined in the <see cref="AxisTitleDefaults"/> class.</summary>
    ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
    Public Sub New(ByVal drawingPane As Pane)

        MyBase.new(drawingPane)

        With AxisTitleDefaults.[Get]
            MyBase.Visible = .Visible
            MyBase.Appearance = New TextAppearance(.Font, .FontColor)
            MyBase.Appearance.Frame.Filled = .Filled
            MyBase.Appearance.Frame.IsOutline = .Framed
            MyBase.Appearance.Frame.Visible = .Framed Or .Filled
            Me._IsShowMagnitude = .IsShowMagnitude
        End With

    End Sub

    ''' <summary>Default constructor for <see cref="Axis"/> Title sets all title properties
    '''   to default values as defined in the <see cref="AxisTitleDefaults"/> class.</summary>
    ''' <param name="title">The title string</param>
    ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
    Public Sub New(ByVal title As String, ByVal drawingPane As Pane)

        Me.new(drawingPane)
        If String.IsNullOrWhiteSpace(title) Then
            title = String.Empty
        End If

        MyBase.Caption = title

    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The Title object from which to copy</param>
    Public Sub New(ByVal model As AxisTitle)

        MyBase.new(model)
        If model IsNot Nothing Then
            Me._IsShowMagnitude = model._IsShowMagnitude
        End If

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the Title</returns>
    Public Shadows Function Clone() As Object
        Return Me.Copy()
    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of Title</returns>
    Public Shadows Function Copy() As AxisTitle
        Return New AxisTitle(Me)
    End Function

    ''' <summary>Renders the title to the specified <see cref="graphics"/> device.  
    '''   The text, frame, and fill options will be rendered as required.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    ''' <param name="text">A <see cref="System.String">String</see> value containing the text to be
    '''   displayed.  This can be multiple lines, separated by new line ('\n')
    '''   characters</param>
    ''' <param name="x">The X location to display the text, in screen
    '''   coordinates, relative to the horizontal (<see cref="HorizontalAlignment"/>)
    '''   alignment parameter <paramref name="alignH"/></param>
    ''' <param name="y">The Y location to display the text, in screen
    '''   coordinates, relative to the vertical (<see cref="VerticalAlignment"/>
    '''   alignment parameter <paramref name="alignV"/></param>
    ''' <param name="scaleFactor">The scaling factor to be used for rendering 
    '''   objects.  This is calculated and passed down by the parent 
    '''   <see cref="Pane"/> object using the <see cref="Pane.getScaleFactor"/>
    '''   method, and is used to proportionally adjust font sizes, etc. 
    '''   according to the actual size of the graph.</param>
    ''' <remarks>Employs the transform matrix in order to position the
    '''   test at any angle while also allowing the location, or anchor point,
    '''   to be at a user-specified alignment.  A caption can be located
    '''   at a given point on the graph such that the left or center or right and top 
    '''   or middle or bottom can be the anchor point.  After thus shifting the origin
    '''   so that the anchor point will be at the (x,y), the method rotates the 
    '''   coordinate system to accommodate the text angle.  It then makes a translation 
    '''   to account for the fact that the text rendering method expects to draw the
    '''   test based on a top-center location while the user may have specified another 
    '''   alignment.</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Overloads Sub Draw(ByVal graphicsDevice As Graphics, ByVal axis As Axis, ByVal [text] As String,
                              ByVal x As Single, ByVal y As Single, ByVal scaleFactor As Double)

        If Not MyBase.Visible OrElse String.IsNullOrWhiteSpace(text) Then
            Return
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If
        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        Dim alignV As VerticalAlignment = VerticalAlignment.Top
        If (axis.AxisType = AxisType.Y) Then
            alignV = VerticalAlignment.Bottom
        End If

        ' Draw the title
        MyBase.Draw(graphicsDevice, [text], x, y, HorizontalAlignment.Center, alignV, scaleFactor)

    End Sub

    ''' <summary>Return the title caption</summary>
    ''' <returns>A <see cref="System.String"/> value</returns>
    Public Overloads Overrides Function ToString() As String

        Return MyBase.Caption

    End Function

    ''' <summary>Returns the title caption including the <see cref="Scale.ScaleExponent"/>
    '''   if <see cref="AxisTitle.IsShowMagnitude"/></summary>
    ''' <param name="ScaleExponent">the magnitude multiplier decade for scale values.
    '''   This is used to limit the size of the displayed value labels.  For example, if the 
    '''   value is really 2000000, then the graph could instead display 2000 with a 
    '''   magnitude multiplier decade of 3 (10^3).</param>
    ''' <returns>A <see cref="System.String"/> value</returns>
    Public Overloads Function ToString(ByVal scaleExponent As Integer) As String

        If scaleExponent <> 0 AndAlso Me.IsShowMagnitude Then
            Return MyBase.Caption + String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  " (10^{0})", scaleExponent)
        Else
            Return MyBase.ToString
        End If

    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets the mode of displaying the magnitude label for large scale values.
    '''   A "magnitude" value (power of 10) is automatically used for scaling the graph.  
    '''   This magnitude value is automatically appended to the end of the <see cref="Axis"/> 
    '''   <see cref="Title"/> (e.graphicsDevice., "(10^4)") to indicate that a magnitude is in use.  
    '''   This property controls whether or not the magnitude is included in the title.  
    '''   Note that it only affects the axis title; a magnitude value may still be used 
    '''   even if it is not shown in the title.</summary>
    ''' <value>True to show the magnitude value, false to hide it</value>
    Public Property IsShowMagnitude() As Boolean

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="AxisTitle" /> class that defines the
''' default property values for the <see cref="AxisTitle" /> class.
''' </summary>
Public NotInheritable Class AxisTitleDefaults
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs this class.
    ''' This constructor is private to ensure only a single instance of this class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
        Me._Visible = True
        Me._Font = New Font("Arial", 14, FontStyle.Regular Or FontStyle.Bold)
        Me._FontColor = Color.Black
        Me._Filled = False
        Me._Framed = False
        Me._IsShowMagnitude = False
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared instance As AxisTitleDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As AxisTitleDefaults
        If AxisTitleDefaults.instance Is Nothing OrElse AxisTitleDefaults.instance.IsDisposed Then
            SyncLock AxisTitleDefaults.syncLocker
                AxisTitleDefaults.instance = New AxisTitleDefaults()
            End SyncLock
        End If
        Return AxisTitleDefaults.instance
    End Function

#Region "IDisposable Support"
    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is disposed.
    ''' </summary>
    ''' <value><c>True</c> if this instance is disposed; otherwise, <c>False</c>.</value>
    Private Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases unmanaged and - optionally - managed resources.
    ''' </summary>
    ''' <param name="disposing"><c>True</c> to release both managed and unmanaged resources; <c>False</c> to release only unmanaged resources.</param>
    Private Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    ' dispose managed state (managed objects).
                    If Me._Font IsNot Nothing Then
                        Me._Font.Dispose()
                        Me._Font = Nothing
                    End If
                End If
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region


#End Region

    ''' <summary>Gets or sets the default display mode for the axis value <see cref="Labels"/>
    '''   (<see cref="Labels.Visible"/> property). True to show the labels, False 
    '''   to hide them.
    '''</summary>
    Public Property Visible() As Boolean

    ''' <summary>Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    '''   of the <see cref="Title"/> caption</summary>
    Public Property Font() As Font

    ''' <summary>Gets or sets the default font color for the <see cref="Legend"/> entries
    '''   (<see cref="TextAppearance.FontColor"/> property).</summary>
    ''' <value>A <see cref="System.Drawing.Color">Color</see></value>
    Public Property FontColor() As Color

    ''' <summary>Gets or sets the default font filled mode for the <see cref="Title"/> caption
    ''' font specification <see cref="Title.Appearance"/>
    '''   (<see cref="Frame.Filled"/> property). True
    '''   for filled, False otherwise.
    '''</summary>
    Public Property Filled() As Boolean

    ''' <summary>Gets or sets the default font framed mode for the <see cref="Title"/> caption
    ''' font specification <see cref="Title.Appearance"/>
    '''   (<see cref="Frame.IsOutline"/> property). True
    '''   for framed, False otherwise.
    '''</summary>
    Public Property Framed() As Boolean

    ''' <summary>Gets or sets the default display mode for the title magnitude
    '''   (<see cref="AxisTitle.IsShowMagnitude"/> property). True
    '''   to show the magnitude, False to omit the magnitude.</summary>
    Public Property IsShowMagnitude() As Boolean

End Class

#End Region

