''' <summary>Provides alignment properties.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public Structure Alignment

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs an instance of this class.</summary>
    ''' <param name="horizontal">The <see cref="HorizontalAlignment"/>  setting</param>
    ''' <param name="vertical"><see cref="VerticalAlignment"/> setting</param>
    Public Sub New(ByVal horizontal As HorizontalAlignment, ByVal vertical As VerticalAlignment)

        Me._Horizontal = horizontal
        Me._vertical = vertical

    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As Alignment, ByVal right As Alignment) As Boolean
        Return Alignment.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As Alignment, ByVal right As Alignment) As Boolean
        Return Not Alignment.Equals(left, right)
    End Operator

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>. </returns>
    Public Overloads Shared Function Equals(ByVal left As Alignment, ByVal right As Alignment) As Boolean
        Return left._Horizontal.Equals(right._Horizontal) AndAlso left._Vertical.Equals(right._Vertical)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <param name="obj"> Another object to compare to. </param>
    ''' <returns> <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        If obj IsNot Nothing AndAlso TypeOf obj Is Alignment Then
            Return Equals(CType(obj, Alignment))
        Else
            Return False
        End If
    End Function

    ''' <summary>Returns True if the value of the <param>compared</param> equals to the
    '''   instance value.</summary>
    ''' <param name="compared">The <see cref="Alignment">Alignment</see> to compare for 
    ''' equality with this instance.</param>
    ''' <returns> <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>. </returns>
    ''' <remarks>Alignments are the same if the have the same 
    ''' <see cref="Horizontal"/> and <see cref="Horizontal"/> ranges.</remarks>
    Public Overloads Function Equals(ByVal compared As Alignment) As Boolean
        Return Alignment.Equals(Me, compared)
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Returns the hash code for this <see cref="Alignment"/> structure. </summary>
    ''' <returns> An <see cref="System.integer">integer</see> value. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me._Horizontal.GetHashCode() Xor Me._Vertical.GetHashCode()
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets the horizontal alignment setting</summary>
    ''' <value>A <see cref="HorizontalAlignment"/> value</value>
    Public Property Horizontal() As HorizontalAlignment

    ''' <summary>Gets or sets the vertical alignment setting</summary>
    ''' <value>A <see cref="VerticalAlignment"/> value</value>
    Public Property Vertical() As VerticalAlignment

#End Region

End Structure

