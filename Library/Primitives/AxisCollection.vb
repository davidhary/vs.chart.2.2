''' <summary>A collection class containing a list of <see cref="Axis"/> objects
'''   that define the set of Axes to be displayed on the graph.</summary>
''' <remarks>
''' Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the constructor.
''' </remarks>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public NotInheritable Class AxisCollection
    Inherits System.Collections.ObjectModel.Collection(Of Axis)
    Implements ICloneable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Default constructor for the collection class.</summary>
    ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
    Public Sub New(ByVal drawingPane As Pane)
        MyBase.new()
        If drawingPane Is Nothing Then
            Throw New ArgumentNullException("drawingPane")
        End If
        Me._pane = drawingPane
    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The Axis Collection object from which to copy</param>
    Public Sub New(ByVal model As AxisCollection)
        MyBase.new()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        For Each item As Axis In model
            Me.Add(item)
        Next item
        Me._pane = model._pane
    End Sub

#End Region

#Region " CUSTOM COLLECTION METHODS "

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the AxisCollection</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the AxisCollection</returns>
    Public Function Copy() As AxisCollection
        Return New AxisCollection(Me)
    End Function

    ''' <summary>Renders all the <see cref="Axis"/> objects in the list to the
    '''   specified <see cref="Graphics"/> device by calling the <see cref="Axis.Draw"/> 
    '''   method of each <see cref="Axis"/> object.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.Graphics"/> of the <see cref="M:Paint"/> method.</param>
    Public Sub Draw(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        ' Clip everything to the pane area
        graphicsDevice.SetClip(Me._pane.PaneArea)

        ' Loop for each axis
        For Each axis As Axis In Me

            ' Render the axis
            axis.Draw(graphicsDevice)

        Next axis

    End Sub

    ''' <summary>Adjusts the <see cref="Pane.AxisArea"/> for the axis space.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.Graphics"/> of the <see cref="M:Paint"/> method.</param>
    ''' <param name="scaleFactor">The scaling factor for the chart with reference
    '''   to the chart <see cref="Pane.BaseDimension"/>.  This scaling factor is 
    '''   calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''   The scale factor is applied to fonts, symbols, etc.</param>		
    ''' <param name="axisArea">The rectangle that contains the area bounded by the axes,
    '''   in pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso></param>
    ''' <returns>Adjusted <see cref="Pane.AxisArea"/></returns>
    Public Function GetAxisArea(ByVal graphicsDevice As Graphics, ByVal scaleFactor As Double,
                                ByVal axisArea As RectangleF) As RectangleF

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        ' calculate the margins required for the X, Y, and Y2 axes
        Dim margins As MarginsF = New MarginsF(0, 0, 0, 0)
        Dim axisSpace As SizeF

        ' Loop for each axis
        For Each axis As Axis In Me

            ' get width and height required for axis space
            axisSpace = axis.GetSpace(graphicsDevice, scaleFactor)

            ' Set margins based on the axis
            Select Case axis.AxisType
                Case AxisType.X
                    margins.Bottom = Math.Max(margins.Bottom, axisSpace.Height)
                    margins.Left = Math.Max(margins.Left, axisSpace.Width)
                    margins.Right = Math.Max(margins.Right, axisSpace.Width)
                Case AxisType.Y
                    margins.Left = Math.Max(margins.Left, axisSpace.Width)
                    margins.Bottom = Math.Max(margins.Bottom, axisSpace.Height)
                    margins.Top = Math.Max(margins.Top, axisSpace.Height)
                Case AxisType.Y2
                    margins.Right = Math.Max(margins.Right, axisSpace.Width)
                    margins.Bottom = Math.Max(margins.Bottom, axisSpace.Height)
                    margins.Top = Math.Max(margins.Top, axisSpace.Height)
                Case Else
                    Debug.Assert(Not Debugger.IsAttached, "Unhandled axis type")
            End Select
        Next axis

        ' return adjusted axis rectangle
        Return margins.GetAdjustedArea(axisArea)

    End Function

    ''' <summary>Rescale all the <see cref="Axis"/> objects in the list to the
    '''   specified <see cref="CurveCollection"/> by calling the <see cref="Axis.Rescale"/> 
    '''   method of each <see cref="Axis"/> object.</summary>
    Public Sub Rescale(ByVal curves As CurveCollection, ByVal ignoreInitial As Boolean)

        ' validate argument.
        If curves Is Nothing Then
            Throw New ArgumentNullException("curves")
        End If

        ' Get the scale range of the X-Y curves
        Dim xyRange As PlanarRangeR = curves.GetRange(ignoreInitial, False)

        ' Get the scale range of the X-Y2 curves
        Dim xy2Range As PlanarRangeR = curves.GetRange(ignoreInitial, True)

        ' set the X range to include both ranges.
        Dim xRange As isr.Drawing.RangeR = xyRange.X.ExtendRange(xy2Range.X).Copy

        ' Loop for each axis
        For Each item As Axis In Me
            ' Pick new scales based on the range
            Select Case item.AxisType
                Case AxisType.X
                    item.Rescale(xRange.Min, xRange.Max)
                Case AxisType.Y
                    item.Rescale(xyRange.Y.Min, xyRange.Y.Max)
                Case AxisType.Y2
                    item.Rescale(xy2Range.Y.Min, xy2Range.Y.Max)
                Case Else
                    Debug.Assert(Not Debugger.IsAttached, "Unhandled axis type")
            End Select
        Next item

    End Sub

    ''' <summary>Sets the screen ranges for rendering the <see cref="Axis"/>.</summary>
    ''' <param name="axisArea">The <see cref="System.Drawing.RectangleF"/> that
    '''   that contains the area bounded by the axes.</param>
    Public Sub SetScreenRange(ByVal axisArea As RectangleF)

        ' Loop for each axis
        For Each axis As Axis In Me
            ' Render the axis
            axis.SetScreenRange(axisArea)
        Next axis

    End Sub

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></summary>
    Private _pane As Pane

#End Region

End Class

