''' <summary>Defines a <see cref="System.integer">integer</see> auto value.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public Class AutoValue
    Implements ICloneable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a auto value</summary>
    ''' <param name="value">A <see cref="System.integer">integer</see> value</param>
    ''' <param name="AutoScale">The value auto settings mode</param>
    Public Sub New(ByVal [value] As Integer, ByVal autoScale As Boolean)
        MyBase.new()
        Me._value = [value]
        Me._autoScale = autoScale
    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The Arrow object from which to copy</param>
    Public Sub New(ByVal model As AutoValue)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Me._autoScale = model._autoScale
        Me._value = model._value
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As AutoValue, ByVal right As AutoValue) As Boolean
        Return AutoValue.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As AutoValue, ByVal right As AutoValue) As Boolean
        Return Not AutoValue.Equals(left, right)
    End Operator

    ''' <summary> Returns True if the value of the <param>compared</param> equals to the instance
    ''' value. </summary>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Shared Function Equals(ByVal left As AutoValue, ByVal right As AutoValue) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left._Value.Equals(right._Value) AndAlso left._AutoScale.Equals(right._AutoScale)
        End If
    End Function

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return AutoValue.Equals(Me, TryCast(obj, AutoValue))
    End Function

    ''' <summary>Returns True if the value of the <param>compared</param> equals to the
    '''   instance value.</summary>
    ''' <param name="compared">The <see cref="AutoValue">AutoValue</see> to compare for 
    ''' equality with this instance.</param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    ''' <remarks>Auto Values are the same if the have the same 
    ''' <see cref="Value"/> and <see cref="AutoScale"/> ranges.</remarks>
    Public Overloads Function Equals(ByVal compared As AutoValue) As Boolean
        If compared Is Nothing Then
            Throw New ArgumentNullException("compared")
        End If
        Return AutoValue.Equals(Me, compared)
    End Function

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the Arrow</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the Arrow</returns>
    Public Function Copy() As AutoValue
        Return New AutoValue(Me)
    End Function

    ''' <summary>Creates a unique hash code.</summary>
    ''' <returns>An <see cref="System.integer">integer</see> value</returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me._value.GetHashCode + Me._autoScale.GetHashCode
    End Function

    ''' <summary>Returns the default string representation of the range.</summary>
    ''' <returns> The default string representation of the range. </returns>
    Public Overloads Overrides Function ToString() As String
        Return Me._Value.ToString(Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary>Returns the default string representation of the range.</summary>
    ''' <param name="format">The format string</param>
    ''' <returns> The formatted string representation of the range. </returns>
    Public Overloads Function ToString(ByVal format As String) As String
        If String.IsNullOrWhiteSpace(format) Then
            Throw New ArgumentNullException("format")
        End If
        Return Me._Value.ToString(format, Globalization.CultureInfo.CurrentCulture)
    End Function

    Private _Value As Integer
    ''' <summary>Gets or sets the <see cref="AutoValue"/> value</summary>
    ''' <value>A <see cref="System.Integer">Integer</see> value</value>
    Public Property [Value]() As Integer
        Get
            Return Me._Value
        End Get
        Set(ByVal value As Integer)
            Me._Value = value
        End Set
    End Property

    ''' <summary>Gets or sets the auto setting state of this AutoValue which determines if the
    '''   value is set automatically.  This state is set to False if the Value is manually 
    '''   changed.</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see> value</value>
    Public Property AutoScale() As Boolean

#End Region

End Class

''' <summary>Defines a <see cref="System.Single">Single</see> auto value.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public Class AutoValueF
    Implements ICloneable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a auto value</summary>
    ''' <param name="value">A <see cref="System.Single">Single</see> value</param>
    ''' <param name="AutoScale">The value auto settings mode</param>
    Public Sub New(ByVal [value] As Single, ByVal autoScale As Boolean)
        MyBase.new()
        Me._value = [value]
        Me._autoScale = autoScale
    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The Arrow object from which to copy</param>
    Public Sub New(ByVal model As AutoValueF)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Me._autoScale = model._autoScale
        Me._value = model._value
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <param name="left">  Automatic value f to be compared. </param>
    ''' <param name="right"> Automatic value f to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As AutoValueF, ByVal right As AutoValueF) As Boolean
        Return AutoValueF.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <param name="left">  Automatic value f to be compared. </param>
    ''' <param name="right"> Automatic value f to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As AutoValueF, ByVal right As AutoValueF) As Boolean
        Return Not AutoValueF.Equals(left, right)
    End Operator

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="left">  Automatic value f to be compared. </param>
    ''' <param name="right"> Automatic value f to be compared. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Shared Function Equals(ByVal left As AutoValueF, ByVal right As AutoValueF) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left._value.Equals(right._value) AndAlso left._AutoScale.Equals(right._AutoScale)
        End If
    End Function

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return AutoValueF.Equals(Me, TryCast(obj, AutoValueF))
    End Function

    ''' <summary> Returns True if the value of the <param>compared</param> equals to the instance
    ''' value. </summary>
    ''' <remarks> Auto Values are the same if the have the same
    ''' <see cref="Value"/> and <see cref="AutoScale"/> ranges. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="compared"> The <see cref="AutoValueF">AutoValueF</see> to compare for equality
    ''' with this instance. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Function Equals(ByVal compared As AutoValueF) As Boolean
        If compared Is Nothing Then
            Throw New ArgumentNullException("compared")
        End If
        Return AutoValueF.Equals(Me, compared)
    End Function

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the Arrow</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the Arrow</returns>
    Public Function Copy() As AutoValueF
        Return New AutoValueF(Me)
    End Function

    ''' <summary>Creates a unique hash code.</summary>
    ''' <returns>An <see cref="System.integer">integer</see> value</returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me._value.GetHashCode + Me._autoScale.GetHashCode
    End Function

    ''' <summary> Returns the default string representation of the range. </summary>
    ''' <returns> The default string representation of the range. </returns>
    Public Overloads Overrides Function ToString() As String
        Return Me._value.ToString(Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary> Returns the default string representation of the range. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="format"> The format string. </param>
    ''' <returns> The formatted string representation of the range. </returns>
    Public Overloads Function ToString(ByVal format As String) As String
        If String.IsNullOrWhiteSpace(format) Then
            Throw New ArgumentNullException("format")
        End If
        Return Me._value.ToString(format, Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary> Returns a caption for displaying the value with additional decimal value relative to
    ''' the base value. </summary>
    ''' <param name="extraDecimalPlaces"> The extra decimal places. </param>
    ''' <returns> a caption for displaying the value with additional decimal value relative to the base
    ''' value. </returns>
    Public Function Caption(ByVal extraDecimalPlaces As Integer) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                             "{0:F" & (Me._value.DecimalPlaces + extraDecimalPlaces).ToString("D", Globalization.CultureInfo.CurrentCulture) & "}",
                             Me._value)
    End Function

    Private _value As Single
    ''' <summary>Gets or sets the <see cref="AutoValue"/> value</summary>
    ''' <value>A <see cref="System.Single">Single</see> value</value>
    Public Property [Value]() As Single
        Get
            Return Me._value
        End Get
        Set(ByVal value As Single)
            Me._value = value
        End Set
    End Property

    ''' <summary>Gets or sets the auto setting state of this AutoValue which determines if the
    '''   value is set automatically.  This state is set to False if the Value is manually 
    '''   changed.</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see> value</value>
    Public Property AutoScale() As Boolean

#End Region

End Class

''' <summary>Defines a <see cref="System.Double">Double</see> auto value.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public Class AutoValueR
    Implements ICloneable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a auto value</summary>
    ''' <param name="value">A <see cref="System.Double">Double</see> value</param>
    ''' <param name="AutoScale">The value auto settings mode</param>
    Public Sub New(ByVal [value] As Double, ByVal autoScale As Boolean)
        MyBase.new()
        Me._value = [value]
        Me._autoScale = autoScale
    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The Arrow object from which to copy</param>
    Public Sub New(ByVal model As AutoValueR)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Me._autoScale = model._autoScale
        Me._value = model._value
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As AutoValueR, ByVal right As AutoValueR) As Boolean
        Return AutoValueR.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As AutoValueR, ByVal right As AutoValueR) As Boolean
        Return Not AutoValueR.Equals(left, right)
    End Operator

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Shared Function Equals(ByVal left As AutoValueR, ByVal right As AutoValueR) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left._Value.Equals(right._Value) AndAlso left._AutoScale.Equals(right._AutoScale)
        End If
    End Function

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return AutoValueR.Equals(Me, TryCast(obj, AutoValueR))
    End Function

    ''' <summary>Returns True if the value of the <param>compared</param> equals to the
    '''   instance value.</summary>
    ''' <param name="compared">The <see cref="AutoValueR">AutoValueR</see> to compare for 
    ''' equality with this instance.</param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    ''' <remarks>Auto Values are the same if the have the same 
    ''' <see cref="Value"/> and <see cref="AutoScale"/> ranges.</remarks>
    Public Overloads Function Equals(ByVal compared As AutoValueR) As Boolean
        If compared Is Nothing Then
            Throw New ArgumentNullException("compared")
        End If
        Return AutoValueR.Equals(Me, compared)
    End Function

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the Arrow</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the Arrow</returns>
    Public Function Copy() As AutoValueR
        Return New AutoValueR(Me)
    End Function

    ''' <summary>Creates a unique hash code.</summary>
    ''' <returns>An <see cref="System.integer">integer</see> value</returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me._Value.GetHashCode + Me._AutoScale.GetHashCode
    End Function

    ''' <summary>Returns the default string representation of the range.</summary>
    ''' <returns> The default string representation of the range. </returns>
    Public Overloads Overrides Function ToString() As String
        Return Me._Value.ToString(Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary>Returns the default string representation of the range.</summary>
    ''' <param name="format">The format string</param>
    ''' <returns> The formatted string representation of the range. </returns>
    Public Overloads Function ToString(ByVal format As String) As String
        If String.IsNullOrWhiteSpace(format) Then
            Throw New ArgumentNullException("format")
        End If
        Return Me._Value.ToString(format, Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary> Returns a caption for displaying the value with additional decimal value relative to
    ''' the base value. </summary>
    ''' <param name="extraDecimalPlaces"> The extra decimal places. </param>
    ''' <returns> a caption for displaying the value with additional decimal value relative to the base
    ''' value. </returns>
    Public Function Caption(ByVal extraDecimalPlaces As Integer) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                             "{0:F" & (Me._Value.DecimalPlaces + extraDecimalPlaces).ToString("D", Globalization.CultureInfo.CurrentCulture) & "}",
                             Me._Value)
    End Function

    Private _Value As Double
    ''' <summary>Gets or sets the <see cref="AutoValue"/> value</summary>
    ''' <value>A <see cref="System.Double">Double</see> value</value>
    Public Property [Value]() As Double
        Get
            Return Me._Value
        End Get
        Set(ByVal value As Double)
            Me._Value = value
        End Set
    End Property

    ''' <summary>Gets or sets the auto setting state of this AutoValue which determines if the
    '''   value is set automatically.</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see> value</value>
    Public Property AutoScale() As Boolean

#End Region

End Class

