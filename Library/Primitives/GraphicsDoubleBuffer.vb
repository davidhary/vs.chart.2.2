''' <summary>Removes flickering from painting controls with gdi+.</summary>
''' <remarks>The class removes flickering by double buffering the drawing. It draws 
'''   on a separate bitmap canvas file. Once all drawing is complete, you can then 
'''   push the drawing to the main drawing canvas all at once. This will kill most 
'''   if not all flickering the drawing produces.
'''   The double buffer slows down the line recorder significantly without removing much 
'''   of the flickering.
''' </remarks>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="07/27/05" by="David" revision="1.0.2034.x">
''' Created
''' </history>
Public Class GraphicsDoubleBuffer

    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="canvasWidth">The canvas width.</param>
    ''' <param name="canvasHeight">The canvas height.</param>
    Public Sub New(ByVal canvasWidth As Integer, ByVal canvasHeight As Integer)
        Me._canvas = New Bitmap(canvasWidth, canvasHeight)
        Me._graphics = Graphics.FromImage(Me._canvas)
        Me._graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _disposed As Boolean
    ''' <summary>Gets or sets (private) the dispose status sentinel.</summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._disposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me._canvas IsNot Nothing Then
                        Me._canvas.Dispose()
                        Me._canvas = Nothing
                    End If

                    If Me._graphics IsNot Nothing Then
                        Me._graphics.Dispose()
                        Me._graphics = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS  and  PROPERTIES "

    ''' <summary>The bitmap where drawing is down before it is pushed to the
    '''   object.</summary>
    Public Property [Canvas]() As Bitmap

    Private _graphics As Graphics
    ''' <summary>Reference to graphics context for the buffered canvas.</summary>
    Public ReadOnly Property GraphicsDevice() As Graphics
        Get
            Return Me._graphics
        End Get
    End Property

    ''' <summary>Renders the double buffer to the screen</summary>
    ''' <param name="graphicsDevice">Window forms graphics Object</param>
    Public Sub Render(ByVal graphicsDevice As Graphics)
        If graphicsDevice IsNot Nothing AndAlso Me._canvas IsNot Nothing Then
            graphicsDevice.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None
            graphicsDevice.DrawImage(Me._canvas, 0, 0)
        End If

    End Sub

    ''' <summary>Returns true if double buffering can be achieved</summary>
    Public Function CanDoubleBuffer() As Boolean
        Return Graphics.FromImage(Me._canvas) IsNot Nothing
    End Function

#End Region

End Class

