''' <summary>Handles the rendering of text with variety of alignment options, font settings, colors, frame and fill
'''   modes, and text angles.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public Class TextAppearance
    Implements ICloneable, IDisposable

#Region " SHARED "

    ''' <summary>Returns a font style</summary>
    ''' <param name="bold"><see cref="System.Boolean"/> <c>True</c> if bold.</param>
    ''' <param name="italic"><see cref="System.Boolean"/> <c>True</c> if italic.</param>
    ''' <param name="underline"><see cref="System.Boolean"/> <c>True</c> if underline.</param>
    ''' <returns>A <see cref="System.Drawing.FontStyle"/></returns>
    Public Shared Function GetFontStyle(ByVal bold As Boolean, ByVal italic As Boolean, ByVal underline As Boolean) As FontStyle

        Dim style As FontStyle = FontStyle.Regular
        If bold Then
            style = FontStyle.Bold
        End If
        If italic Then
            style = style Or FontStyle.Italic
        End If
        If underline Then
            style = style Or FontStyle.Underline
        End If
        Return style

    End Function

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Construct <see cref="isr.Drawing.TextAppearance">TextAppearance</see> with given and default property values
    '''   values as defined in the <see cref="TextAppearanceDefaults"/> class.</summary>
    ''' <param name="font">A <see cref="System.Drawing.Font">Font</see></param>
    ''' <param name="color">The color with which to render the font</param>
    Public Sub New(ByVal font As Font, ByVal color As Color)

        ' instantiate the base class
        MyBase.New()

        If font Is Nothing Then
            Throw New ArgumentNullException("font")
        End If

        Me._baseFont = New System.Drawing.Font(font, font.Style)
        Me._lastBaseFont = CType(Me._baseFont.Clone, System.Drawing.Font)
        Me._scaledFont = CType(Me._baseFont.Clone, System.Drawing.Font)
        Me._scaledFontSize = Me._baseFont.Size
        ' superfluous per Code Analysis: Me._angle = 0.0F
        Me._fontColor = color
        Me._frame = New Frame

        reconstructFont(1.0F)

    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The TextAppearance object from which to copy</param>
    Public Sub New(ByVal model As TextAppearance)

        ' instantiate the base class
        MyBase.New()

        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Me._fontColor = model._fontColor
        Me._angle = model._angle
        Me._frame = model._frame.Copy()

        Me._baseFont = CType(model._baseFont.Clone, Font)
        Me._lastBaseFont = CType(model._lastBaseFont.Clone, Font)
        Me._scaledFont = CType(model._scaledFont.Clone, Font)
        Me._scaledFontSize = model._scaledFontSize
        reconstructFont(1.0F)

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _disposed As Boolean
    ''' <summary>Gets or sets (private) the dispose status sentinel.</summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._disposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me._scaledFont IsNot Nothing Then
                        Me._scaledFont.Dispose()
                        Me._scaledFont = Nothing
                    End If

                    If Me._frame IsNot Nothing Then
                        Me._frame.Dispose()
                        Me._frame = Nothing
                    End If

                    If Me._baseFont IsNot Nothing Then
                        Me._baseFont.Dispose()
                    End If

                    If Me._lastBaseFont IsNot Nothing Then
                        Me._lastBaseFont.Dispose()
                    End If

                    If Me._scaledFont IsNot Nothing Then
                        Me._scaledFont.Dispose()
                    End If

                    Me._fontColor = Nothing

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>get a <see cref="System.Drawing.SizeF">Size</see> structure representing the width and height
    '''   of the bounding box for the specified text string, based on the scaled font 
    '''   size. This method differs from <see cref="MeasureString"/> in that it takes 
    '''   into account the rotation angle of the font, and gives the dimensions of the
    '''   bounding box that encloses the text at the specified angle.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    ''' <param name="text">The text string for which the width is to be calculated</param>
    ''' <param name="scaleFactor">The scaling factor for the chart with reference
    '''   to the chart <see cref="Pane.BaseDimension"/>.  This scaling factor is 
    '''   calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''   The scale factor is applied to fonts, symbols, etc.</param>		
    ''' <returns>A scaled <see cref="System.Drawing.SizeF">Size</see> of text dimensions, in pixels</returns>
    Public Function BoundingBox(ByVal graphicsDevice As Graphics, ByVal [text] As String,
                                ByVal scaleFactor As Double) As SizeF

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If
        If String.IsNullOrWhiteSpace(text) Then
            Return New SizeF(0.0F, 0.0F)
        End If

        reconstructFont(scaleFactor)
        Dim s As SizeF = graphicsDevice.MeasureString([text], Me._scaledFont)

        Dim cs As Single = Convert.ToSingle(Math.Abs(Math.Cos((Me._angle * Math.PI / 180.0))))
        Dim sn As Single = Convert.ToSingle(Math.Abs(Math.Sin((Me._angle * Math.PI / 180.0))))

        Dim s2 As New SizeF(s.Width * cs + s.Height * sn, s.Width * sn + s.Height * cs)

        Return s2

    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the TextAppearance</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent <see cref="isr.Drawing.TextAppearance">TextAppearance</see> copy</returns>
    Public Function Copy() As TextAppearance
        Return New TextAppearance(Me)
    End Function

    ''' <summary>Renders the specified <paramref name="text"/> to the specified
    ''' <see cref="graphics"/> device.  The text, frame, and fill options
    ''' will be rendered as required.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    ''' <param name="text">A <see cref="System.String">String</see> value containing the text to be
    '''   displayed.  This can be multiple lines, separated by new line ('\n')
    '''   characters</param>
    ''' <param name="x">The X location to display the text, in screen
    '''   coordinates, relative to the horizontal (<see cref="HorizontalAlignment"/>)
    '''   alignment parameter <paramref name="alignH"/></param>
    ''' <param name="y">The Y location to display the text, in screen
    '''   coordinates, relative to the vertical (<see cref="VerticalAlignment"/>
    '''   alignment parameter <paramref name="alignV"/></param>
    ''' <param name="alignH">A <see cref="HorizontalAlignment"/> alignment value.</param>
    ''' <param name="alignV">A <see cref="VerticalAlignment"/> alignment value.</param>
    ''' <param name="scaleFactor">The scaling factor to be used for rendering 
    '''   objects.  This is calculated and passed down by the parent 
    '''   <see cref="Pane"/> object using the <see cref="Pane.getScaleFactor"/>
    '''   method, and is used to proportionally adjust font sizes, etc. 
    '''   according to the actual size of the graph.</param>
    ''' <remarks>Employs the transform matrix in order to position the
    '''   test at any angle while also allowing the location, or anchor point,
    '''   to be at a user-specified alignment.  A caption can be located
    '''   at a given point on the graph such that the left or center or right and top 
    '''   or middle or bottom can be the anchor point.  After thus shifting the origin
    '''   so that the anchor point will be at the (x,y), the method rotates the 
    '''   coordinate system to accommodate the text angle.  It then makes a translation 
    '''   to account for the fact that the text rendering method expects to draw the
    '''   test based on a top-center location while the user may have specified another 
    '''   alignment.</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal [text] As String,
                    ByVal x As Single, ByVal y As Single,
                    ByVal alignH As HorizontalAlignment, ByVal alignV As VerticalAlignment,
                    ByVal scaleFactor As Double)

        If String.IsNullOrWhiteSpace(text) Then
            Return
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        ' make sure the font size is properly scaled
        reconstructFont(scaleFactor)

        ' get the width and height of the text
        Dim sizeF As System.Drawing.SizeF = graphicsDevice.MeasureString([text], Me._scaledFont)

        ' Save the old transform matrix for later restoration
        Dim matrix As System.Drawing.Drawing2D.Matrix = graphicsDevice.Transform

        ' Move the coordinate system to local coordinates
        ' of this text object (that is, at the specified
        ' x,y location)
        graphicsDevice.TranslateTransform(x, y)

        ' Since the text will be drawn by graphicsDevice.DrawString() assuming the location is
        ' the top-center (the Font is aligned using StringFormat to the center so 
        ' multi-line text is center justified), shift the coordinate system so that 
        ' we are actually aligned per the caller specified position
        If alignH = HorizontalAlignment.Left Then
            x = sizeF.Width / 2.0F
        ElseIf alignH = HorizontalAlignment.Right Then
            x = -sizeF.Width / 2.0F
        Else
            x = 0.0F
        End If
        If alignV = VerticalAlignment.Center Then
            y = -sizeF.Height / 2.0F
        ElseIf alignV = VerticalAlignment.Bottom Then
            y = -sizeF.Height
        Else
            y = 0.0F
        End If

        ' Rotate the coordinate system according to the specified angle
        If Me._angle <> 0.0F Then
            graphicsDevice.RotateTransform(-_angle)
        End If

        ' Shift the coordinates to accommodate the alignment parameters
        graphicsDevice.TranslateTransform(x, y)

        ' make a center justified StringFormat alignment for drawing the text
        Using strFormat As New StringFormat
            strFormat.Alignment = StringAlignment.Center
            ' Create a rectangle representing the frame around the text.  Note that, 
            ' while the text is drawn based on the top-center position, the rectangle 
            ' is drawn based on the top-left position.  Therefore, move the rectangle
            ' width/2 to the left to align it properly
            Dim rectF As New RectangleF(-sizeF.Width / 2.0F, 0.0F, sizeF.Width, sizeF.Height)

            ' draw the frame
            Me._frame.Draw(graphicsDevice, rectF)

            Using sb As New SolidBrush(Me._fontColor)
                ' Draw the actual text.  Note that the coordinate system is set up such that
                ' 0,0 is at the location where the CenterTop of the text needs to be.
                graphicsDevice.DrawString([text], Me._scaledFont, sb, 0.0F, 0.0F, strFormat)
            End Using

            ' Restore the transform matrix back to original
            graphicsDevice.Transform = matrix
        End Using

    End Sub

    ''' <summary>Scale and get the <see cref="System.Drawing.SizeF">Size</see> of the specified text string, based 
    '''   on the scaled font size</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    ''' <param name="text">The text string for which the width is to be calculated</param>
    ''' <param name="scaleFactor">The scaling factor for the chart with reference
    '''   to the chart <see cref="Pane.BaseDimension"/>.  This scaling factor is 
    '''   calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''   The scale factor is applied to fonts, symbols, etc.</param>		
    ''' <returns>Scaled <see cref="System.Drawing.SizeF">Size</see> dimensions in pixels,</returns>
    Public Function MeasureString(ByVal graphicsDevice As Graphics, ByVal [text] As String,
                                  ByVal scaleFactor As Double) As SizeF

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        If String.IsNullOrWhiteSpace(text) Then
            Return New SizeF(0.0F, 0.0F)
        Else
            reconstructFont(scaleFactor)
            Return graphicsDevice.MeasureString([text], Me._scaledFont)
        End If
    End Function

    ''' <summary>get the <see cref="System.Drawing.SizeF">Size</see> of the specified text string, based 
    '''   on the scaled font size</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    ''' <param name="text">The text string for which the width is to be calculated</param>
    ''' <returns>Scaled <see cref="System.Drawing.SizeF">Size</see> dimensions in pixels,</returns>
    Public Function MeasureString(ByVal graphicsDevice As Graphics, ByVal [text] As String) As SizeF

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        If String.IsNullOrWhiteSpace(text) Then
            Return New SizeF(0.0F, 0.0F)
        Else
            Return graphicsDevice.MeasureString([text], Me._scaledFont)
        End If

    End Function

    ''' <summary>Scale and return the scaled <see cref="System.Drawing.Font">Font</see>.</summary>
    ''' <param name="scaleFactor">The scaling factor for the chart with reference
    '''   to the chart <see cref="Pane.BaseDimension"/>.  This scaling factor is 
    '''   calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''   The scale factor is applied to fonts, symbols, etc.</param>		
    ''' <returns>A reference to the scaled <see cref="System.Drawing.Font">Font</see> object with a size of 
    '''   <see cref="_scaledFontSize"/>, and font <see cref="FontFamily"/>.</returns>
    Public Function ScaleFont(ByVal scaleFactor As Double) As Font
        reconstructFont(scaleFactor)
        Return Me._scaledFont
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>The angle at which this <see cref="isr.Drawing.TextAppearance">TextAppearance</see> object is drawn.
    '''   The angle is measured in anti-clockwise degrees from horizontal.  
    '''   Negative values are permitted.</summary>
    ''' <value>A <see cref="System.Single">Single</see> in radians</value> 
    Public Property Angle() As Single

    ''' <summary>Gets or sets the base <see cref="System.Drawing.Font">Font</see> of the
    '''   <see cref="isr.Drawing.TextAppearance">TextAppearance</see>, which serves to create the scaled font for 
    '''   rendering text.</summary>
    Private _baseFont As System.Drawing.Font

    ''' <summary>The color of the font characters for this <see cref="isr.Drawing.TextAppearance">TextAppearance</see>.
    ''' Note that the frame and background
    ''' colors are set using the <see cref="Frame.LineColor"/> and
    ''' <see cref="Frame.FillColor"/> properties, respectively.</summary>
    ''' <value>A <see cref="System.Drawing.Color">Color</see> value</value>
    Public Property FontColor() As Color

    ''' <summary>The size of the font for this <see cref="isr.Drawing.TextAppearance">TextAppearance</see> object.</summary>
    ''' <value>A <see cref="System.Single">Single</see> in points (1/72 inch)</value> 
    ''' <remarks>The new size will be scaled by the existing scale factor.</remarks>
    Public Property FontSize() As Single
        Get
            Return Me._baseFont.Size
        End Get
        Set(ByVal value As Single)
            If value <> Me.FontSize Then
                Me._baseFont = New Font(Me._baseFont.FontFamily, value, Me._baseFont.Style)
            End If
        End Set
    End Property

    ''' <summary>The font family name for this <see cref="isr.Drawing.TextAppearance">TextAppearance</see>.</summary>
    ''' <value>A <see cref="System.String">String</see> with the 
    '''   <see cref="System.Drawing.Font">Font</see> family name, e.graphicsDevice., "Arial"</value>
    Public Property FontFamilyName() As String
        Get
            Return Me._baseFont.FontFamily.Name
        End Get
        Set(ByVal value As String)
            If value <> Me.FontFamilyName Then
                Me._baseFont = New Font(value, Me._baseFont.Size, Me._baseFont.Style)
            End If
        End Set
    End Property

    ''' <summary>Gets or sets the frame property.</summary>
    ''' <value>A <see cref="Frame"/> property</value>
    Public Property Frame() As Frame

    ''' <summary>Gets or sets the <see cref="isr.Drawing.TextAppearance">TextAppearance</see> Bold font style.</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see> value, True for bold, false for not Italic</value>
    Public Property Bold() As Boolean
        Get
            Return Me._baseFont.Bold
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.Bold Then
                Me._baseFont = New Font(Me._baseFont.FontFamily, Me._baseFont.Size,
                                        TextAppearance.GetFontStyle(value, Me._baseFont.Italic, Me._baseFont.Underline))
            End If
        End Set
    End Property

    ''' <summary>Gets or sets the <see cref="isr.Drawing.TextAppearance">TextAppearance</see> Italic font style.</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see> value, true for italic, false for normal</value>
    Public Property Italic() As Boolean
        Get
            Return Me._baseFont.Italic
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.Italic Then
                Me._baseFont = New Font(Me._baseFont.FontFamily, Me._baseFont.Size,
                                        TextAppearance.GetFontStyle(Me._baseFont.Bold, value, Me._baseFont.Underline))
            End If
        End Set
    End Property

    ''' <summary>Gets or sets the <see cref="isr.Drawing.TextAppearance">TextAppearance</see> Underline font style.</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see> value, true for underline, false for normal</value>
    Public Property IsUnderline() As Boolean
        Get
            Return Me._baseFont.Underline
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.IsUnderline Then
                Me._baseFont = New Font(Me._baseFont.FontFamily, Me._baseFont.Size,
                                        TextAppearance.GetFontStyle(Me._baseFont.Bold, Me._baseFont.Italic, value))
            End If
        End Set
    End Property

    ''' <summary>Gets or sets the last base font that was constructed for drawing.  This stored value
    '''   is used for comparing the new with the old for determining if a new 
    '''   scaled font needs to be constructed.</summary>
    Private _lastBaseFont As System.Drawing.Font

    ''' <summary>Private field that stores a reference to the scaled <see cref="System.Drawing.Font">Font</see>
    '''   object for this <see cref="isr.Drawing.TextAppearance">TextAppearance</see>.  This font object will be at
    '''   the actual drawn size <see cref="_scaledFontSize"/> according to the current
    '''   size of the <see cref="Pane"/>.  Use the public method
    '''   <see cref="ScaledFont"/> to access this font object.</summary>
    Private _scaledFont As System.Drawing.Font

    ''' <summary>Gets or sets the scaled font</summary>
    ''' <value>A <see cref="System.Drawing.Font">Font</see> property</value>
    Public ReadOnly Property ScaledFont() As System.Drawing.Font
        Get
            Return Me._scaledFont
        End Get
    End Property

    ''' <summary>Private field that temporarily stores the scaled size of the font for this
    '''   <see cref="isr.Drawing.TextAppearance">TextAppearance</see> object.  This represents the actual on-screen
    '''   size, rather than the <see cref="Size"/> that represents the reference
    '''   size for a "full-sized" <see cref="Pane"/>.</summary>
    Private _scaledFontSize As Single

    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String"/> value</value>
    ''' <remarks>Use this property to get or set the file name.</remarks>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary>Reconstruct the scaled font if different than the existing.</summary>
    ''' <param name="scaleFactor">The chart scaling factor relative to the 
    '''   <see cref="Pane.BaseDimension"/>, as calculated by <see cref="Pane.getScaleFactor"/>.</param>		
    Private Sub reconstructFont(ByVal scaleFactor As Double)

        ' get the requested new size
        Dim newSize As Single = Convert.ToSingle(Me._baseFont.Size * scaleFactor)

        ' Regenerate the font only if the size has changed significantly or the font is
        ' new 
        If Math.Abs((newSize - Me._scaledFontSize)) > TextAppearanceDefaults.[Get].SignificantFontSizeChange OrElse
            (Not Me._baseFont.Equals(Me._lastBaseFont)) Then

            ' make the new font.
            Me._scaledFont = New Font(Me._baseFont.FontFamily, newSize, Me._baseFont.Style)

            ' save the new size
            Me._scaledFontSize = newSize

            ' update the last font.
            Me._lastBaseFont = CType(Me._baseFont.Clone, Font)

        End If

    End Sub

#End Region

End Class

#Region " DEFAULTS "

''' <summary>A simple subclass of the <see cref="isr.Drawing.TextAppearance">TextAppearance</see> class that defines the
'''   default property values for the <see cref="isr.Drawing.TextAppearance">TextAppearance</see> class.</summary>
Public NotInheritable Class TextAppearanceDefaults

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs this class.
    ''' This constructor is private to ensure only a single instance of this class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
        Me._SignificantFontSizeChange = 0.1
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared instance As TextAppearanceDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As TextAppearanceDefaults
        If TextAppearanceDefaults.instance Is Nothing Then
            SyncLock TextAppearanceDefaults.syncLocker
                TextAppearanceDefaults.instance = New TextAppearanceDefaults()
            End SyncLock
        End If
        Return TextAppearanceDefaults.instance
    End Function

#End Region

    ''' <summary>Gets or sets the change in font size that is considered significant.</summary>
    Public Property SignificantFontSizeChange() As Double

End Class

#End Region

