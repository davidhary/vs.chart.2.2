''' <summary>A collection class containing a list of <see cref="Curve"/> objects
''' that define the set of curves to be displayed on the graph.</summary>
''' <remarks>
''' Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the constructor.
''' </remarks>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public NotInheritable Class CurveCollection
  Inherits System.Collections.ObjectModel.Collection(Of Curve)

  Implements ICloneable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>Default constructor for the collection class.</summary>
  ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
  Public Sub New(ByVal drawingPane As Pane)
    MyBase.new()
    Me._pane = drawingPane
  End Sub

  ''' <summary>The Copy Constructor</summary>
  ''' <param name="model">The XAxis object from which to copy</param>
  Public Sub New(ByVal model As CurveCollection)
    MyBase.new()
    If model Is Nothing Then
      Throw New ArgumentNullException("model")
    End If
    Dim item As Curve
    For Each item In model
      Me.Add(New Curve(item))
    Next item
    Me._pane = Me._pane
  End Sub

#End Region

#Region " CUSTOM COLLECTION METHODS "

  ''' <summary>Add a <see cref="Curve"/> to the collection.</summary>
  ''' <param name="curve">A reference to the <see cref="Curve"/> object to add</param>
  Public Overloads Sub Add(ByVal curve As Curve)
    If curve Is Nothing Then
      Throw New ArgumentNullException("curve")
    End If
    MyBase.Add(curve)
    curve.SerialNumber = MyBase.Count
  End Sub

  ''' <summary>Deep-copy clone routine</summary>
  ''' <returns>A new, independent copy of the CurveCollection</returns>
  Public Function Clone() As Object Implements ICloneable.Clone
    Return Me.Copy()
  End Function

  ''' <summary>Deep-copy clone routine</summary>
  ''' <returns>A new, independent copy of the CurveCollection</returns>
  Public Function Copy() As CurveCollection
    Return New CurveCollection(Me)
  End Function

  ''' <summary>Renders all the <see cref="Curve">Curves</see> to the
  ''' specified <see cref="graphics"/> device.</summary>
  ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
  '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
  Public Sub Draw(ByVal graphicsDevice As Graphics)

    ' validate argument.
    If graphicsDevice Is Nothing Then
      Throw New ArgumentNullException("graphicsDevice")
    End If

    ' Loop for each curve
    For Each curve As Curve In Me
      ' Render the curve
      curve.Draw(graphicsDevice) ' , axisArea, scaleFactor)
    Next curve

  End Sub

  ''' <summary>Adjusts the <see cref="Pane.AxisArea"/> for the 
  '''   <see cref="isr.Drawing.Stylus">Stylus</see> size.</summary>
    ''' <param name="scaleFactor">The scaling factor for the chart with reference
  '''   to the chart <see cref="Pane.BaseDimension"/>.  This scaling factor is 
  '''   calculated by the <see cref="Pane.getScaleFactor"/> method.  
  '''   The scale factor is applied to fonts, symbols, etc.</param>		
  ''' <param name="axisArea">The rectangle that contains the area bounded by the axes,
  '''   in pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso></param>
  ''' <returns>Adjusted <see cref="Pane.AxisArea"/></returns>
  Public Function GetAxisArea(ByVal scaleFactor As Double, ByVal axisArea As RectangleF) As RectangleF

    ' Loop for each curve
    For Each curve As Curve In Me
      ' Render the curve
      axisArea = curve.getAxisArea(scaleFactor, axisArea)
    Next curve

    ' return adjusted axis rectangle
    Return axisArea

  End Function

  ''' <summary>get the extended range of all specified curves</summary>
  ''' <param name="ignoreInitial">Affects how initial zero y values are treated
  '''   for setting the range of X values.  If True, then initial zero data points are 
  '''   ignored when determining the X range.  All data after the first non-zero Y value 
  '''   are included.</param>
  ''' <param name="isY2axis">True to use Y2 Axis.  Otherwise Y Axis.</param>
  Public Function GetRange(ByVal ignoreInitial As Boolean, ByVal isY2Axis As Boolean) As PlanarRangeR

    ' get the empty range
    Dim range As PlanarRangeR = PlanarRangeR.Empty

    ' Loop over each curve in the collection
    For Each curve As Curve In Me

      If (isY2Axis AndAlso curve.IsY2Axis) OrElse 
        ((Not isY2Axis) AndAlso (Not curve.IsY2Axis)) Then

        range.ExtendRange(curve.getRange(ignoreInitial))

      End If

    Next

    If range.Equals(PlanarRangeR.Empty) Then
      ' Use unit range if no data were available
      Return PlanarRangeR.Unity
    Else
      Return range
    End If

  End Function

  ''' <summary>Determine if there is any data in any of the <see cref="Curve"/>
  ''' objects for this graph.  This method does not verify valid data, it
  ''' only checks to see if <see cref="Curve.PointCount"/> > 0.</summary>
    ''' <returns><c>True</c> if there is any data, false otherwise</returns>
  Public Function HasData() As Boolean
    For Each curve As Curve In Me
      If curve.PointCount > 0 Then
        Return True
      End If
    Next curve
    Return False
  End Function

  ''' <summary>Rescale all the time series <see cref="Axis"/> objects in the list to the
  '''   specified <see cref="CurveCollection"/> by calling the <see cref="Axis.Rescale"/> 
  '''   method of each <see cref="Axis"/> object.</summary>
  Public Sub ScaleTimeSeriesAxis()

    ' Loop for each curve
    For Each curve As Curve In Me
      curve.ScaleTimeSeriesAxis()
    Next curve

  End Sub

  ''' <summary>Gets or sets reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></summary>
  Private _pane As Pane

#End Region

End Class

