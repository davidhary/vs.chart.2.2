''' <summary>Contains a list of <see cref="TimeSeriesBookmark"/> objects, which tag a time series.</summary>
''' <remarks>
''' Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the constructor.
''' </remarks>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="06/02/04" by="David" revision="1.0.1614.x">
''' Created
''' </history>
Public NotInheritable Class TimeSeriesBookmarkCollection
  Inherits System.Collections.ObjectModel.Collection(Of TimeSeriesBookmark)
  Implements ICloneable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>Default constructor for the <see cref="TimeSeriesBookmarkCollection"/> collection class.</summary>
  ''' <param name="curve">Reference to the <see cref="isr.Drawing.Curve">Curve</see></param>
  Public Sub New(ByVal curve As Curve)
    MyBase.new()
    Me._curve = curve
  End Sub

  ''' <summary>The Copy Constructor</summary>
  ''' <param name="model">The TimeSeriesBookmarkCollection object from which to copy</param>
  Public Sub New(ByVal model As TimeSeriesBookmarkCollection)
    MyBase.new()
    If model Is Nothing Then
      Throw New ArgumentNullException("model")
    End If
    Dim item As isr.Drawing.TimeSeriesBookmark
    For Each item In model
      Me.Add(New isr.Drawing.TimeSeriesBookmark(item))
    Next item
    Me._curve = model._curve
  End Sub

#End Region

#Region " CUSTOM COLLECTION METHODS "

  ''' <summary>Add a <see cref="TimeSeriesBookmark"/> to the collection.</summary>
  ''' <param name="bookmark">A reference to the <see cref="TimeSeriesBookmark"/> object to add</param>
  Public Overloads Sub Add(ByVal bookmark As TimeSeriesBookmark)
    MyBase.Add(bookmark)
    bookmark.SerialNumber = MyBase.Count
  End Sub

  ''' <summary>Deep-copy clone routine</summary>
  ''' <returns>A new, independent copy of the TimeSeriesBookmarkCollection</returns>
  Public Function Clone() As Object Implements ICloneable.Clone
    Return Me.Copy()
  End Function

  ''' <summary>Deep-copy clone routine</summary>
  ''' <returns>A new, independent copy of the TimeSeriesBookmarkCollection</returns>
  Public Function Copy() As TimeSeriesBookmarkCollection
    Return New TimeSeriesBookmarkCollection(Me)
  End Function

  ''' <summary>Creates a one-dimensional <see cref="T:System.Array">Array</see> 
  '''   instance containing the collection items.</summary>
  ''' <returns>Array of type <see cref="TimeSeriesBookmark">time series Bookmark</see></returns>
  Public Function ToArray() As TimeSeriesBookmark()
    Dim Bookmarks As TimeSeriesBookmark() = New TimeSeriesBookmark(Count - 1) {}
    CopyTo(Bookmarks, 0)
    Return Bookmarks
  End Function

  ''' <summary>Gets or sets reference to the drawing <see cref="isr.Drawing.Curve">curve</see></summary>
  Private _curve As Curve

#End Region

End Class

