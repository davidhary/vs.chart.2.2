''' <summary>Defines a <see cref="System.Double">Double</see> two-dimensional range structure.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public Structure PlanarRangeR

#Region " SHARED "

    ''' <summary>Gets or sets the empty range</summary>
    ''' <value>A <see cref="PlanarRangeR"/> value with empty X and Y ranges</value>
    Public Shared ReadOnly Property [Empty]() As PlanarRangeR
        Get
            Return New PlanarRangeR(isr.Drawing.RangeR.Empty, isr.Drawing.RangeR.Empty)
        End Get
    End Property

    ''' <summary>gets the planar range for the <see cref="X"/> and <see cref="Y"/> data arrays</summary>
    ''' <param name="x">The x data array</param>
    ''' <param name="y">The y data array</param>
    ''' <param name="ignoreInitial">Affects how initial zero y values are treated
    '''   for setting the range of X values.  If True, then initial zero data points are 
    '''   ignored when determining the X range.  All data after the first non-zero Y value 
    '''   are included.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Shared Function GetRange(ByVal x() As Double, ByVal y() As Double, ByVal ignoreInitial As Boolean) As PlanarRangeR

        ' return the unit range if no data
        If x Is Nothing OrElse y Is Nothing OrElse x.Length = 0 OrElse y.Length = 0 Then
            Return PlanarRangeR.Unity
        End If

        Dim NumPoints As Integer = Convert.ToInt32(Math.Min(x.Length, y.Length))

        ' initialize the values to the empty range
        Dim yTemp As Double
        Dim xTemp As Double
        yTemp = y(0)
        xTemp = x(0)
        Dim xMin As Double = xTemp
        Dim yMin As Double = yTemp
        Dim xMax As Double = xTemp
        Dim yMax As Double = yTemp

        ' Loop over each point in the arrays
        For i As Integer = 0 To NumPoints - 1

            yTemp = y(i)
            xTemp = x(i)

            ' ignoreInitial becomes false at the first non-zero Y value
            If ignoreInitial AndAlso (yTemp <> 0) AndAlso (yTemp <> System.Double.MaxValue) Then
                ignoreInitial = False
            End If

            If Not ignoreInitial AndAlso (xTemp <> System.Double.MaxValue) AndAlso (yTemp <> System.Double.MaxValue) Then
                If xTemp < xMin Then
                    xMin = xTemp
                ElseIf xTemp > xMax Then
                    xMax = xTemp
                End If
                If yTemp < yMin Then
                    yMin = yTemp
                ElseIf yTemp > yMax Then
                    yMax = yTemp
                End If
            End If

        Next i

        Return New PlanarRangeR(New isr.Drawing.RangeR(xMin, xMax), New isr.Drawing.RangeR(yMin, yMax))

    End Function

    ''' <summary>Gets or sets the unit range</summary>
    ''' <value>A <see cref="PlanarRangeR"/> value with unity X and Y ranges</value>
    Public Shared ReadOnly Property Unity() As PlanarRangeR
        Get
            Return New PlanarRangeR(isr.Drawing.RangeR.Unity, isr.Drawing.RangeR.Unity)
        End Get
    End Property

    ''' <summary>Gets or sets the zero range value</summary>
    ''' <value>A <see cref="PlanarRangeR"/> value</value>
    Public Shared ReadOnly Property Zero() As PlanarRangeR
        Get
            Return New PlanarRangeR(RangeR.Zero, RangeR.Zero)
        End Get
    End Property

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Contracts a planar range based on the X and Y ranges.</summary>
    ''' <param name="x">The horizontal <see cref="isr.Drawing.RangeR"/></param>
    ''' <param name="y">The vertical <see cref="isr.Drawing.RangeR"/></param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub New(ByVal x As isr.Drawing.RangeR, ByVal y As isr.Drawing.RangeR)

        SetPlanarRange(x, y)

    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The PlanarRangeR object from which to copy</param>
    Public Sub New(ByVal model As PlanarRangeR)

        Me._x = model._x
        Me._y = model._y

    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <param name="left">  Planar range r to be compared. </param>
    ''' <param name="right"> Planar range r to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As PlanarRangeR, ByVal right As PlanarRangeR) As Boolean
        Return PlanarRangeR.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <param name="left">  Planar range r to be compared. </param>
    ''' <param name="right"> Planar range r to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As PlanarRangeR, ByVal right As PlanarRangeR) As Boolean
        Return Not PlanarRangeR.Equals(left, right)
    End Operator

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <param name="left">  Planar range r to be compared. </param>
    ''' <param name="right"> Planar range r to be compared. </param>
    ''' <returns> <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>. </returns>
    Public Overloads Shared Function Equals(ByVal left As PlanarRangeR, ByVal right As PlanarRangeR) As Boolean
        Return left._X.Equals(right._X) AndAlso left._Y.Equals(right._Y)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <param name="obj"> Another object to compare to. </param>
    ''' <returns> <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean

        If obj IsNot Nothing AndAlso TypeOf obj Is PlanarRangeR Then
            Return Equals(CType(obj, PlanarRangeR))
        Else
            Return False
        End If

    End Function

    ''' <summary>Returns True if the value of the <param>compared</param> equals to the
    '''   instance value.</summary>
    ''' <param name="compared">The <see cref="PlanarRangeR">PlanarRangeR</see> to compare for 
    ''' equality with this instance.</param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    ''' <remarks>Planar Ranges are the same if the have the same 
    ''' <see cref="X"/> and <see cref="Y"/> ranges.</remarks>
    Public Overloads Function Equals(ByVal compared As PlanarRangeR) As Boolean
        Return PlanarRangeR.Equals(Me, compared)
    End Function

    ''' <summary>Creates a unique hash code.</summary>
    ''' <returns>An <see cref="System.integer">integer</see> value</returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me._x.GetHashCode Xor Me._y.GetHashCode
    End Function

#End Region

#Region " METHODS "

    ''' <summary>Returns true if the <see cref="PlanarRangeR"/> contains the 
    '''   <see cref="PointF">Point</see></summary>
    ''' <param name="point">A <see cref="PointF">Point</see></param>
    Public Function Contains(ByVal point As PointF) As Boolean
        Return Me._x.Contains(point.X) AndAlso Me._y.Contains(point.Y)
    End Function

    ''' <summary>Returns true if the <see cref="PlanarRangeR"/> contains the 
    '''   <see cref="PointF">Point</see></summary>
    ''' <param name="point">A <see cref="PointF">Point</see></param>
    ''' <param name="tolerance">A <see cref="SizeF">Tolerance</see> around the point</param>
    Public Function Contains(ByVal point As PointF, ByVal tolerance As SizeF) As Boolean
        Return Me._x.Contains(point.X, tolerance.Width) AndAlso Me._y.Contains(point.Y, tolerance.Height)
    End Function

    ''' <summary>Extend this range to include both its present values and the
    '''   specified range.</summary>
    ''' <param name="range">A <see cref="planarRangeR"/> value</param>
    Public Function ExtendRange(ByVal range As PlanarRangeR) As PlanarRangeR

        Me._x.ExtendRange(range.X)
        Me._y.ExtendRange(range.Y)
        Return Me

    End Function

    ''' <summary>Sets the PlanarRangeR based on the extrema.</summary>
    ''' <param name="x">The horizontal <see cref="isr.Drawing.RangeR"/>.</param>
    ''' <param name="y">The vertical <see cref="isr.Drawing.RangeR"/>.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Overloads Sub SetPlanarRange(ByVal x As isr.Drawing.RangeR, ByVal y As isr.Drawing.RangeR)

        Me._x = x
        Me._y = y

    End Sub

    ''' <summary>Returns the default string representation of the PlanarRangeR.</summary>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                             "[{0},{1}]", Me._x.ToString(), Me._y.ToString())
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets the horizontal range</summary>
    ''' <value>A <see cref="isr.Drawing.RangeR"/> value</value>
    Public Property X() As isr.Drawing.RangeR

    ''' <summary>Gets or sets the vertical range</summary>
    ''' <value>A <see cref="isr.Drawing.RangeR"/> value</value>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Y")>
    Public Property Y() As isr.Drawing.RangeR

#End Region

End Structure

