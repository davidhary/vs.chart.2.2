''' <summary>Provides Tick properties.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public Class Tick

    Implements ICloneable, IDisposable

#Region " SHARED "

    ''' <summary>Return a standard hour spacing at 1, 2, 3, 6, 12, or 24 hours</summary>
    ''' <param name="hour">Hour to round up to standard spacing</param>
    ''' <returns>A standard hours spacing</returns>
    Public Shared Function SelectHourSpacing(ByVal hour As Double) As Double

        hour = Math.Floor(hour)
        If hour > 12.0 Then
            Return 24.0
        ElseIf hour > 6.0 Then
            Return 12.0
        ElseIf hour > 3.0 Then
            Return 6.0
        ElseIf hour < 1.0 Then
            Return 1.0
        Else
            Return hour
        End If

    End Function

    ''' <summary>Return a standard minute spacing at 1, 5, 15 or 30 minutes</summary>
    ''' <param name="minute">Minute to round up to standard spacing</param>
    ''' <returns>A standard minute spacing</returns>
    Public Shared Function SelectMinuteSpacing(ByVal minute As Double) As Double

        minute = Math.Floor(minute)
        ' make sure the minute spacing is 1, 5, 15, or 30 minutes
        If minute > 15.0 Then
            Return 30.0
        ElseIf minute > 5.0 Then
            Return 15.0
        ElseIf minute > 1.0 Then
            Return 5.0
        ElseIf minute < 1.0 Then
            Return 1.0
        Else
            Return minute
        End If

    End Function

    ''' <summary>Return a standard month spacing at 1, 2, 3, 6, or 12 month</summary>
    ''' <param name="month">Month to round up to standard month spacing</param>
    ''' <returns>A standard month spacing</returns>
    Public Shared Function SelectMonthSpacing(ByVal month As Double) As Double

        month = Math.Floor(month)
        ' make sure the minor spacing is 1, 2, 3, 6, or 12 months
        If month > 6 Then
            Return 12
        ElseIf month > 3 Then
            Return 6
        Else
            Return month
        End If

    End Function

    ''' <summary>Return a standard second spacing at 1, 5, 15, or 30 seconds</summary>
    ''' <param name="second">Second to round up to standard spacing</param>
    ''' <returns>A standard second spacing</returns>
    Public Shared Function SelectSecondSpacing(ByVal second As Double) As Double

        second = Math.Floor(second)
        ' make sure the second spacing is 1, 5, 15, or 30 minutes
        If second > 15.0 Then
            Return 30.0
        ElseIf second > 5.0 Then
            Return 15.0
        ElseIf second > 1.0 Then
            Return 5.0
        ElseIf second < 1.0 Then
            Return 1.0
        Else
            Return second
        End If

    End Function

    ''' <summary>Return a standard spacing at 1, 2, 5, or 10 size</summary>
    ''' <param name="spacing">Value to round up to standard spacing</param>
    ''' <returns>A standard spacing</returns>
    Public Shared Function SelectStandardSpacing(ByVal spacing As Double) As Double

        spacing = Math.Floor(spacing)
        ' promote the spacing to either 1, 2, 5, or 10
        If spacing > 7.5 Then
            Return 10.0
        ElseIf spacing > 2.5 Then
            Return 5.0
        ElseIf spacing > 1.5 Then
            Return 2.0
        Else
            Return 1
        End If

    End Function

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs an instance of this class.</summary>
    ''' <param name="major"><see cref="System.Boolean">Boolean</see> True for major tick marks
    '''   and False for minor tick marks</param>
    Public Sub New(ByVal major As Boolean)

        ' instantiate the base class
        MyBase.New()

        Me._isMajor = major
        Me._ScaleFormat = TickDefaults.[Get].ScaleFormat
        If major Then
            With MajorTickDefaults.[Get]
                Me._IsInside = .IsInside
                Me._IsBottom = .IsBottom
                Me._IsTop = .IsTop
                Me._IsOutside = .IsOutside
                Me._Visible = .Visible
                Me._LineColor = .LineColor
                Me._LineWidth = .LineWidth
                Me._Length = .Length
                Me._spacing = .Spacing
                Me._AutoSpacing = .AutoSpacing
                Me._TimeIntervalType = .TimeIntervalType
            End With
        Else
            With MinorTickDefaults.[Get]
                Me._IsInside = .IsInside
                Me._IsBottom = .IsBottom
                Me._IsTop = .IsTop
                Me._IsOutside = .IsOutside
                Me._Visible = .Visible
                Me._LineColor = .LineColor
                Me._LineWidth = .LineWidth
                Me._Length = .Length
                Me._spacing = .Spacing
                Me._AutoSpacing = .AutoSpacing
                Me._TimeIntervalType = .TimeIntervalType
            End With
        End If

    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The Tick object from which to copy</param>
    Public Sub New(ByVal model As Tick)

        Me.new(True)
        If model IsNot Nothing Then
            Me._isMajor = model._isMajor
            Me._autoSpacing = model._autoSpacing
            Me._IsInside = model._IsInside
            Me._isTop = model._isTop
            Me._isOutside = model._isOutside
            Me._visible = model._visible
            Me._lineColor = model._lineColor
            Me._lineWidth = model._lineWidth
            Me._length = model._length
            Me._spacing = model._spacing
            Me._scaleFormat = model._scaleFormat
            Me._TimeIntervalType = model._TimeIntervalType
        End If

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _disposed As Boolean
    ''' <summary>Gets or sets (private) the dispose status sentinel.</summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._disposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub


#End Region

#Region " METHODS "

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of Tick</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the Tick</returns>
    Public Function Copy() As Tick
        Return New Tick(Me)
    End Function

    ''' <summary> Draws a single tick mark. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice">  Reference to a graphic device to be drawn into.  This is
    ''' normally
    ''' <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method. </param>
    ''' <param name="tickPen">         Reference to the tick
    ''' <see cref="System.Drawing.Pen">pen</see> </param>
    ''' <param name="x">               The pixel location from the 'left side the
    ''' <see cref="Pane.AxisArea"/>. </param>
    ''' <param name="transformedArea"> The pixel location of the far side of the
    ''' <see cref="Pane.AxisArea"/> from this axis. This value is the axisArea.Height for the X Axis,
    ''' or the axisArea.Width for the Y Axis and Y2 Axis. </param>
    ''' <param name="tickSize">        Size of the tick. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Private Sub Draw(ByVal graphicsDevice As Graphics, ByVal tickPen As Pen, ByVal x As Single,
                     ByVal transformedArea As RectangleF, ByVal tickSize As Single)

        If Not Me._Visible Then
            Return
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        If Me.IsInside OrElse Me.IsOutside Then
            Dim y1 As Single = 0.0F
            Dim y2 As Single = 0.0F
            ' draw the outside tick
            If Me._IsOutside Then
                y2 = tickSize
            End If
            ' draw the inside tick
            If Me.IsInside Then
                y1 = -tickSize
            End If
            ' update:  for drawing ticks at Axis Point replace 0.0F with LocalTransform Axis point.
            graphicsDevice.DrawLine(tickPen, x, y1, x, y2)
        End If

        ' draw the top tick
        If Me.IsTop Then
            Dim topPosition As Single = transformedArea.Top
            graphicsDevice.DrawLine(tickPen, x, topPosition, x, topPosition + tickSize)
        End If

        ' draw the bottom tick
        If Me.IsBottom Then
            Dim bottomPosition As Single = transformedArea.Bottom
            graphicsDevice.DrawLine(tickPen, x, bottomPosition, x, bottomPosition - tickSize)
        End If

    End Sub

    ''' <summary>Draw major tick marks</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    ''' <param name="axis">Reference to the <see cref="Axis"/></param>
    ''' <param name="scaleFactor">The scaling factor for the chart with reference
    '''   to the chart <see cref="Pane.BaseDimension"/>.  This scaling factor is 
    '''   calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''   The scale factor is applied to fonts, symbols, etc.</param>		
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal axis As Axis, ByVal scaleFactor As Double)

        If Not Me._visible Then
            Return
        End If

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        Dim scaledTick As Single = Me.getScaledLength(scaleFactor)
        Using tickPen As New Pen(Me._lineColor, Me._lineWidth)
            ' loop for each major tick
            For i As Integer = 0 To Me._validTickCount - 1
                ' draw the major tick
                Draw(graphicsDevice, tickPen, Me._locations(i), axis.TransformedArea, scaledTick)
            Next
        End Using

    End Sub

    ''' <summary>Calculate a date that is close to the specified date and an even multiple 
    '''   of the selected <see cref="TimeIntervalType"/> for a 
    '''   <see cref="CoordinateScaleType.Date"/> scale.</summary>
    ''' <param name="dateValue">The date which the calculation should be close to</param>
    ''' <param name="direction">The desired direction for the date to take.
    '''   One (1) indicates the result date should be greater than the specified
    '''   date parameter.  Minus one (-1) indicates the other direction.</param>
    ''' <returns>The calculated date</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Friend Function getEvenDateSpacing(ByVal dateValue As Double, ByVal direction As Integer) As Double

        Dim year, month, day, hour, minute, second, millisecond As Integer

        JulianDate.ToCalendarDate(dateValue, year, month, day, hour, minute, second, millisecond)

        ' If the direction is -1, then it is sufficient to go to the beginning of
        ' the current time period, .e.graphicsDevice., for 15-May-95, and monthly spacing, we
        ' can just back up to 1-May-95
        If direction < 0 Then
            direction = 0
        End If
        Select Case Me._timeIntervalType
            Case TimeIntervalType.Year
                ' If the date is already an exact year, then don't space to the next year
                If direction = 1 AndAlso month = 1 AndAlso day = 1 AndAlso hour = 0 AndAlso minute = 0 AndAlso second = 0 Then
                    Return dateValue
                Else
                    Return JulianDate.FromArbitraryCalendarDate(year + direction, 1, 1, 0, 0, 0, 0)
                End If
            Case TimeIntervalType.Month
                ' If the date is already an exact month, then don't space to the next month
                If direction = 1 AndAlso day = 1 AndAlso hour = 0 AndAlso minute = 0 AndAlso second = 0 Then
                    Return dateValue
                Else
                    Return JulianDate.FromArbitraryCalendarDate(year, month + direction, 1, 0, 0, 0, 0)
                End If
            Case TimeIntervalType.Day
                ' If the date is already an exact Day, then don't space to the next day
                If direction = 1 AndAlso hour = 0 AndAlso minute = 0 AndAlso second = 0 Then
                    Return dateValue
                Else
                    Return JulianDate.FromArbitraryCalendarDate(year, month, day + direction, 0, 0, 0, 0)
                End If
            Case TimeIntervalType.Hour
                ' If the date is already an exact hour, then don't space to the next hour
                If direction = 1 AndAlso minute = 0 AndAlso second = 0 Then
                    Return dateValue
                Else
                    Return JulianDate.FromArbitraryCalendarDate(year, month, day, hour + direction, 0, 0, 0)
                End If
            Case TimeIntervalType.Minute
                ' If the date is already an exact minute, then don't space to the next minute
                If direction = 1 AndAlso second = 0 Then
                    Return dateValue
                Else
                    Return JulianDate.FromArbitraryCalendarDate(year, month, day, hour, minute + direction, 0, 0)
                End If
            Case TimeIntervalType.Second
                Return JulianDate.FromArbitraryCalendarDate(year, month, day, hour, minute, second + direction, 0)
        End Select

    End Function

    ''' <summary>Determines the value for the first major tick mark, i.e., the first value that is an 
    '''   integral multiple of the tick spacing, taking into account date/time units if appropriate.</summary>
    ''' <param name="axis">Reference to the <see cref="Axis"/></param>
    ''' <returns>First major tick mark value.</returns>
    Private Function getFirstMajorTickValue(ByVal axis As Axis) As Double

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        Select Case axis.CoordinateScale.CoordinateScaleType

            Case CoordinateScaleType.Date

                Dim year, month, day, hour, minute, second, millisecond As Integer
                JulianDate.ToCalendarDate(axis.Min.Value, year, month, day, hour, minute, second, millisecond)
                Select Case Me._timeIntervalType
                    Case TimeIntervalType.Year
                        month = 1
                        day = 1
                        hour = 0
                        minute = 0
                        second = 0
                    Case TimeIntervalType.Month
                        day = 1
                        hour = 0
                        minute = 0
                        second = 0
                    Case TimeIntervalType.Day
                        hour = 0
                        minute = 0
                        second = 0
                    Case TimeIntervalType.Hour
                        minute = 0
                        second = 0
                    Case TimeIntervalType.Minute
                        second = 0
                    Case TimeIntervalType.Second
                End Select

                Dim xlsDay As Double = JulianDate.FromArbitraryCalendarDate(year, month, day, hour, minute, second, 0)
                If xlsDay < axis.Min.Value Then
                    Select Case Me._timeIntervalType
                        Case TimeIntervalType.Year
                            year += 1
                        Case TimeIntervalType.Month
                            month += 1
                        Case TimeIntervalType.Day
                            day += 1
                        Case TimeIntervalType.Hour
                            hour += 1
                        Case TimeIntervalType.Minute
                            minute += 1
                        Case TimeIntervalType.Second
                            second += 1
                    End Select

                    xlsDay = JulianDate.FromArbitraryCalendarDate(year, month, day, hour, minute, second, 0)
                End If

                Return xlsDay

            Case CoordinateScaleType.Linear, CoordinateScaleType.StripChart

                ' go to the nearest even multiple of the spacing size
                Return Me._spacing * Math.Round(axis.Min.Value / Me._spacing)
                'Dim decadeScale As Double = Math.Pow(10, Me.DecimalPlaces(axis))
                'Return Math.Round(decadeScale * axis.Min.Value) / decadeScale

            Case CoordinateScaleType.Log

                ' go to the nearest even multiple of the spacing size

                Return Math.Ceiling((axis.Min.Value.Log10 - 0.00000001))

            Case CoordinateScaleType.Text

                Return 1.0

            Case Else

                Debug.Assert(Not Debugger.IsAttached, String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  "unhandled coordinate scale type {0}", axis.CoordinateScale.CoordinateScaleType))

        End Select

    End Function

    ''' <summary>Determine the ordinals of the first minor tick mark</summary>
    ''' <param name="axis">Reference to the <see cref="Axis"/></param>
    ''' <param name="firstMajorTickValue">The value of the first major tick for the axis.</param>
    ''' <returns>The ordinal position of the first minor tick, relative to the first major tick.
    '''   This value can be negative (e.graphicsDevice., -3 means the first minor tick is 3 minor spacing
    '''   increments before the first major tick.</returns>
    Private Shared Function getFirstMinorTickOrdinal(ByVal axis As Axis, ByVal firstMajorTickValue As Double) As Integer

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        Select Case axis.CoordinateScale.CoordinateScaleType

            Case CoordinateScaleType.Date

                Dim offset As Double = axis.Min.Value - firstMajorTickValue
                Select Case axis.MajorTick.TimeIntervalType
                    Case TimeIntervalType.Year
                        Return Convert.ToInt32(Math.Floor(offset / 365.0))
                    Case TimeIntervalType.Month
                        Return Convert.ToInt32(Math.Floor(offset / 28.0))
                    Case TimeIntervalType.Day
                        Return Convert.ToInt32(Math.Floor(offset))
                    Case TimeIntervalType.Hour
                        Return Convert.ToInt32(Math.Floor(offset * JulianDate.HoursPerDay))
                    Case TimeIntervalType.Minute
                        Return Convert.ToInt32(Math.Floor(offset * JulianDate.MinutesPerDay))
                    Case TimeIntervalType.Second
                        Return Convert.ToInt32(Math.Floor(offset * JulianDate.SecondsPerDay))
                End Select

            Case CoordinateScaleType.Linear, CoordinateScaleType.StripChart

                ' regular linear scale
                Return Convert.ToInt32(Math.Floor((axis.Min.Value - firstMajorTickValue) / axis.MinorTick.Spacing))

            Case CoordinateScaleType.Log

                ' log scale
                Return -9

            Case CoordinateScaleType.Text

                ' text labels (ordinal scale)

                ' This should never happen (no minor tick marks for text labels)
                Return 0

            Case Else

                Debug.Assert(Not Debugger.IsAttached, String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  "unhandled coordinate scale type {0}", axis.CoordinateScale.CoordinateScaleType))

        End Select

    End Function

    ''' <summary>Calculate major tick spacing for a <see cref="CoordinateScaleType.Date"/> scale.
    '''   This method is used by <see cref="Axis.Rescale"/>.</summary>
    ''' <param name="axis">Reference to the <see cref="Axis"/></param>
    ''' <param name="targetSpacingCount">The desired "typical" number of tick spacing
    '''   to divide the range into</param>
    ''' <returns>The calculated spacing for the specified data range.  Also
    '''   calculates and sets the values for <see cref="TimeIntervalType"/>,
    '''   and <see cref="ScaleFormat"/></returns>
    Friend Function getMajorDateSpacing(ByVal axis As Axis, ByVal targetSpacingCount As Integer) As Double

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        ' Calculate an initial guess at spacing
        Dim tempSpacing As Double = axis.Range / targetSpacingCount
        Dim range As Double = axis.Range

        If range > TickDefaults.[Get].RangeYearYear Then

            Me._TimeIntervalType = TimeIntervalType.Year
            Me._ScaleFormat = "&yyyy"
            tempSpacing = Math.Max(Math.Floor((tempSpacing / 365.0)), 1.0)

        ElseIf range > TickDefaults.[Get].RangeYearMonth Then

            Me._TimeIntervalType = TimeIntervalType.Year
            Me._ScaleFormat = "&mmm-&yy"
            tempSpacing = 1.0

        ElseIf range > TickDefaults.[Get].RangeMonthMonth Then

            Me._TimeIntervalType = TimeIntervalType.Month
            Me._ScaleFormat = "&mmm-&yy"
            tempSpacing = Math.Max(Math.Floor((tempSpacing / 30.0)), 1.0)

        ElseIf range > TickDefaults.[Get].RangeDayDay Then

            Me._TimeIntervalType = TimeIntervalType.Day
            ScaleFormat = "&d-&mmm"
            tempSpacing = Math.Max(Math.Floor(tempSpacing), 1.0)

        ElseIf range > TickDefaults.[Get].RangeDayHour Then

            Me._TimeIntervalType = TimeIntervalType.Day
            Me._ScaleFormat = "&d-&mmm &hh:&nn"
            tempSpacing = 1.0

        ElseIf range > TickDefaults.[Get].RangeHourHour Then

            Me._TimeIntervalType = TimeIntervalType.Hour
            Me._ScaleFormat = "&hh:&nn"
            tempSpacing = Tick.SelectHourSpacing(tempSpacing * JulianDate.HoursPerDay)

        ElseIf range > TickDefaults.[Get].RangeHourMinute Then

            Me._TimeIntervalType = TimeIntervalType.Hour
            Me._ScaleFormat = "&hh:&nn"
            tempSpacing = 1.0

        ElseIf range > TickDefaults.[Get].RangeMinuteMinute Then

            Me._TimeIntervalType = TimeIntervalType.Minute
            Me._ScaleFormat = "&hh:&nn"
            tempSpacing = Tick.SelectHourSpacing(tempSpacing * JulianDate.MinutesPerDay)

        ElseIf range > TickDefaults.[Get].RangeMinuteSecond Then

            Me._TimeIntervalType = TimeIntervalType.Minute
            Me._ScaleFormat = "&nn:&ss"
            tempSpacing = 1.0

        Else
            ' SecondSecond
            Me._TimeIntervalType = TimeIntervalType.Second
            Me._ScaleFormat = "&nn:&ss"
            tempSpacing = Tick.SelectSecondSpacing(tempSpacing * JulianDate.SecondsPerDay)

        End If

        Return tempSpacing

    End Function

    ''' <summary>gets the updated tick count for the axis.</summary>
    ''' <history>Fix bug by limiting rounding digits to 0 to 15
    ''' </history>
    Private Function getTickCount(ByVal axis As Axis) As Integer

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        Select Case axis.CoordinateScale.CoordinateScaleType

            Case CoordinateScaleType.Date

                Dim range As Double = axis.Range

                ' Date-Time scale
                Dim year1, year2, month1, month2, day1, day2, hour1, hour2, minute1, minute2, second1, second2, millisecond1, millisecond2 As Integer

                JulianDate.ToCalendarDate(axis.Min.Value, year1, month1, day1, hour1, minute1, second1, millisecond1)
                JulianDate.ToCalendarDate(axis.Max.Value, year2, month2, day2, hour2, minute2, second2, millisecond2)

                Select Case Me._timeIntervalType
                    Case TimeIntervalType.Year
                        Return Math.Max(1, Convert.ToInt32(Math.Floor((year2 - year1 + 1.0) / Me._spacing)))
                    Case TimeIntervalType.Month
                        Return Math.Max(1, Convert.ToInt32(Math.Floor((month2 - month1 + 12.0 * (year2 - year1) + 1.0) / Me._spacing)))
                    Case TimeIntervalType.Day
                        Return Math.Max(1, Convert.ToInt32(Math.Floor((range + 1.0) / Me._spacing)))
                    Case TimeIntervalType.Hour
                        Return Math.Max(1, Convert.ToInt32(Math.Floor(range * JulianDate.HoursPerDay + 1.0)))
                    Case TimeIntervalType.Minute
                        Return Math.Max(1, Convert.ToInt32(Math.Floor(range * JulianDate.MinutesPerDay + 1.0)))
                    Case TimeIntervalType.Second
                        Return Math.Max(1, Convert.ToInt32(Math.Floor(range * JulianDate.SecondsPerDay + 1.0)))
                    Case Else
                        Debug.Assert(Not Debugger.IsAttached, "Unhandled time interval type " & Me._TimeIntervalType.ToString)
                End Select

            Case CoordinateScaleType.Linear

                ' set the tick count
                Dim roundingDigits As Integer = Me.DecimalPlaces(axis) + 2
                roundingDigits = CInt(Math.Max(roundingDigits, 0))
                roundingDigits = CInt(Math.Min(roundingDigits, 15))
                Return Convert.ToInt32(Math.Round(Math.Round(axis.Range, roundingDigits) / Me._spacing) + 1)

            Case CoordinateScaleType.Log

                Dim roundingDigits As Integer = axis.DecimalPlaces + 2
                roundingDigits = CInt(Math.Max(roundingDigits, 0))
                roundingDigits = CInt(Math.Min(roundingDigits, 15))
                Return Convert.ToInt32(Math.Floor(Math.Round(axis.Max.Value, roundingDigits).Log10)) -
                       Convert.ToInt32(Math.Ceiling(Math.Round(axis.Min.Value, roundingDigits).Log10)) + 1

            Case CoordinateScaleType.StripChart

                If Me._isMajor Then
                    Return MajorTickDefaults.[Get].TimeSeriesSpacingCount + 1
                Else
                    Return MinorTickDefaults.[Get].TimeSeriesSpacingCount + 1
                End If

            Case CoordinateScaleType.Text

                ' If no array of labels is available, just assume 10 labels so we don't blow up.
                If axis.TickLabels.TextLabels Is Nothing Then
                    Return 10
                Else
                    Return axis.TickLabels.TextLabels.Length
                End If

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "unhandled coordinate type " & axis.CoordinateScale.CoordinateScaleType.ToString)

        End Select

    End Function

    ''' <summary>Determine the value for any major tick.  Properly
    ''' accounts for <see cref="Coordinatescale.IsLog"/>, <see cref="Coordinatescale.IsText"/>, and other axis format settings.</summary>
    ''' <param name="axis">Reference to the <see cref="Axis"/></param>
    ''' <param name="firstMajorTickValue">The value of the first major tick</param>
    ''' <param name="tickItem">The major tick number (0 = first major tick).  For log scales, this is the 
    '''   actual power of 10.</param>
    ''' <returns>The specified major tick value (floating point double).</returns>
    Private Function getMajorTickValue(ByVal axis As Axis, ByVal firstMajorTickValue As Double, ByVal tickItem As Integer) As Double

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        Select Case axis.CoordinateScale.CoordinateScaleType

            Case CoordinateScaleType.Date

                ' date scale
                Return JulianDate.Add(New JulianDate(firstMajorTickValue),
                                      tickItem * Me._spacing, Me._timeIntervalType).JulianDay

            Case CoordinateScaleType.Linear, CoordinateScaleType.StripChart

                ' regular linear scale
                Return firstMajorTickValue + Me._spacing * tickItem

            Case CoordinateScaleType.Log

                ' log scale
                Return firstMajorTickValue * Math.Pow(10.0, tickItem)

            Case CoordinateScaleType.Text

            Case Else

                Debug.Assert(Not Debugger.IsAttached, String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  "unhandled coordinate scale type {0}", axis.CoordinateScale.CoordinateScaleType))

        End Select

    End Function

    ''' <summary>Calculate minor tick spacing for a <see cref="CoordinateScaleType.Date"/> scale.
    '''   This method is used by <see cref="Axis.Rescale"/>.</summary>
    ''' <param name="axis">Reference to the <see cref="Axis"/></param>
    ''' <param name="targetSpacingCount">The desired "typical" number of tick spacing
    '''   to divide the range into</param>
    ''' <returns>The calculated spacing for the specified data range.  Also
    '''   calculates and sets the values for <see cref="TimeIntervalType"/>,
    '''   and <see cref="ScaleFormat"/></returns>
    Friend Function getMinorDateSpacing(ByVal axis As Axis, ByVal targetSpacingCount As Integer) As Double

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        ' Calculate an initial guess at spacing
        Dim majorspacing As Double = axis.MajorTick.Spacing
        Dim tempSpacing As Double = majorspacing / targetSpacingCount
        Dim range As Double = axis.Range

        If range > TickDefaults.[Get].RangeYearYear Then

            Me._ScaleFormat = "&yyyy"
            Me._TimeIntervalType = TimeIntervalType.Year
            If majorspacing = 1.0 Then
                tempSpacing = 0.25
            Else
                tempSpacing = getSpacing(majorspacing, targetSpacingCount)
            End If

        ElseIf range > TickDefaults.[Get].RangeYearMonth Then

            Me._ScaleFormat = "&mmm-&yy"
            Me._TimeIntervalType = TimeIntervalType.Month

            ' Calculate the minor spacing to give an estimated 4 spacing per major spacing.
            tempSpacing = Tick.SelectMonthSpacing(Math.Ceiling((range / (targetSpacingCount * 3) / 30.0)))

        ElseIf range > TickDefaults.[Get].RangeMonthMonth Then

            Me._ScaleFormat = "&mmm-&yy"
            Me._TimeIntervalType = TimeIntervalType.Month
            tempSpacing = 0.25

        ElseIf range > TickDefaults.[Get].RangeDayDay Then

            Me._ScaleFormat = "&d-&mmm"
            Me._TimeIntervalType = TimeIntervalType.Day
            tempSpacing = 1.0

        ElseIf range > TickDefaults.[Get].RangeDayHour Then

            Me._ScaleFormat = "&d-&mmm &hh:&nn"
            Me._TimeIntervalType = TimeIntervalType.Hour
            ' Calculate the minor spacing to give an estimated 4 spacing per major spacing.
            tempSpacing = Tick.SelectHourSpacing(Math.Ceiling((range / (targetSpacingCount * 3) * JulianDate.HoursPerDay)))
            ' limit to 12 hours
            tempSpacing = Math.Min(tempSpacing, 12)

        ElseIf range > TickDefaults.[Get].RangeHourHour Then

            Me._ScaleFormat = "&hh:&nn"
            Me._TimeIntervalType = TimeIntervalType.Hour
            tempSpacing = 0.25

        ElseIf range > TickDefaults.[Get].RangeHourMinute Then

            Me._ScaleFormat = "&hh:&nn"
            Me._TimeIntervalType = TimeIntervalType.Minute
            ' Calculate the minor spacing to give an estimated 4 spacing per major spacing.
            tempSpacing = Tick.SelectMinuteSpacing(Math.Ceiling((range / (targetSpacingCount * 3) * JulianDate.MinutesPerDay)))

        ElseIf range > TickDefaults.[Get].RangeMinuteMinute Then

            Me._ScaleFormat = "&hh:&nn"
            Me._TimeIntervalType = TimeIntervalType.Minute
            tempSpacing = 0.25

        ElseIf range > TickDefaults.[Get].RangeMinuteSecond Then

            Me._ScaleFormat = "&nn:&ss"
            Me._TimeIntervalType = TimeIntervalType.Second

            ' Calculate the minor spacing to give an estimated 4 spacing per major spacing.
            tempSpacing = Tick.SelectSecondSpacing(Math.Ceiling((range / (targetSpacingCount * 3) * JulianDate.SecondsPerDay)))

        Else
            ' SecondSecond

            Me._ScaleFormat = "&nn:&ss"
            Me._TimeIntervalType = TimeIntervalType.Second
            tempSpacing = 0.25

        End If

        Return tempSpacing

    End Function

    ''' <summary>Determine the value for any minor tick.  Properly
    ''' accounts for <see cref="Coordinatescale.IsLog"/>, <see cref="Coordinatescale.IsText"/>, and other axis format settings.</summary>
    ''' <param name="axis">Reference to the <see cref="Axis"/></param>
    ''' <param name="firstMajorTickValue">The value of the first major tick.  This tick value is the base
    '''   reference for all tick marks (including minor ones).</param>
    ''' <param name="tickItem">The major tick number (0 = first major tick).  For log scales, this is the actual 
    '''   power of 10.</param>
    ''' <returns>The specified minor tick value.</returns>
    Private Function getMinorTickValue(ByVal axis As Axis, ByVal firstMajorTickValue As Double, ByVal tickItem As Integer) As Double

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        Select Case axis.CoordinateScale.CoordinateScaleType

            Case CoordinateScaleType.Date

                ' date scale
                Return JulianDate.Add(New JulianDate(firstMajorTickValue),
                                      tickItem * Me._spacing, Me._timeIntervalType).JulianDay

            Case CoordinateScaleType.Linear, CoordinateScaleType.StripChart

                ' regular linear scale
                Return firstMajorTickValue + Me._spacing * tickItem

            Case CoordinateScaleType.Log

                ' log scale
                Return firstMajorTickValue * Math.Pow(10.0, (tickItem / 9)) * ((tickItem Mod 9) + 1)

            Case CoordinateScaleType.Text

            Case Else

                Debug.Assert(Not Debugger.IsAttached, String.Format(Globalization.CultureInfo.CurrentCulture,
                                                  "unhandled coordinate scale type {0}", axis.CoordinateScale.CoordinateScaleType))

        End Select

    End Function

    ''' <summary>Calculates spacing based on a data range.  This method tries to 
    '''   use the target spacing count of tick spacing while using a rational 
    '''   increment (1, 2, or 5 -- which are even divisors of 10).  This method is
    '''   used by <see cref="Axis.Rescale"/>.</summary>
    ''' <param name="range">The range of data in user scale units.  This can
    '''   be a full range of the data for the major spacing, or just the
    '''   value of the major spacing to calculate the minor spacing</param>
    ''' <param name="targetSpacingCount">The desired "typical" number of tick spacing
    '''   to divide the range into</param>
    ''' <returns>The calculated spacing for the specified data range.</returns>
    Private Shared Function getSpacing(ByVal range As Double, ByVal targetSpacingCount As Double) As Double

        ' Calculate an initial guess at the spacing
        Dim tempSpacing As Double = range / targetSpacingCount

        ' get the magnitude of the spacing
        Dim mag As Double = Math.Floor(Math.Log10(tempSpacing))
        Dim magPow As Double = Math.Pow(10.0R, mag)

        ' Get the standard spacing as close as possible to 1, 2, 5, or 10
        Dim magMsd As Double = Tick.SelectStandardSpacing(tempSpacing / magPow)

        Return magMsd * magPow

    End Function

    ''' <summary>Calculate the scaled tick length for this <see cref="Tick"/></summary>
    ''' <param name="scaleFactor">The scaling factor for the chart with reference
    '''   to the chart <see cref="Pane.BaseDimension"/>.  This scaling factor is 
    '''   calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''   The scale factor is applied to fonts, symbols, etc.</param>		
    ''' <returns>A <see cref="System.Single">Single</see> in pixels</returns>
    Friend Function getScaledLength(ByVal scaleFactor As Double) As Single
        Return Convert.ToSingle(Me._length * scaleFactor + 0.5)
    End Function

    Private _locations As Single()
    ''' <summary>Returns the location of the tick item as calculated by <see cref="M:SetLocations"/></summary>
    ''' <param name="tickItem">The serial tick item for which to return the location</param>
    Friend Function getLocation(ByVal tickItem As Integer) As Single
        Return Me._locations(tickItem)
    End Function

    Private _values As Double()
    ''' <summary>Returns the value of the tick item as calculated by <see cref="M:SetValue"/></summary>
    ''' <param name="tickItem">The serial tick item for which to return the value</param>
    Friend Function getValue(ByVal tickItem As Integer) As Double
        Return Me._values(tickItem)
    End Function

    ''' <summary>Set time series tick locations</summary>
    ''' <param name="axis">Reference to the <see cref="Axis"/></param>
    Private Sub setStripChartLocations(ByVal axis As Axis)

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        ' set the tick spacing
        If Me._isMajor Then

            Me.SetSpacing(axis)
            'axis.MinorTick.SetTickSpacing(axis)
            'call a second time to get it right.
            '_spacing = axis.MinorTick.Spacing * Tick.MinorTickDefaults.TimeSeriesSpacingCount()

            ' allocate space
            Me._locations = New Single(Me._tickCount - 1) {}

            Dim tickSpacing As Integer = Convert.ToInt32(Me._spacing)
            If Me._tickOrigin < 0 Then
                Me._tickOrigin = tickSpacing
            End If
            Dim location As Integer = Me._tickOrigin
            Me._validTickCount = 0

            For i As Integer = 0 To Me._tickCount - 1

                ' set tick location value
                Me._locations(i) = location

                ' limit tick to screen scale range plus minus half a pixel.
                If location < (axis.ScreenScaleRange.Span + 0.5) Then
                    Me._validTickCount = i + 1
                End If

                ' get next tick location
                location += tickSpacing

            Next

        Else

            If Me._visible Then

                ' set the minor tick spacing
                Me.SetSpacing(axis)

                Dim tickSpacing As Integer = Convert.ToInt32(Me._spacing)
                Me._locations = New Single((axis.MajorTick.TickCount - 1) * (Me._tickCount - 1) + 1) {}
                If Me._tickOrigin < 0 Then
                    Me._tickOrigin = tickSpacing
                End If
                Dim tickLocation As Integer = Me._tickOrigin
                For i As Integer = 0 To Me._locations.Length - 1
                    Me._locations(i) = tickLocation
                    tickLocation += tickSpacing
                Next

            End If

        End If

    End Sub

    ''' <summary>Set non-time series tick locations</summary>
    ''' <param name="axis">Reference to the <see cref="Axis"/></param>
    Private Sub SetOtherLocations(ByVal axis As Axis)

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        If Me._isMajor Then

            ' allocate location space
            Me._locations = New Single(Me._tickCount - 1) {}

            ' loop for each major tick
            For i As Integer = 0 To Me._tickCount - 1

                ' convert the value to a pixel position
                Me._locations(i) = axis.LocalTransform(Me._values(i))

            Next

        Else

            ' clear the array
            Me._locations = New Single() {}

            If Me._visible Then

                Dim firstMajorTickvalue As Double
                If axis.MajorTick.TickCount > 0 Then
                    firstMajorTickvalue = axis.MajorTick.getValue(0)
                Else
                    firstMajorTickvalue = 0
                End If

                Dim tMajor As Double = axis.MajorTick.Spacing
                Dim tMinor As Double = Me._spacing
                If axis.CoordinateScale.IsDate Then
                    tMajor *= JulianDate.GetDaysPerTimeInterval(axis.MajorTick.TimeIntervalType)
                    tMinor *= JulianDate.GetDaysPerTimeInterval(Me._timeIntervalType)
                End If

                If axis.CoordinateScale.IsLog OrElse (tMinor < tMajor) Then

                    ' Minor tick marks start at the minimum value and spacing all the way through
                    ' the full scale.  This means that if the minor spacing is not
                    ' an even division of the major spacing size, the minor tick marks won't
                    ' line up with all of the scale labels and major ticks.
                    Dim tickItem As Integer = getFirstMinorTickOrdinal(axis, firstMajorTickvalue)
                    Dim tickValue As Double = getMinorTickValue(axis, firstMajorTickvalue, tickItem)

                    ' update tick count
                    Me._tickCount = Me.getTickCount(axis)

                    ' allocate huge space for ticks
                    Me._locations = New Single(Me._tickCount - 1) {}

                    ' Draw the minor tick marks
                    Dim tolerance As Double = Me._spacing / 2
                    Me._validTickCount = 0
                    While axis.UserScaleRange.Contains(tickValue, tolerance) AndAlso tickItem < Me._tickCount

                        If tickItem >= 0 Then
                            Me._locations(Me._validTickCount) = axis.LocalTransform(tickValue)
                            Me._validTickCount += 1
                        End If
                        tickItem += 1
                        tickValue = getMinorTickValue(axis, firstMajorTickvalue, tickItem)

                    End While

                    ' reshape the array to hold only the set values.
                    ' ReDim Preserve Me._locations(Me._tickCount - 1)

                End If

            End If

        End If

    End Sub

    ''' <summary>Set tick locations</summary>
    ''' <param name="axis">Reference to the <see cref="Axis"/></param>
    Friend Sub SetLocations(ByVal axis As Axis)

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        If axis.CoordinateScale.CoordinateScaleType = CoordinateScaleType.StripChart Then

            setStripChartLocations(axis)

        Else

            SetOtherLocations(axis)

        End If

    End Sub

    ''' <summary>Set the tick values for the axis and time series data (if relevant)</summary>
    ''' <param name="axis">Reference to the <see cref="Axis"/></param>
    Friend Sub SetValues(ByVal axis As Axis)

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        ' update tick counts
        Me._tickCount = Me.getTickCount(axis)

        If Me._tickCount <= 0 Then
            ' if zero, return after setting an empty array.
            Me._values = New Double() {}
            Return
        Else
            Me._values = New Double(Me._tickCount - 1) {}
        End If

        If axis.CoordinateScale.CoordinateScaleType <> CoordinateScaleType.StripChart Then

            If Me._isMajor Then

                Dim tickValue As Double

                ' the range comparison is necessary because due to real value comparisons,
                ' the first and/or last tick values are out of range.
                Dim tolerance As Double = Me._spacing / 100

                ' get the first major tick value
                Dim firstTickValue As Double = getFirstMajorTickValue(axis)

                ' allocate array space
                Me._values = New Double(Me._tickCount - 1) {}

                ' loop for each major tick
                Dim i As Integer = 0
                Do
                    tickValue = getMajorTickValue(axis, firstTickValue, i)
                    If axis.UserScaleRange.Contains(tickValue, tolerance) Then
                        Me._values(i) = tickValue
                        Me._validTickCount = i + 1
                    End If
                    i += 1
                Loop Until i = Me._tickCount

            Else

                ' minor ticks need no values

            End If

        End If

    End Sub

    ''' <summary>Set the tick values for the axis and time series data (if relevant)</summary>
    ''' <param name="axis">Reference to the <see cref="Axis"/></param>
    ''' <param name="timeSeriesPointer">Pointer to the current point in the time series</param>
    ''' <param name="timeSeries">The <see cref="isr.Drawing.TimeSeriesPointR">time series</see> data point</param>
    Friend Sub SetValues(ByVal axis As Axis, ByVal timeSeriesPointer As Integer, ByVal timeSeries() As isr.Drawing.TimeSeriesPointR)

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        ' update tick counts
        Me._tickCount = Me.getTickCount(axis)

        If Me._tickCount <= 0 Then
            ' if zero, return after setting an empty array.
            Me._values = New Double() {}
            Return
        Else
            Me._values = New Double(Me._tickCount - 1) {}
        End If

        If axis.CoordinateScale.CoordinateScaleType = CoordinateScaleType.StripChart Then

            If Me._isMajor Then

                Dim timeSeriesLength As Integer = timeSeries.Length

                ' set the time series tick values
                For i As Integer = 0 To Me._tickCount - 1

                    ' set the location of the time axis division
                    Dim k As Integer = timeSeriesPointer - Convert.ToInt32(axis.ScreenScaleRange.Span - Me._locations(i))
                    If k < 0 Then
                        k += timeSeriesLength
                    End If
                    If k >= timeSeries.Length Then
                        ' when rescaling we may get into a situation were k is out of bounds, in which case
                        ' a temporary shift in time may occur.
                        k = timeSeriesPointer
                    End If
                    Me._values(i) = timeSeries(k).Seconds

                Next

            Else
                ' minor ticks do not need values
            End If

        Else

            Me.SetValues(axis)

        End If

    End Sub

    ''' <summary>Increments the screen position for drawing time series tick marks and grid lines</summary>
    Friend Sub IncrementOrigin()
        Me._tickOrigin -= 1
    End Sub

    ''' <summary>Set the major tick spacing</summary>
    ''' <param name="axis">Specifies reference to the axis</param>
    Friend Sub SetSpacing(ByVal axis As Axis)

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        Select Case axis.CoordinateScale.CoordinateScaleType

            Case CoordinateScaleType.Date

                ' Date Scale.  Calculate the tick spacing
                If Me._autoSpacing Then
                    If Me._isMajor Then
                        Me._spacing = getMajorDateSpacing(axis, MajorTickDefaults.[Get].TargetSpacingCount)
                    Else
                        Me._spacing = getMinorDateSpacing(axis, MinorTickDefaults.[Get].TargetSpacingCount)
                    End If
                End If

            Case CoordinateScaleType.StripChart

                If Me._isMajor Then
                    If axis.ScreenScaleRange Is Nothing Then
                        ' Me._spacing = MajorTickDefaults.Spacing ' .2101
                        Me._spacing = axis.MinorTick.Spacing * MinorTickDefaults.[Get].TimeSeriesSpacingCount()
                    Else
                        Me._spacing = Math.Round(axis.ScreenScaleRange.Span / MajorTickDefaults.[Get].TimeSeriesSpacingCount())
                    End If
                    ' TO_DO: call again with UseWhole
                    ' If axis.MinorTick.Spacing > 0 Then
                    ' Me._spacing = axis.MinorTick.Spacing * Tick.MinorTickDefaults.TimeSeriesSpacingCount()
                    ' End If
                Else
                    Me._spacing = Math.Round(axis.MajorTick.Spacing / MinorTickDefaults.[Get].TimeSeriesSpacingCount)
                End If

            Case CoordinateScaleType.Linear

                ' linear scale:  Calculate tick spacing
                Dim range As Double = axis.Range
                If Me._autoSpacing Then
                    If Me._isMajor Then
                        Me._spacing = Tick.getSpacing(range, MajorTickDefaults.[Get].TargetSpacingCount)
                    Else
                        Me._spacing = Tick.getSpacing(axis.MajorTick.Spacing, MinorTickDefaults.[Get].TargetSpacingCount)
                    End If
                End If

            Case CoordinateScaleType.Log

                ' Log Scale spacing is ignored.  set for the validity check.
                Me._spacing = 1.0

            Case CoordinateScaleType.Text

                ' if this is a text-based axis, then set tick spacing to one
                Me._spacing = 1.0

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "unhandled coordinate scale")

        End Select

        ' get he tick count
        Me._tickCount = Me.getTickCount(axis)

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets the decimal places represented by the tick spacing</summary>
    Friend ReadOnly Property DecimalPlaces(ByVal axis As Axis) As Integer
        Get
            Return Math.Max(axis.DecimalPlaces, Me._spacing.DecimalPlaces)
        End Get
    End Property

    ''' <summary>Determines whether or not the scale spacing <see cref="Spacing"/>
    '''   is set automatically.  This value will be set to false if <see cref="Spacing"/> 
    '''   is manually changed.</summary>
    ''' <value>True for automatic mode or False for manual mode</value>
    Public Property AutoSpacing() As Boolean

    ''' <summary>Determines whether or not the tick marks are shown on the inside
    '''   bottom axis frame.</summary>
    ''' <value>True to show the inside bottom tick marks, false otherwise</value>
    Public Property IsBottom() As Boolean

    ''' <summary>Determines if the <see cref="Tick"/> is major or minor tick mark</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see></value>
    Public Property IsMajor() As Boolean

    ''' <summary>Determines if the <see cref="Tick"/> lines at each labeled value will 
    '''   be drawing</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see></value>
    Public Property Visible() As Boolean

    ''' <summary>Determines whether or not outside tick marks
    '''   are shown.  These are tick marks on the outside of the <see cref="Axis"/> frame.
    '''   The tick spacing is controlled by <see cref="Spacing"/>.</summary>
    ''' <value>True to show outside tick marks, false otherwise</value>
    Public Property IsOutside() As Boolean

    ''' <summary>Determines whether or not inside tick marks
    '''   are shown.  These are tick marks on the inside of the <see cref="Axis"/> frame.
    '''   The tick spacing is controlled by <see cref="Spacing"/>.</summary>
    ''' <value>True to show inside tick marks, false otherwise</value>
    Public Property IsInside() As Boolean

    ''' <summary>Determines whether or not the tick marks are shown on the inside
    '''   top axis frame.</summary>
    ''' <value>True to show the inside top tick marks, false otherwise</value>
    Public Property IsTop() As Boolean

    ''' <summary>The length of the <see cref="Axis"/> tick marks.  This length will be 
    '''   scaled by the <see cref="Pane.getScaleFactor"/> of the chart.</summary>
    ''' <value>A <see cref="System.Single">Single</see> in pixels</value>
    Public Property Length() As Single

    ''' <summary>The color to use for drawing this <see cref="Tick"/>.</summary>
    ''' <value>A <see cref="System.Drawing.Color">Color</see> value</value>
    Public Property LineColor() As Color

    ''' <summary>The pen width to be used when drawing tick marks for this <see cref="Axis"/></summary>
    ''' <value>A <see cref="System.Single">Single</see> in pixels</value>
    Public Property LineWidth() As Single

    ''' <summary>The format of the <see cref="Tick"/> labels. This field is only used if 
    '''   the <see cref="Axis.CoordinateScale"/> is set to <see cref="CoordinateScaleType.Date"/>.</summary>
    ''' <value>A <see cref="System.String"/> as defined for the <see cref="JulianDate.ToString"/> function</value>
    Public Property ScaleFormat() As String

    Private _spacing As Double
    ''' <summary>The scale spacing is the increment between labeled axis values. 
    '''   This value can be set automatically based on the state of <see cref="AutoSpacing"/>.  
    '''   If this value is set manually, then <see cref="AutoSpacing"/> will
    '''   also be set to false.  This value is ignored for <see cref="CoordinateScaleType.Log"/> and
    '''   <see cref="CoordinateScaleType.Text"/> axes.  For <see cref="CoordinateScaleType.Date"/> axes, this
    '''   value is defined by the axis <see cref="Tick.TimeIntervalType"/>.</summary>
    ''' <value>A <see cref="System.Single">Single</see> in user scale units</value>
    Public Property Spacing() As Double
        Get
            Return Me._spacing
        End Get
        Set(ByVal value As Double)
            Me._spacing = value
            Me.AutoSpacing = False
        End Set
    End Property

    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A System.String value.</value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

    Private _tickCount As Integer
    ''' <summary>Returns the tick count</summary>
    Public ReadOnly Property TickCount() As Integer
        Get
            Return Me._tickCount
        End Get
    End Property

    ''' <summary>Gets or sets the first position of the time series tick.</summary>
    Public Property TickOrigin() As Integer

    ''' <summary>The type of time intervals used for the major spacing (<see cref="Spacing"/>).
    '''   Only applies to Date-Time axes (<see cref="CoordinateScaleType.Date"/> = true).
    '''   The axis is set to date type with the <see cref="Type"/> property</summary>
    ''' <value>Enumeration type <see cref="TimeIntervalType"/></value>
    Public Property TimeIntervalType() As TimeIntervalType

    Private _validTickCount As Integer
    ''' <summary>Returns the valid tick count</summary>
    Public ReadOnly Property ValidTickCount() As Integer
        Get
            Return Me._validTickCount
        End Get
    End Property

#End Region

End Class

#Region " DEFAULTS "

''' <summary>A simple subclass of the <see cref="Tick"/> class that defines the
'''   default property values for the <see cref="Tick"/> class.</summary>
Public NotInheritable Class TickDefaults

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs this class.
    ''' This constructor is private to ensure only a single instance of this class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
        Me._RangeYearYear = 5 * JulianDate.GetDaysPerTimeInterval(TimeIntervalType.Year)  ' = 1825  ' 5 years
        Me._RangeYearMonth = JulianDate.GetDaysPerTimeInterval(TimeIntervalType.Year)  ' = 365 ' 1 year
        Me._RangeMonthMonth = 3 * JulianDate.GetDaysPerTimeInterval(TimeIntervalType.Month) ' 90 ' 3 months
        Me._RangeDayDay = 10 ' 10 days
        Me._RangeDayHour = 3 ' 3 days
        Me._RangeHourHour = 10 * JulianDate.GetDaysPerTimeInterval(TimeIntervalType.Hour) ' 0.4167 ' 10 hours
        Me._RangeHourMinute = 3 * JulianDate.GetDaysPerTimeInterval(TimeIntervalType.Hour) ' 0.125 ' 3 hours
        Me._RangeMinuteMinute = 10 * JulianDate.GetDaysPerTimeInterval(TimeIntervalType.Minute)  ' 0.00694 ' 10 Minutes
        Me._RangeMinuteSecond = 3 * JulianDate.GetDaysPerTimeInterval(TimeIntervalType.Minute) ' 0.002083 ' 3 Minutes
        Me._ScaleFormat = "&dd-&mmm-&yy &hh:&nn"
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared instance As TickDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As TickDefaults
        If TickDefaults.instance Is Nothing Then
            SyncLock TickDefaults.syncLocker
                TickDefaults.instance = New TickDefaults()
            End SyncLock
        End If
        Return TickDefaults.instance
    End Function

#End Region

    ''' <summary>A default setting for the <see cref="CoordinateScaleType.Date"/> 
    '''   auto-ranging code in days. If the data span exceeds this value, a selection
    '''   of major <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Year"/>
    '''   and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Year"/>
    '''   will be made.  The default value is 5 years, i.e., 1825 days.</summary>
    Public Property RangeYearYear() As Double

    ''' <summary>A default setting for the <see cref="CoordinateScaleType.Date"/> 
    '''   auto-ranging code in days. If the data span exceeds this value, a selection
    '''   of major <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Year"/>
    '''   and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Month"/>
    '''   will be made.  The default value is 1 year, i.e., 365 days.</summary>
    Public Property RangeYearMonth() As Double

    ''' <summary>A default setting for the <see cref="CoordinateScaleType.Date"/> 
    '''   auto-ranging code in days. If the data span exceeds this value, a selection
    '''   of major <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Month"/>
    '''   and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Month"/>
    '''   will be made.  The default value is 3 months, i.e., 90 days.</summary>
    Public Property RangeMonthMonth() As Double

    ''' <summary>A default setting for the <see cref="CoordinateScaleType.Date"/> 
    '''   auto-ranging code in days. If the data span exceeds this value, a selection
    '''   of major <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Day"/>
    '''   and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Day"/>
    '''   will be made.  The default value is 10 days.</summary>
    Public Property RangeDayDay() As Double

    ''' <summary>A default setting for the <see cref="CoordinateScaleType.Date"/> 
    '''   auto-ranging code in days. If the data span exceeds this value, a selection
    '''   of major <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Day"/>
    '''   and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Hour"/>
    '''   will be made.  The default value is 3 days.</summary>
    Public Property RangeDayHour() As Double

    ''' <summary>A default setting for the <see cref="CoordinateScaleType.Date"/> 
    '''   auto-ranging code in days. If the data span exceeds this value, a selection
    '''   of major <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Hour"/>
    '''   and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Hour"/>
    '''   will be made.  The default value is 10 hours. i.e., 0.4167 days.</summary>
    Public Property RangeHourHour() As Double

    ''' <summary>A default setting for the <see cref="CoordinateScaleType.Date"/> 
    '''   auto-ranging code in days. If the data span exceeds this value, a selection
    '''   of major <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Hour"/>
    '''   and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Minute"/>
    '''   will be made.  The default value is 3 hours, i.e., 0.125 days.</summary>
    Public Property RangeHourMinute() As Double

    ''' <summary>A default setting for the <see cref="CoordinateScaleType.Date"/> 
    '''   auto-ranging code in days. If the data span exceeds this value, a selection
    '''   of major <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Minute"/>
    '''   and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Minute"/>
    '''   will be made.  The default value is 10 minutes, i.e., 0.00694 days.</summary>
    Public Property RangeMinuteMinute() As Double

    ''' <summary>A default setting for the <see cref="CoordinateScaleType.Date"/> 
    '''   auto-ranging code in days. If the data span exceeds this value, a selection
    '''   of major <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Minute"/>
    '''   and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Second"/>
    '''   will be made.  The default value is 3 minutes, i.e., 0.002083 days.</summary>
    Public Property RangeMinuteSecond() As Double

    ''' <summary>Gets or sets the default setting for the <see cref="Axis"/> scale date format string
    '''   (<see cref="Tick.ScaleFormat"/> property).  This value is set as per
    '''   the <see cref="JulianDate.ToString"/> function.
    '''</summary>
    Public Property ScaleFormat() As String

End Class

''' <summary>A simple subclass of the <see cref="Tick"/> class that defines the
'''   default property values for the major tick instance of the 
'''   <see cref="Tick"/> class.</summary>
Public NotInheritable Class MajorTickDefaults

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs this class.
    ''' This constructor is private to ensure only a single instance of this class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
        Me._AutoSpacing = True
        Me._IsBottom = True
        Me._Visible = True
        Me._IsOutside = True
        Me._IsInside = True
        Me._IsTop = True
        Me._Length = 5
        Me._LineWidth = 1.0F
        Me._lineColor = Color.Black
        Me._Spacing = 0.1F
        Me._TargetSpacingCount = 7
        Me._TimeIntervalType = TimeIntervalType.Year
        Me._TimeSeriesSpacingCount = 10
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared instance As MajorTickDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As MajorTickDefaults
        If MajorTickDefaults.instance Is Nothing Then
            SyncLock MajorTickDefaults.syncLocker
                MajorTickDefaults.instance = New MajorTickDefaults()
            End SyncLock
        End If
        Return MajorTickDefaults.instance
    End Function

#End Region

    ''' <summary>Gets or sets the default auto spacing mode for the major <see cref="Tick"/> lines.</summary>
    Public Property AutoSpacing() As Boolean

    ''' <summary>The display mode for the bottom frame major <see cref="Tick"/> marks
    '''   (<see cref="Tick.IsTop"/> property).</summary>
    ''' <value>True to show the tick marks inside the axis on the top frame side,
    '''   False otherwise</value>
    Public Property IsBottom() As Boolean

    ''' <summary>Gets or sets the default display mode for the major <see cref="Tick"/> lines
    '''   (<see cref="Tick.Visible"/> property). True to show the major 
    '''   tick lines, false to hide them.
    '''</summary>
    Public Property Visible() As Boolean

    ''' <summary>The display mode for the major outside <see cref="Tick"/> marks
    '''   (<see cref="Tick.IsOutside"/> property).</summary>
    ''' <value>True to show the major tick marks (outside the axis),
    '''   false otherwise</value>
    Public Property IsOutside() As Boolean

    ''' <summary>The display mode for the major inside <see cref="Tick"/> marks
    '''   (<see cref="Tick.IsInside"/> property).</summary>
    ''' <value>True to show the major tick marks (inside the axis),
    '''   false otherwise</value>
    Public Property IsInside() As Boolean

    ''' <summary>The display mode for the major top frame <see cref="Tick"/> marks
    '''   (<see cref="Tick.IsTop"/> property).</summary>
    ''' <value>True to show the tick marks inside the axis on the top frame side,
    '''   False otherwise</value>
    Public Property IsTop() As Boolean

    ''' <summary>Gets or sets the default length of the major <see cref="Tick"/> marks.
    '''   (<see cref="Tick.Length"/> property).</summary>
    ''' <value>A <see cref="System.Single">Single</see> in pixels</value>
    Public Property Length() As Single

    ''' <summary>Gets or sets the pen width for drawing the major <see cref="Tick"/> marks.</summary>
    ''' <value>A <see cref="System.Single">Single</see> in pixels</value>
    Public Property LineWidth() As Single

    ''' <summary>Gets or sets the default color for drawing the major <see cref="Tick"/> line.</summary>
    ''' <value>A <see cref="System.Drawing.Color">Color</see></value>
    Public Property LineColor() As Color

    ''' <summary>Gets or sets the default spacing of the major <see cref="Tick"/> marks.
    '''   (<see cref="Tick.Spacing"/> property).</summary>
    ''' <value>A <see cref="System.Single">Single</see> in user-defined coordinates</value>
    Public Property Spacing() As Single

    ''' <summary>Gets or sets the default target number of tick spacing for automatically selecting the 
    '''   axis scale spacing (see <see cref="Axis.Rescale"/>). This number is an initial 
    '''   target value for the number of major spacing on an axis.</summary>
    Public Property TargetSpacingCount() As Integer

    ''' <summary>Gets or sets the default time interval for date scale</summary>
    Public Property TimeIntervalType() As TimeIntervalType

    ''' <summary>Gets or sets the default time series spacing.</summary>
    Public Property TimeSeriesSpacingCount() As Integer

End Class

''' <summary>A simple subclass of the <see cref="Tick"/> class that defines the
'''   default property values for the major tick instance of the 
'''   <see cref="Tick"/> class.</summary>
Public NotInheritable Class MinorTickDefaults

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs this class.
    ''' This constructor is private to ensure only a single instance of this class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
        Me._AutoSpacing = True
        Me._IsBottom = True
        Me._Visible = True
        Me._IsOutside = True
        Me._IsInside = True
        Me._IsTop = True
        Me._Length = 3
        Me._LineColor = Color.Black
        Me._LineWidth = 1.0F
        Me._Spacing = 0.1F
        Me._TargetSpacingCount = 5
        Me._TimeIntervalType = TimeIntervalType.Year
        Me._TimeSeriesSpacingCount = 5
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared instance As MinorTickDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As MinorTickDefaults
        If MinorTickDefaults.instance Is Nothing Then
            SyncLock MinorTickDefaults.syncLocker
                MinorTickDefaults.instance = New MinorTickDefaults()
            End SyncLock
        End If
        Return MinorTickDefaults.instance
    End Function

#End Region

    ''' <summary>Gets or sets the default auto spacing mode for the minor <see cref="Tick"/> lines.</summary>
    Public Property AutoSpacing() As Boolean

    ''' <summary>The display mode for the bottom frame minor <see cref="Tick"/> marks
    '''   (<see cref="Tick.IsTop"/> property).</summary>
    ''' <value>True to show the tick marks inside the axis on the top frame side,
    '''   False otherwise</value>
    Public Property IsBottom() As Boolean

    ''' <summary>Gets or sets the default display mode for the minor <see cref="Tick"/> lines
    '''   (<see cref="Tick.Visible"/> property). True to show the minor tick lines, 
    '''   false to hide them.</summary>
    Public Property Visible() As Boolean

    ''' <summary>The display mode for the minor outside <see cref="Tick"/> marks
    '''   (<see cref="Tick.IsOutside"/> property).</summary>
    ''' <value>True to show the minor tick marks (outside the axis),
    '''   false otherwise</value>
    Public Property IsOutside() As Boolean

    ''' <summary>The display mode for the minor inside <see cref="Tick"/> marks
    '''   (<see cref="Tick.IsInside"/> property).</summary>
    ''' <value>True to show the minor tick marks (inside the axis),
    '''   false otherwise</value>
    Public Property IsInside() As Boolean

    ''' <summary>The display mode for the minor top frame <see cref="Tick"/> marks
    '''   (<see cref="Tick.IsTop"/> property).</summary>
    ''' <value>True to show the minor tick marks inside the axis on the top frame side,
    '''   false otherwise</value>
    Public Property IsTop() As Boolean

    ''' <summary>Gets or sets the default length of the minor <see cref="Tick"/> marks.
    '''   (<see cref="Tick.Length"/> property).</summary>
    ''' <value>A <see cref="System.Single">Single</see> in pixels</value>
    Public Property Length() As Single

    ''' <summary>Gets or sets the default color for drawing the minor <see cref="Tick"/> marks.</summary>
    ''' <value>A <see cref="System.Drawing.Color">Color</see></value>
    Public Property LineColor() As Color

    ''' <summary>Gets or sets the default pen width for drawing the minor <see cref="Tick"/> marks.</summary>
    ''' <value>A <see cref="System.Single">Single</see> in pixels</value>
    Public Property LineWidth() As Single

    ''' <summary>Gets or sets the default spacing of the minor <see cref="Tick"/> marks.
    '''   (<see cref="Tick.Spacing"/> property).</summary>
    ''' <value>A <see cref="System.Single">Single</see> in user-defined coordinates.</value>
    Public Property Spacing() As Single

    ''' <summary>Gets or sets the default target number of minor spacing for automatically selecting the 
    '''   axis scale minor spacing (see <see cref="Axis.Rescale"/>).
    '''   This number is an initial target value for the number of minor spacing
    '''   on an axis.</summary>
    Public Property TargetSpacingCount() As Integer

    ''' <summary>Gets or sets the default time interval for date scale</summary>
    Public Property TimeIntervalType() As TimeIntervalType

    ''' <summary>Gets or sets the default time series spacing.</summary>
    Public Property TimeSeriesSpacingCount() As Integer

End Class

#End Region

