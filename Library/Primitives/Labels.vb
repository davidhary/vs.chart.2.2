''' <summary>Handles specification and drawing of axis value labels.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public Class Labels

    Implements ICloneable, IDisposable

    ' update:  with getSpace, allocated an area for the label and use the area to 
    ' display the labels aligned to the axis.
    ' add Tick Label class to handle generating tick labels.

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Default constructor for <see cref="Axis"/> Labels that sets all properties
    '''   to default values as defined in the <see cref="LabelsDefaults"/> class.</summary>
    ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
    Public Sub New(ByVal drawingPane As Pane)

        MyBase.new()
        If drawingPane Is Nothing Then
            Throw New ArgumentNullException("drawingPane")
        End If

        With LabelsDefaults.[Get]
            Me._appearance = New TextAppearance(.Font, .FontColor)
            Me._appearance.Frame.Filled = .Filled
            Me._appearance.Frame.IsOutline = .Framed
            Me._appearance.Frame.Visible = .Framed Or .Filled
            Me._DecimalPlaces = .DecimalPlaces.Copy()
            Me._textLabels = New String() {}
            Me._pane = drawingPane
            Me._ScaleFormat = .ScaleFormat
            Me._Visible = .Visible
        End With

    End Sub

    ''' <summary>Default constructor for <see cref="Axis"/> Labels that sets all properties
    '''   to default values as defined in the <see cref="LabelsDefaults"/> class.</summary>
    ''' <param name="textLabels">The array of text labels</param>
    ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
    Public Sub New(ByVal textLabels() As String, ByVal drawingPane As Pane)

        Me.new(drawingPane)

        If textLabels Is Nothing Then
            Throw New ArgumentNullException("textLabels")
        End If

        Me._textLabels = textLabels

    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The Labels object from which to copy</param>
    Public Sub New(ByVal model As Labels)

        MyBase.new()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Me._appearance = model._appearance.Copy
        Me._decimalPlaces = model._decimalPlaces
        Me._visible = model._visible
        Me._scaleFormat = model._scaleFormat
        If model._textLabels IsNot Nothing Then
            Me._textLabels = CType(model._textLabels.Clone, String())
        End If
        Me._pane = model._pane

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _disposed As Boolean
    ''' <summary>Gets or sets (private) the dispose status sentinel.</summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._disposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._textLabels = Nothing
                    If Me._appearance IsNot Nothing Then
                        Me._appearance.Dispose()
                        Me._appearance = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the Labels</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of Labels</returns>
    Public Function Copy() As Labels
        Return New Labels(Me)
    End Function

    ''' <summary>Renders the specified tick label to the <see cref="graphics"/> device.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    ''' <param name="text">A <see cref="System.String">String</see> value containing the text to be
    '''   displayed.  This can be multiple lines, separated by new line ('\n')
    '''   characters</param>
    ''' <param name="x">The X location to display the text, in screen
    '''   coordinates, relative to the horizontal (<see cref="HorizontalAlignment"/>)
    '''   alignment parameter <paramref name="alignH"/></param>
    ''' <param name="y">The Y location to display the text, in screen
    '''   coordinates, relative to the vertical (<see cref="VerticalAlignment"/>
    '''   alignment parameter <paramref name="alignV"/></param>
    ''' <param name="scaleFactor">The scaling factor to be used for rendering 
    '''   objects.  This is calculated and passed down by the parent 
    '''   <see cref="Pane"/> object using the <see cref="Pane.getScaleFactor"/>
    '''   method, and is used to proportionally adjust font sizes, etc. 
    '''   according to the actual size of the graph.</param>
    ''' <remarks>This method centers the label vertical and horizontally.</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal [text] As String,
                    ByVal x As Single, ByVal y As Single, ByVal scaleFactor As Double)

        If Not Me._visible OrElse String.IsNullOrWhiteSpace(text) Then
            Return
        End If

        ' validate arguments.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        Me._appearance.Draw(graphicsDevice, [text], x, y, HorizontalAlignment.Center, VerticalAlignment.Center, scaleFactor)

    End Sub

    ''' <summary>Draw the major tick labels.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    ''' <param name="scaleFactor">The scaling factor for the chart with reference
    '''   to the chart <see cref="Pane.BaseDimension"/>.  This scaling factor is 
    '''   calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''   The scale factor is applied to fonts, symbols, etc.</param>		
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal axis As Axis, ByVal tick As Tick, ByVal scaleFactor As Double)

        If Not Me._visible Then
            Return
        End If

        ' validate arguments.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If
        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If
        If tick Is Nothing Then
            Throw New ArgumentNullException("tick")
        End If

        Dim scaleMultplier As Double
        Dim textCenter As Single

        ' save the current clip bounds
        Dim clipBounds As RectangleF = graphicsDevice.ClipBounds

        If axis.CoordinateScale.IsStripChart Then

            ' clip to transformed axis area
            Dim labelArea As Rectangle = Rectangle.Round(axis.TransformedArea)
            'graphicsDevice.SetClip(axis.TransformedArea)
            labelArea.Offset(1, 0)
            labelArea.Inflate(-2, 0)
            graphicsDevice.SetClip(labelArea)

            ' get the Y position of the center of the axis labels
            ' (the axis itself is referenced at zero)
            textCenter = -0.75F * Me._pane.AxisArea.Height
            scaleMultplier = 1

        Else
            ' get the Y position of the center of the axis labels
            ' (the axis itself is referenced at zero)
            textCenter = tick.getScaledLength(scaleFactor) + Me.BoundingBox.Height / 2.0F
            scaleMultplier = Math.Pow(10.0R, axis.ScaleExponent.Value)

        End If

        ' loop for each tick
        For i As Integer = 0 To tick.ValidTickCount - 1

            ' draw the label
            Dim tmpStr As String = Me.getTickLabel(i, tick.getValue(i), axis.CoordinateScale, scaleMultplier)
            If Not String.IsNullOrWhiteSpace(tmpStr) Then
                Me.Draw(graphicsDevice, tmpStr, tick.getLocation(i), 0.0F + textCenter, scaleFactor)
            End If

        Next

        ' reset the strip chart clipping area
        If axis.CoordinateScale.IsStripChart Then
            ' restore the clip bounds
            graphicsDevice.SetClip(clipBounds)
        End If

    End Sub

    ''' <summary>Returns a value label at the specified ordinal position.</summary>
    ''' <param name="index">The zero-based, ordinal index of the label.  For example, 
    '''   a value of 2 returns the third value label.</param>
    ''' <param name="tickValue">The numeric value associated with the label.  This value is 
    ''' ignored for log (<see cref="Coordinatescale.IsLog"/>) and 
    ''' text (<see cref="Coordinatescale.IsText"/>) types.</param>
    ''' <param name="coordinateScale">The <see cref="isr.Drawing.CoordinateScale"/> for the axis <see cref="Scale"/></param>
    ''' <param name="ScaleExponent">The <see cref="Scale.ScaleExponent"/> value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Function getTickLabel(ByVal index As Integer, ByVal tickValue As Double,
                                  ByVal coordinateScale As CoordinateScale, ByVal ScaleExponent As Integer) As String

        Return getTickLabel(index, tickValue, coordinateScale, Math.Pow(10.0R, ScaleExponent))

    End Function

    ''' <summary>Returns a value label at the specified ordinal position.</summary>
    ''' <param name="index">The zero-based, ordinal index of the label.  For example, 
    '''   a value of 2 returns the third value label.</param>
    ''' <param name="tickValue">The numeric value associated with the label.  This value is 
    '''   ignored for log (<see cref="Coordinatescale.IsLog"/>) and text (<see cref="Coordinatescale.IsText"/>) types.</param>
    ''' <param name="coordinateScale">The <see cref="isr.Drawing.CoordinateScale"/> for the axis <see cref="Scale"/></param>
    ''' <param name="scaleMultiplier">The 10^<see cref="Scale.ScaleExponent"/> value.</param>
    Private Function getTickLabel(ByVal index As Integer, ByVal tickValue As Double,
                                  ByVal coordinateScale As CoordinateScale, ByVal scaleMultiplier As Double) As String

        Select Case coordinateScale.CoordinateScaleType

            Case CoordinateScaleType.Date

                Return JulianDate.ToString(tickValue, Me._scaleFormat)

            Case CoordinateScaleType.Linear

                Dim tmpStr As String = "{0:F*}"
                tmpStr = tmpStr.Replace("*", Me._decimalPlaces.ToString("D"))
                Return String.Format(Globalization.CultureInfo.CurrentCulture, tmpStr, tickValue / scaleMultiplier)

            Case CoordinateScaleType.Log

                If index >= -3 AndAlso index <= 4 Then
                    Return String.Format(Globalization.CultureInfo.CurrentCulture,
                                         "{0}", Math.Pow(10.0, index))
                Else
                    Return String.Format(Globalization.CultureInfo.CurrentCulture,
                                         "1e{0}", index)
                End If

            Case CoordinateScaleType.StripChart

                Dim timeValue As Date = TimeSeriesPointR.GetDateTime(tickValue)
                Return timeValue.ToString(Me._scaleFormat, Globalization.CultureInfo.CurrentCulture)

            Case CoordinateScaleType.Text

                If Me._textLabels Is Nothing OrElse index < 0 OrElse index >= Me._textLabels.Length Then
                    Return String.Empty
                Else
                    Return Me._textLabels(index)
                End If

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "Unhandled coordinate scale type")
                Return String.Empty

        End Select

    End Function

    ''' <summary>Set the decimal places based on the <see paramref="CoordinateScale"/>.</summary>
    ''' <param name="axis">Reference to the <see cref="Axis"/></param>
    ''' <param name="baseValue">The value to use to figuring auto decimal places.</param>
    ''' <param name="coordinateScale">The <see cref="isr.Drawing.CoordinateScale"/> for the axis <see cref="Scale"/></param>
    ''' <param name="ScaleExponent">The <see cref="Scale.ScaleExponent"/> value.</param>
    Friend Sub SetDecimalPlaces(ByVal axis As Axis, ByVal baseValue As Double, ByVal ScaleExponent As Integer,
                                ByVal coordinateScale As CoordinateScale)

        Select Case coordinateScale.CoordinateScaleType

            Case CoordinateScaleType.Date

                ' Date Scale.  the number of decimal places to display is not used
                Me._decimalPlaces.Value = 0

            Case CoordinateScaleType.Linear

                ' Calculate the appropriate number of decimal places to display if required
                If DecimalPlaces.AutoScale Then
                    Me._DecimalPlaces.Value = Math.Max(axis.DecimalPlaces, baseValue.DecimalPlaces - ScaleExponent)
                End If

            Case CoordinateScaleType.Log

                ' Log Scale
                If Me._decimalPlaces.AutoScale Then
                    ' The number of decimal places to display is not used
                    Me._decimalPlaces.Value = 0
                End If

            Case CoordinateScaleType.StripChart

                ' if this is a strip-chart axis, then ignore as we have ordinal setting
                Me._decimalPlaces.Value = 0

            Case CoordinateScaleType.Text

                ' if this is a text-based axis, then ignore as we have ordinal setting
                Me._decimalPlaces.Value = 0

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "Unhandled coordinate scale type")

        End Select

    End Sub

    ''' <summary>Set the bounding box fitting the maximum size of the scale value text that is 
    '''   required to label the <see cref="Axis"/>. Use <see cref="P:BoundingBox">Bounding Box</see>
    '''   to determine how much space is required for the axis labels.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    ''' <param name="axis">Reference to the <see cref="Axis"/></param>
    ''' <param name="scaleFactor">The scaling factor for the chart with reference
    '''   to the chart <see cref="Pane.BaseDimension"/>.  This scaling factor is 
    '''   calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''   The scale factor is applied to fonts, symbols, etc.</param>		
    Friend Sub SetBoundingBox(ByVal graphicsDevice As Graphics, ByVal axis As Axis, ByVal scaleFactor As Double)

        If axis Is Nothing Then
            Throw New ArgumentNullException("axis")
        End If

        Dim scaleMultplier As Double = Math.Pow(10.0R, axis.ScaleExponent.Value)
        Me._boundingBox = New SizeF(0, 0)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        ' loop for each major tick
        For i As Integer = 0 To axis.MajorTick.TickCount - 1

            ' estimate the label size
            Dim tmpStr As String = Me.getTickLabel(i, axis.MajorTick.getValue(i), axis.CoordinateScale, scaleMultplier)
            Dim sizeF As SizeF = Me.Appearance.BoundingBox(graphicsDevice, tmpStr, scaleFactor)
            If sizeF.Height > boundingBox.Height Then
                Me._boundingBox.Height = sizeF.Height
            End If
            If sizeF.Width > boundingBox.Width Then
                Me._boundingBox.Width = sizeF.Width
            End If

        Next i

    End Sub

#End Region

#Region " PROPERTIES "

    Private _appearance As TextAppearance
    ''' <summary>gets a reference to the <see cref="isr.Drawing.TextAppearance">TextAppearance</see> class used to 
    '''   render the axis value labels</summary>
    ''' <value>A <see cref="isr.Drawing.TextAppearance">TextAppearance</see> instance</value>
    Public ReadOnly Property Appearance() As TextAppearance
        Get
            Return Me._appearance
        End Get
    End Property

    Private _boundingBox As SizeF = New SizeF(0, 0)
    ''' <summary>Gets or sets a <see cref="System.Drawing.SizeF">size</see> structure representing 
    '''   the width and height of the bounding box for the axis label</summary>
    Public ReadOnly Property BoundingBox() As SizeF
        Get
            Return Me._boundingBox
        End Get
    End Property

    ''' <summary>Gets or sets the <see cref="AutoValue"/> for the number of decimal places 
    '''   displayed for axis value labels.  The value can be determined automatically 
    '''   depending on <see cref="AutoValue.AutoScale"/>.</summary>
    ''' <value>The number of decimal places to be displayed for axis value labels</value>
    Public Property DecimalPlaces() As AutoValue

    ''' <summary>Determines if the value <see cref="Labels"/> will be drawn</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see></value>
    Public Property Visible() As Boolean

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></summary>
    Private _pane As Pane

    ''' <summary>The format of the <see cref="Tick"/> labels. This field is only used if 
    '''   the <see cref="Axis.CoordinateScale"/> is set to <see cref="CoordinateScaleType.Date"/>.</summary>
    ''' <value>A <see cref="System.String"/> as defined for the <see cref="JulianDate.ToString"/> function</value>
    Public Property ScaleFormat() As String

    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A System.String value.</value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

    Private _textLabels As String()
    ''' <summary>Gets or sets the <see cref="Axis"/> text labels for a 
    '''   <see cref="CoordinateScaleType.Text"/> axis.</summary>
    Public Function TextLabels() As String()
        Return Me._textLabels
    End Function

#End Region

End Class

#Region " DEFAULTS "

''' <summary>A simple subclass of the <see cref="Labels"/> class that defines the
'''   default property values for the <see cref="Labels"/> class.</summary>
Public NotInheritable Class LabelsDefaults
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs this class.
    ''' This constructor is private to ensure only a single instance of this class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
        Me._Visible = True
        Me._DecimalPlaces = New AutoValue(0, True)
        Me._Font = New Font("Arial", 12, FontStyle.Regular)
        Me._FontColor = Color.Black
        Me._Filled = False
        Me._Framed = False
        Me._ScaleFormat = "&dd-&mmm-&yy &hh:&nn"
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared instance As LabelsDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As LabelsDefaults
        If LabelsDefaults.instance Is Nothing OrElse LabelsDefaults.instance.IsDisposed Then
            SyncLock LabelsDefaults.syncLocker
                LabelsDefaults.instance = New LabelsDefaults()
            End SyncLock
        End If
        Return LabelsDefaults.instance
    End Function

#Region "IDisposable Support"
    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is disposed.
    ''' </summary>
    ''' <value><c>True</c> if this instance is disposed; otherwise, <c>False</c>.</value>
    Private Property IsDisposed As Boolean ' To detect redundant calls

    ''' <summary>
    ''' Releases unmanaged and - optionally - managed resources.
    ''' </summary>
    ''' <param name="disposing"><c>True</c> to release both managed and unmanaged resources; <c>False</c> to release only unmanaged resources.</param>
    Private Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    ' dispose managed state (managed objects).
                    If Me._Font IsNot Nothing Then
                        Me._Font.Dispose()
                        Me._Font = Nothing
                    End If
                End If
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#End Region

    ''' <summary>Gets or sets the default display mode for the axis value <see cref="Labels"/>
    '''   (<see cref="Labels.Visible"/> property). True to show the labels, False 
    '''   to hide them.
    '''</summary>
    Public Property Visible() As Boolean

    ''' <summary>Gets or sets the default decimal places for the axis value <see cref="Labels"/>
    '''   (<see cref="Labels.DecimalPlaces"/> property).</summary>
    Public Property DecimalPlaces() As AutoValue

    ''' <summary>Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    '''   of the <see cref="Title"/> caption</summary>
    Public Property Font() As Font

    ''' <summary>Gets or sets the default font color for the <see cref="Legend"/> entries
    '''   (<see cref="TextAppearance.FontColor"/> property).</summary>
    ''' <value>A <see cref="System.Drawing.Color">Color</see></value>
    Public Property FontColor() As Color

    ''' <summary>Gets or sets the default font filled mode for the <see cref="Axis"/> scale value
    '''   appearance <see cref="TextAppearance.Frame"/>. True
    '''   for filled, False otherwise.
    '''</summary>
    Public Property Filled() As Boolean

    ''' <summary>Gets or sets the default font framed mode for the <see cref="Axis"/> scale value
    '''   appearance <see cref="Labels.Appearance"/>
    '''   (<see cref="TextAppearance.Frame"/> property). True
    '''   for framed, False otherwise.
    '''</summary>
    Public Property Framed() As Boolean

    ''' <summary>Gets or sets the default setting for the <see cref="Axis"/> scale date format string
    '''   (<see cref="Tick.ScaleFormat"/> property).  This value is set as per
    '''   the <see cref="JulianDate.ToString"/> function.
    '''</summary>
    Public Property ScaleFormat() As String

End Class

#End Region

