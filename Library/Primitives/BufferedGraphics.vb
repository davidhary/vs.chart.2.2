''' <summary>Implements Double Buffering rendering of the graph to provide smooth
'''   refreshing.
'''   The double buffer slows down the line recorder significantly.
''' </summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public Class BufferedGraphics

    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="canvasWidth">The canvas width.</param>
    ''' <param name="canvasHeight">The canvas height.</param>
    Public Sub New(ByVal canvasWidth As Integer, ByVal canvasHeight As Integer)
        CreateDoubleBuffer(canvasWidth, canvasHeight)
        '_graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _disposed As Boolean
    ''' <summary>Gets or sets (private) the dispose status sentinel.</summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._disposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me._canvas IsNot Nothing Then
                        Me._canvas.Dispose()
                        Me._canvas = Nothing
                    End If

                    If Me._graphics IsNot Nothing Then
                        Me._graphics.Dispose()
                        Me._graphics = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Creates double buffer object</summary>
    ''' <param name="width">width of paint area</param>
    ''' <param name="height">height of paint area</param>
    ''' <returns>true/false if double buffer is created</returns>
    Public Function CreateDoubleBuffer(ByVal width As Integer, ByVal height As Integer) As Boolean

        If width = 0 OrElse height = 0 Then
            Return False
        End If

        If width <> width OrElse height <> height Then

            If Me._canvas IsNot Nothing Then
                Me._canvas.Dispose()
                Me._canvas = Nothing
            End If

            If Me._graphics IsNot Nothing Then
                Me._graphics.Dispose()
                Me._graphics = Nothing
            End If

            Me._width = width
            Me._height = height

            Me._canvas = New Bitmap(width, height)
            Me._graphics = Graphics.FromImage(Me._canvas)

        End If

        Return True

    End Function

    ''' <summary>Renders the double buffer to the screen</summary>
    ''' <param name="graphicsDevice">Window forms graphics Object</param>
    Public Sub Render(ByVal graphicsDevice As Graphics)
        If graphicsDevice IsNot Nothing AndAlso Me._canvas IsNot Nothing Then
            graphicsDevice.DrawImage(Me._canvas, New Rectangle(0, 0, Me._width, Me._height), 0, 0, Me._width, Me._height, GraphicsUnit.Pixel)
        End If
    End Sub

    ''' <summary>Returns true if double buffering can be achieved</summary>
    Public Function CanDoubleBuffer() As Boolean
        Return Me._graphics IsNot Nothing
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A System.String value.</value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

    Private _canvas As Bitmap
    Private _width As Integer
    Private _height As Integer

    Private _graphics As Graphics
    ''' <summary>Reference to graphics context</summary>
    Public ReadOnly Property GraphicsDevice() As Graphics
        Get
            Return Me._graphics
        End Get
    End Property

#End Region

End Class
