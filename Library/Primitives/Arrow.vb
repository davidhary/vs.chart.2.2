''' <summary>Represents a graphic arrow or Cord object on the graph.  A list of
''' Arrow objects is maintained by the <see cref="ArrowCollection"/> collection class.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public Class Arrow
    Implements ICloneable, IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a instance defining the position of the
    ''' arrow to be pre-specified.  All other properties are set to
    ''' default values</summary>
    ''' <param name="origin">The starting point of the <see cref="Arrow"/>.</param>
    ''' <param name="destination">The ending point of the <see cref="Arrow"/>.</param>
    ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
    Public Sub New(ByVal origin As PointF, ByVal destination As PointF, ByVal drawingPane As Pane)

        MyBase.New()
        If drawingPane Is Nothing Then
            Throw New ArgumentNullException("drawingPane")
        End If

        With ArrowDefaults.[Get]
            Me._LineColor = .LineColor
            Me._ArrowheadSize = .ArrowheadSize
            Me._LineWidth = .LineWidth
            Me._Origin = New PointF(0, 0)
            Me._Destination = New PointF(0.2, 0.2)
            Me._Arrowhead = .Arrowhead
            Me._CoordinateFrame = .CoordinateFrame
        End With

        Me._Origin = origin
        Me._Destination = destination
        Me._pane = drawingPane

    End Sub

    ''' <summary>Constructs a instance defining the position of the
    ''' arrow to be pre-specified.  All other properties are set to
    ''' default values</summary>
    ''' <param name="x1">The x position of the starting point that defines the
    '''   <see cref="Arrow"/>.  The units of this position are specified by the
    '''   <see cref="CoordinateFrame"/> property.</param>
    ''' <param name="y1">The y position of the starting point that defines the
    '''   <see cref="Arrow"/>.  The units of this position are specified by the
    '''   <see cref="CoordinateFrame"/> property.</param>
    ''' <param name="x2">The x position of the ending point that defines the
    '''   <see cref="Arrow"/>.  The units of this position are specified by the
    '''   <see cref="CoordinateFrame"/> property.</param>
    ''' <param name="y2">The y position of the ending point that defines the
    '''   <see cref="Arrow"/>.  The units of this position are specified by the
    '''   <see cref="CoordinateFrame"/> property.</param>
    ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub New(ByVal x1 As Single, ByVal y1 As Single,
                   ByVal x2 As Single, ByVal y2 As Single, ByVal drawingPane As Pane)

        Me.New(New PointF(x1, y1), New PointF(x2, y2), drawingPane)

    End Sub

    ''' <summary>Constructs a instance defining the position, color, and size of the
    ''' <see cref="Arrow"/> to be pre-specified.</summary>
    ''' <param name="lineColor">The line <see cref="System.Drawing.Color">Color</see> for the arrow</param>
    ''' <param name="arrowheadSize">The size of the arrowhead, measured in points.</param>
    ''' <param name="x1">The x position of the starting point that defines the
    '''   arrow.  The units of this position are specified by the
    '''   <see cref="CoordinateFrame"/> property.</param>
    ''' <param name="y1">The y position of the starting point that defines the
    '''   arrow.  The units of this position are specified by the
    '''   <see cref="CoordinateFrame"/> property.</param>
    ''' <param name="x2">The x position of the ending point that defines the
    '''   arrow.  The units of this position are specified by the
    '''   <see cref="CoordinateFrame"/> property.</param>
    ''' <param name="y2">The y position of the ending point that defines the
    '''   arrow.  The units of this position are specified by the
    '''   <see cref="CoordinateFrame"/> property.</param>
    ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub New(ByVal lineColor As Color, ByVal arrowheadSize As Single,
                   ByVal x1 As Single, ByVal y1 As Single,
                   ByVal x2 As Single, ByVal y2 As Single, ByVal drawingPane As Pane)

        Me.New(x1, y1, x2, y2, drawingPane)

        Me._lineColor = lineColor
        Me._arrowheadSize = arrowheadSize

    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The Arrow object from which to copy</param>
    Public Sub New(ByVal model As Arrow)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Me._origin = model._origin
        Me._destination = model._destination
        Me._arrowheadSize = model._arrowheadSize
        Me._lineColor = model._lineColor
        Me._lineWidth = model._lineWidth
        Me._arrowhead = model._arrowhead
        Me._coordinateFrame = model._coordinateFrame
        Me._pane = model._pane

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _disposed As Boolean
    ''' <summary>Gets or sets (private) the dispose status sentinel.</summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._disposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._lineColor = Nothing

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <returns> A new, independent copy of the Arrow. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <returns> A new, independent copy of the Arrow. </returns>
    Public Function Copy() As Arrow
        Return New Arrow(Me)
    End Function

    ''' <summary>Renders this object to the specified <see cref="Graphics"/> device.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.Graphics"/> of the <see cref="M:Paint"/> method.</param>
    Public Sub Draw(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        ' get the he origin of the arrow in screen coordinates
        Dim fromPix As PointF = Me._pane.generalTransform(Me._origin, Me._coordinateFrame)

        Dim toPix As PointF = Me._pane.generalTransform(Me._destination, Me._coordinateFrame)

        If Me._pane.DrawArea.Contains(fromPix) And Me._pane.DrawArea.Contains(toPix) Then

            ' get a scaled size for the arrowhead
            Dim scaledSize As Single = Convert.ToSingle(Me._arrowheadSize * Me._pane.ScaleFactor)

            ' calculate the length and the angle of the arrow "vector"
            Dim dy As Double = toPix.Y - fromPix.Y
            Dim dx As Double = toPix.X - fromPix.X
            Dim angle As Single = Convert.ToSingle(Math.Atan2(dy, dx)) * 180.0F / Convert.ToSingle(Math.PI)
            Dim length As Single = Convert.ToSingle(Math.Sqrt((dx * dx + dy * dy)))

            ' Save the old transform matrix
            Dim transformMatrix As System.Drawing.Drawing2D.Matrix = graphicsDevice.Transform

            ' Move the coordinate system so it is located at the starting point
            ' of this arrow
            graphicsDevice.TranslateTransform(fromPix.X, fromPix.Y)

            ' Rotate the coordinate system according to the angle of this arrow
            ' about the starting point
            graphicsDevice.RotateTransform(angle)

            ' get a pen according to this arrow properties
            Using pen As New Pen(Me._lineColor, Me._lineWidth)
                ' Draw the line segment for this arrow
                graphicsDevice.DrawLine(pen, 0, 0, length, 0)
            End Using

            ' Only show the arrowhead if required
            If Me._arrowhead Then

                Using brush As New SolidBrush(Me._lineColor)
                    ' Create a polygon representing the arrowhead based on the scaled size
                    Dim polyPt(3) As PointF
                    Dim xSize As Single = scaledSize
                    Dim ySize As Single = scaledSize / 3.0F
                    polyPt(0).X = length
                    polyPt(0).Y = 0
                    polyPt(1).X = length - xSize
                    polyPt(1).Y = ySize
                    polyPt(2).X = length - xSize
                    polyPt(2).Y = -ySize
                    polyPt(3) = polyPt(0)

                    ' render the arrowhead
                    graphicsDevice.FillPolygon(brush, polyPt)
                End Using


            End If

            ' Restore the transform matrix back to its original state
            graphicsDevice.Transform = transformMatrix

        Else

            Debug.Assert(Not Debugger.IsAttached, "Arrow out of range")

        End If

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets the size of the arrowhead.
    ''' The display of the arrowhead can be enabled or disabled 
    ''' with the <see cref="ArrowHead"/> property.</summary>
    ''' <value>A <see cref="System.Single">Single</see> in pixels</value>
    Public Property ArrowheadSize() As Single

    ''' <summary>Gets or sets the coordinate system to be used for defining the <see cref="Arrow"/> position</summary>
    ''' <value>A <see cref="CoordinateFrameType"/></value>
    Public Property CoordinateFrame() As CoordinateFrameType

    ''' <summary>Gets or sets the destination or ending point of <see cref="Arrow"/> segment</summary>
    ''' <value>A <see cref="System.Drawing.PointF">Point</see> with units according to the 
    '''   <see cref="coordinateFrame"/></value>
    Public Property Destination() As PointF

    ''' <summary>Determines whether or not to draw an arrowhead</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see> <c>True</c> if an arrowhead is 
    '''   to be drawn, False otherwise.</value>
    Public Property Arrowhead() As Boolean

    ''' <summary>The <see cref="System.Drawing.Color">Color</see> of the arrowhead and line 
    '''   segment</summary>
    ''' <value>A <see cref="System.Drawing.Color">Color</see></value>
    Public Property LineColor() As Color

    ''' <summary>The width of the line segment for the <see cref="Arrow"/></summary>
    ''' <value>A <see cref="System.Single">Single</see> width in pixels</value>
    Public Property LineWidth() As Single

    ''' <summary>Gets or sets the origin or starting point of <see cref="Arrow"/> segment</summary>
    ''' <value>A <see cref="System.Drawing.PointF">Point</see> with units according to the 
    '''   <see cref="coordinateFrame"/></value>
    Public Property Origin() As PointF

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></summary>
    Private _pane As Pane

    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A System.String value.</value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="Arrow" /> class that defines the
''' default property values for the <see cref="Arrow" /> class.
''' </summary>
Public NotInheritable Class ArrowDefaults

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs this class.
    ''' This constructor is private to ensure only a single instance of this class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
        Me._ArrowheadSize = 12.0F
        Me._coordinateFrame = CoordinateFrameType.AxisXYScale
        Me._arrowhead = True
        Me._lineWidth = 1.0F
        Me._lineColor = Color.Red
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared instance As ArrowDefaults

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As ArrowDefaults
        If ArrowDefaults.instance Is Nothing Then
            SyncLock ArrowDefaults.syncLocker
                ArrowDefaults.instance = New ArrowDefaults()
            End SyncLock
        End If
        Return ArrowDefaults.instance
    End Function

#End Region

    ''' <summary>Gets or sets the default size for the <see cref="Arrow"/>.</summary>
    ''' <value>A <see cref="System.Single">Single</see> in pixels</value>
    Public Property ArrowheadSize() As Single

    ''' <summary>Gets or sets the default coordinate system to be used for defining the
    ''' <see cref="Arrow"/> location coordinates
    ''' (<see cref="Arrow.CoordinateFrame"/> property).
    '''</summary>
    ''' <value>A <see cref="CoordinateFrameType"/></value>
    Public Property CoordinateFrame() As CoordinateFrameType

    ''' <summary>Gets or sets the default display mode for the <see cref="Arrow"/> item Arrowhead
    ''' (<see cref="Arrow.ArrowHead"/> property).  true to show the
    ''' arrowhead, false to hide it.
    '''</summary>
    Public Property Arrowhead() As Boolean

    ''' <summary>Gets or sets the default line width used for the <see cref="Arrow"/> line segment
    '''   (<see cref="Arrow.LineWidth"/> property).</summary>
    ''' <value>A <see cref="System.Single">Single</see> in pixels</value>
    Public Property LineWidth() As Single

    ''' <summary>Gets or sets the default color used for the <see cref="Arrow"/> line segment
    ''' and arrowhead (<see cref="Arrow.LineColor"/> property).</summary>
    ''' <value>A <see cref="System.Drawing.Color">Color</see></value>
    Public Property LineColor() As Color

End Class

#End Region

