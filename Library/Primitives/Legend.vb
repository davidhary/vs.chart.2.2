''' <summary>Encapsulates the chart <see cref="Legend"/> that is displayed
''' in the <see cref="Pane"/></summary>
''' <remarks>Use this class to ....</remarks>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public Class Legend

    Implements ICloneable, IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a <see cref="Legend"/> with default values as defined in the 
    '''   <see cref="LegendDefaults"/> class.</summary>
    ''' <param name="drawingPane">Reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></param>
    Public Sub New(ByVal drawingPane As Pane)

        MyBase.new()
        If drawingPane Is Nothing Then
            Throw New ArgumentNullException("drawingPane")
        End If

        With LegendDefaults.[Get]
            Me._Placement = .Placement
            Me._Frame = New Frame
            Me._Frame.IsOutline = .Framed
            Me._Frame.Filled = .Filled
            Me._Frame.Visible = .Filled Or .Framed
            Me._Frame.LineColor = .FrameColor
            Me._Frame.LineWidth = .FrameWidth
            Me._Frame.FillColor = .FillColor

            Me._HorizontalStack = .HorizontalStack
            Me._Visible = .Visible
            Me._appearance = New TextAppearance(.Font, .FontColor)

        End With

        Me._appearance.Frame.Visible = False
        Me._pane = drawingPane

    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The XAxis object from which to copy</param>
    Public Sub New(ByVal model As Legend)

        MyBase.new()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Me._area = model._area
        Me._placement = model._placement
        Me._frame = model._frame.Copy()
        Me._horizontalStack = model._horizontalStack
        Me._visible = model._visible
        Me._appearance = model._appearance.Copy()
        Me._pane = model._pane
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>Gets or sets (private) the dispose status sentinel.</summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me._appearance IsNot Nothing Then
                        Me._appearance.Dispose()
                        Me._appearance = Nothing
                    End If
                    If Me._frame IsNot Nothing Then
                        Me._frame.Dispose()
                        Me._frame = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Private values the determine how legend space is allocated.  
    '''   <code>
    '''     item:     space  Cord  space      text      space<p>
    '''     width:    0.5w    2w   0.5w  Maximum Width  0.5w</p> 
    '''   </code></summary>
    Private _lineLeftMarginCharacterWidth As Single = 0.5
    Private _lineRightMarginCharacterWidth As Single = 0.5
    Private _lineLengthCharacterWidth As Single = 2
    Private _rightMarginCharacterWidth As Single = 0.5
    Private _legendHorizontalgapCharacterWidth As Single = 0.5
    Private _legendVerticalgapCharacterHeight As Single = 0.5

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the Legend</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the Legend</returns>
    Public Function Copy() As Legend
        Return New Legend(Me)
    End Function

    ''' <summary>Renders the <see cref="Legend"/> to the specified <see cref="graphics"/> device
    ''' This method is normally only called by the Draw method
    ''' of the parent <see cref="Pane"/> object.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    Public Sub Draw(ByVal graphicsDevice As Graphics)

        ' if the legend is not visible, do nothing
        If Not Me._visible Then
            Return
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        ' Clip everything to the draw area
        graphicsDevice.SetClip(Me._pane.DrawArea)

        ' draw the frame before drawing the legend so that it appears in the background
        If Me._pane.Curves.Count > 0 Then
            Me._frame.Draw(graphicsDevice, Me._area)
        End If

        ' Set up some scaled dimensions for calculating sizes and locations
        Dim charWidth As Single = Me._appearance.MeasureString(graphicsDevice, "x", Me._pane.ScaleFactor).Width
        Dim charHeight As Single = Me._appearance.ScaledFont.Height
        Dim halfCharHeight As Single = charHeight / 2.0F

        Dim x, y As Single

        ' Loop for each curve in Curve Collection
        For Each curve As Curve In Me._pane.Curves

            ' Calculate the x,y (TopLeft) location of the current curve legend label
            x = Me._area.Left + charWidth * Me._lineLeftMarginCharacterWidth +
                                            ((curve.SerialNumber - 1) Mod Me._Columns) * Me._ColumnWidth
            y = Me._area.Top + Convert.ToSingle(Math.Floor((curve.SerialNumber - 1) / Me._columns)) * charHeight

            ' Draw the legend label for the current curve
            Me._appearance.Draw(graphicsDevice, curve.Label,
                             x + (Me._lineRightMarginCharacterWidth + Me._lineLengthCharacterWidth) * charWidth,
                             y, HorizontalAlignment.Left, VerticalAlignment.Top, Me._pane.ScaleFactor)

            ' Draw a sample curve to the left of the label text
            curve.Cord.Draw(graphicsDevice, x, y + halfCharHeight,
                            x + Me._lineLengthCharacterWidth * charWidth, y + halfCharHeight)

            ' Draw a sample symbol to the left of the label text				
            curve.Symbol.Draw(graphicsDevice, x + charWidth * Me._lineLengthCharacterWidth / 2,
                              y + halfCharHeight, Me._pane.ScaleFactor)

        Next curve

    End Sub

    ''' <summary>Calculates the <see cref="Legend"/> rectangle (<see cref="Area"/>),
    '''   taking into account the number of required legend entries, and the legend 
    '''   drawing preferences.  Adjusts the size of the <see cref="Pane.AxisArea"/>
    '''   for the parent <see cref="Pane"/> to accommodate the space required by the
    '''   legend.  The legends are drawn into equally sized columns that are stacked
    '''   per the <see cref="HorizontalStack"/> property.</summary>
    ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
    '''   <see cref="PaintEventArgs.graphics"/> of the <see cref="M:Paint"/> method.</param>
    ''' <param name="curves">A reference to the <see cref="CurveCollection"/> collection
    '''   of curves for which a legend is required.</param>
    ''' <param name="scaleFactor">The scaling factor for the chart with reference
    '''   to the chart <see cref="Pane.BaseDimension"/>.  This scaling factor is 
    '''   calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''   The scale factor is applied to fonts, symbols, etc.</param>		
    ''' <param name="drawArea">The rectangle that contains the drawing area in pixels. 
    '''   <seealso cref="Pane.DrawArea">DrawArea</seealso></param>
    ''' <param name="axisArea">The rectangle that contains the area bounded by the axes,
    '''   in pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso></param>
    ''' <returns>Adjusted <see cref="Pane.AxisArea"/></returns>
    ''' <seealso cref="Columns"/> <seealso cref="ColumnWidth"/>
    Friend Function getAxisArea(ByVal graphicsDevice As Graphics, ByVal curves As CurveCollection,
                                ByVal scaleFactor As Double, ByVal drawArea As RectangleF,
                                ByVal axisArea As RectangleF) As RectangleF

        ' If the legend is invisible, don't do anything
        If Not Me._visible Then
            Return axisArea
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        ' Start with an empty rectangle
        Me._area = System.Drawing.RectangleF.Empty
        Me._columns = 1
        Me._columnWidth = 1

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If

        Dim charWidth As Single = Me._appearance.MeasureString(graphicsDevice, "x", scaleFactor).Width
        Dim charHeight As Single = Me._appearance.ScaledFont.Height
        Dim verticalgap As Single = charHeight * Me._legendVerticalgapCharacterHeight
        Dim horizontalgap As Single = charWidth * Me._legendHorizontalgapCharacterWidth
        Dim maxLabelWidth As Single = 0
        Dim widthAvailable As Single

        ' Loop through each curve in the curve list
        ' Find the maximum width of the legend labels
        For Each curve As Curve In curves

            ' Calculate the width of the label save the max width
            maxLabelWidth = Math.Max(maxLabelWidth,
                                     Me._appearance.MeasureString(graphicsDevice, curve.Label, scaleFactor).Width)

        Next curve

        ' Is this legend horizontally stacked?
        If Me._horizontalStack Then

            ' Determine the available space for horizontal stacking
            Select Case Me._placement

                Case PlacementType.Right, PlacementType.Left

                    ' Never stack if the legend is to the right or left
                    widthAvailable = 0

                Case PlacementType.TopLeft, PlacementType.TopRight, PlacementType.BottomLeft, PlacementType.BottomRight

                    ' for the top & bottom, the axis frame width is available
                    widthAvailable = axisArea.Width

                Case PlacementType.InsideTopRight, PlacementType.InsideTopLeft, PlacementType.InsideBottomRight, PlacementType.InsideBottomLeft

                    ' for inside the axis area, use 1/2 of the axis frame width
                    widthAvailable = axisArea.Width / 2

                Case Else

                    ' shouldn't ever happen
                    Debug.Assert(Not Debugger.IsAttached, "Invalid legend location option")
                    widthAvailable = 0

            End Select

            ' width of one legend entry
            Me._columnWidth = charWidth * (Me._lineLeftMarginCharacterWidth +
              Me._lineRightMarginCharacterWidth + Me._lineLengthCharacterWidth +
              Me._rightMarginCharacterWidth) + maxLabelWidth

            ' Calculate the number of columns to accommodate all the legends in 
            ' one or more
            If maxLabelWidth > 0 Then
                Me._columns = Convert.ToInt32(Math.Floor(widthAvailable / Me._columnWidth))
            End If
            ' You can never have more columns than legend entries
            If Me._columns > curves.Count Then
                Me._columns = curves.Count
            End If
            ' a safety check
            If Me._columns = 0 Then
                Me._columns = 1
            End If
        Else
            Me._columnWidth = charWidth * (Me._lineLeftMarginCharacterWidth +
              Me._lineRightMarginCharacterWidth + Me._lineLengthCharacterWidth +
              Me._rightMarginCharacterWidth) + maxLabelWidth
        End If

        ' total legend width
        Dim legendWidth As Single = Me._columns * Me._columnWidth

        ' The height of the legend is the actual height of the lines of text
        '   (curves.Count/columns * height) 
        Dim legendHeight As Single = Convert.ToSingle(Math.Ceiling(curves.Count / Me._columns)) * charHeight

        ' Date.Now calculate the legend area based on the above determined parameters
        ' Also, adjust the plotArea and axisArea to reflect the space for the legend
        If curves.Count > 0 Then

            ' The switch statement assigns the left and top edges, and adjusts the axisArea
            ' as required.  The right and bottom edges are calculated at the bottom of the switch.
            Select Case Me._placement

                Case PlacementType.Right

                    Me._area.X = drawArea.Right - legendWidth
                    Me._area.Y = axisArea.Top

                    axisArea.Width -= legendWidth + horizontalgap

                Case PlacementType.TopLeft

                    Me._area.X = axisArea.Left
                    Me._area.Y = axisArea.Top

                    axisArea.Y += legendHeight + verticalgap
                    axisArea.Height -= legendHeight + verticalgap

                Case PlacementType.TopRight

                    Me._area.X = axisArea.Right - legendWidth
                    Me._area.Y = axisArea.Top

                    axisArea.Y += legendHeight + verticalgap
                    axisArea.Height -= legendHeight + verticalgap

                Case PlacementType.BottomLeft

                    Me._area.X = axisArea.Left
                    Me._area.Y = drawArea.Bottom - legendHeight

                    axisArea.Height -= legendHeight + verticalgap

                Case PlacementType.BottomRight

                    Me._area.X = axisArea.Right - legendWidth
                    Me._area.Y = drawArea.Bottom - legendHeight

                    axisArea.Height -= legendHeight + verticalgap

                Case PlacementType.Left

                    Me._area.X = drawArea.Left
                    Me._area.Y = axisArea.Top

                    axisArea.X += legendWidth + horizontalgap
                    axisArea.Width -= legendWidth + horizontalgap

                Case PlacementType.InsideTopRight

                    Me._area.X = axisArea.Right - legendWidth
                    Me._area.Y = axisArea.Top

                Case PlacementType.InsideTopLeft

                    Me._area.X = axisArea.Left
                    Me._area.Y = axisArea.Top

                Case PlacementType.InsideBottomRight

                    Me._area.X = axisArea.Right - legendWidth
                    Me._area.Y = axisArea.Bottom - legendHeight

                Case PlacementType.InsideBottomLeft

                    Me._area.X = axisArea.Left
                    Me._area.Y = axisArea.Bottom - legendHeight

            End Select

            ' Calculate the Right and Bottom edges of the area
            Me._area.Width = legendWidth
            Me._area.Height = legendHeight

        End If

        ' return adjusted axis rectangle
        Return axisArea

    End Function

#End Region

#Region " PROPERTIES "

    Private _appearance As TextAppearance
    ''' <summary>Gets or sets the <see cref="isr.Drawing.TextAppearance">TextAppearance</see> class used to render
    '''   the <see cref="Legend"/> entries</summary>
    ''' <value>A <see cref="isr.Drawing.TextAppearance">TextAppearance</see> instance</value>
    Public ReadOnly Property Appearance() As TextAppearance
        Get
            Return Me._appearance
        End Get
    End Property

    ''' <summary>Gets or sets the number of columns (horizontal stacking) to be used
    '''   for drawing the legend.</summary>
    ''' <value>A <see cref="System.integer">integer</see> value.</value>
    Public Property Columns() As Integer

    ''' <summary>Gets or sets the width of each column in the legend (pixels)</summary>
    ''' <value>A <see cref="System.Single">Single</see> in pixels.</value>
    Public Property ColumnWidth() As Single

    ''' <summary>Gets or sets the <see cref="Legend"/> <see cref="Frame"/>.</summary>
    ''' <value>A <see cref="Frame"/> value</value>
    Public Property Frame() As Frame

    ''' <summary>Gets or sets a property that shows or hides the <see cref="Legend"/> entirely</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see> True to show the 
    '''   <see cref="Legend"/> or False to hide it</value>
    Public Property Visible() As Boolean

    ''' <summary>Gets or sets a property that allows the <see cref="Legend"/> items to
    ''' stack horizontally in addition to the vertical stacking</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see> True to allow horizontal 
    '''   stacking or False otherwise</value>
    Public Property HorizontalStack() As Boolean

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></summary>
    Private _pane As Pane

    ''' <summary>Gets or sets the location of the <see cref="Legend"/> on the
    '''   <see cref="Pane"/> using the <see cref="Placement"/> type</summary>
    ''' <value>A <see cref="PlacementType"/></value>
    Public Property Placement() As PlacementType

    Private _area As RectangleF
    ''' <summary>get the bounding rectangle for the <see cref="Legend"/> in screen coordinates</summary>
    ''' <value>A screen rectangle in pixels</value>
    Public ReadOnly Property Area() As RectangleF
        Get
            Return Me._area
        End Get
    End Property

    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A System.String value.</value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>A simple subclass of the <see cref="legend"/> class that defines the
'''   default property values for the <see cref="isr.Drawing.Legend"/> class.</summary>-----------------------------------------------------------------------------
Public NotInheritable Class LegendDefaults
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs this class.
    ''' This constructor is private to ensure only a single instance of this class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
        Me._FrameWidth = 1
        Me._FrameColor = Color.Black
        Me._FillColor = Color.White
        Me._Placement = PlacementType.TopLeft
        Me._Framed = True
        Me._Visible = True
        Me._Filled = True
        Me._HorizontalStack = True
        Me._Font = New Font("Arial", 12, FontStyle.Regular)
        Me._FontColor = Color.Black
        Me._FontBold = False
        Me._FontItalic = False
        Me._FontUnderline = False
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared instance As LegendDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As LegendDefaults
        If LegendDefaults.instance Is Nothing OrElse LegendDefaults.instance.IsDisposed Then
            SyncLock LegendDefaults.syncLocker
                LegendDefaults.instance = New LegendDefaults()
            End SyncLock
        End If
        Return LegendDefaults.instance
    End Function

#Region "IDisposable Support"
    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is disposed.
    ''' </summary>
    ''' <value><c>True</c> if this instance is disposed; otherwise, <c>False</c>.</value>
    Private Property IsDisposed As Boolean ' To detect redundant calls

    ''' <summary>
    ''' Releases unmanaged and - optionally - managed resources.
    ''' </summary>
    ''' <param name="disposing"><c>True</c> to release both managed and unmanaged resources; <c>False</c> to release only unmanaged resources.</param>
    Private Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    ' dispose managed state (managed objects).
                    If Me._Font IsNot Nothing Then
                        Me._Font.Dispose()
                        Me._Font = Nothing
                    End If
                End If
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

#End Region

    ''' <summary>Gets or sets the default pen width for the <see cref="Legend"/> frame border.
    ''' (<see cref="Legend.Frame"/> property).</summary>
    ''' <value>A <see cref="System.Single">Single</see> in pixels</value>
    Public Property FrameWidth() As Single

    ''' <summary>Gets or sets the default color for the <see cref="Legend"/> frame border.
    '''   (<see cref="Legend.Frame"/> property).</summary>
    ''' <value>A <see cref="System.Drawing.Color">Color</see></value>
    Public Property FrameColor() As Color

    ''' <summary>Gets or sets the default color for the <see cref="Legend"/> background.
    '''   (<see cref="Legend.Frame"/> property).  Use of this
    '''   color depends on the status of the <see cref="Legend.Frame"/>property.</summary>
    ''' <value>A <see cref="System.Drawing.Color">Color</see></value>
    Public Property FillColor() As Color

    ''' <summary>Gets or sets the default placement for the <see cref="Legend"/> on the graph
    '''   (<see cref="Legend.Placement"/> property).  This property is
    '''   defined as a <see cref="isr.Drawing.PlacementType"/> enumeration.
    '''</summary>
    Public Property Placement() As PlacementType

    ''' <summary>Gets or sets the default frame mode for the <see cref="Legend"/>.
    ''' (<see cref="Legend.Frame"/> property). true
    ''' to draw a frame around the <see cref="Legend.Area"/>,
    ''' false otherwise.
    '''</summary>
    Public Property Framed() As Boolean

    ''' <summary>Gets or sets the default display mode for the <see cref="Legend"/>.
    ''' (<see cref="Legend.Visible"/> property). true
    ''' to show the legend,
    ''' false to hide it.
    '''</summary>
    Public Property Visible() As Boolean

    ''' <summary>Gets or sets the default fill mode for the <see cref="Legend"/> background
    ''' (<see cref="Legend.Frame"/> property).
    ''' true to fill-in the background with color,
    ''' false to leave the background transparent.
    '''</summary>
    Public Property Filled() As Boolean

    ''' <summary>Gets or sets the default horizontal stacking mode for the <see cref="Legend"/>
    '''   (<see cref="Legend.HorizontalStack"/> property). True to allow horizontal legend 
    '''   item stacking, false to allow only vertical legend orientation.</summary>
    Public Property HorizontalStack() As Boolean

    ''' <summary>Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    '''   of the <see cref="Title"/> caption</summary>
    Public Property Font() As Font

    ''' <summary>Gets or sets the default font color for the <see cref="Legend"/> entries
    '''   (<see cref="TextAppearance.FontColor"/> property).</summary>
    ''' <value>A <see cref="System.Drawing.Color">Color</see></value>
    Public Property FontColor() As Color

    ''' <summary>Gets or sets the default font bold mode for the <see cref="Legend"/> entries
    '''   (<see cref="TextAppearance.Bold"/> property). true
    ''' for a bold typeface, false otherwise.
    '''</summary>
    Public Property FontBold() As Boolean

    ''' <summary>Gets or sets the default font italic mode for the <see cref="Legend"/> entries
    ''' (<see cref="TextAppearance.Italic"/> property). true
    ''' for an italic typeface, false otherwise.
    '''</summary>
    Public Property FontItalic() As Boolean

    ''' <summary>Gets or sets the default font underline mode for the <see cref="Legend"/> entries
    ''' (<see cref="TextAppearance.IsUnderline"/> property). true
    ''' for an underlined typeface, false otherwise.
    '''</summary>
    Public Property FontUnderline() As Boolean

End Class
#End Region

