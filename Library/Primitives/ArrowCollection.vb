''' <summary>A collection class containing a list of <see cref="isr.Drawing.Arrow"/> type graphic objects
''' to be displayed on the graph.</summary>
''' <remarks>
''' Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the constructor.
''' </remarks>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/04" by="David" revision="1.0.1581.x">
''' Created
''' </history>
Public NotInheritable Class ArrowCollection
  Inherits System.Collections.ObjectModel.Collection(Of Arrow)
  Implements ICloneable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>Default constructor for the collection class</summary>
  Public Sub New()
    MyBase.new()
  End Sub

  ''' <summary>The Copy Constructor</summary>
  ''' <param name="model">The ArrowCollection object from which to copy</param>
  Public Sub New(ByVal model As ArrowCollection)
    MyBase.new()
    If model Is Nothing Then
      Throw New ArgumentNullException("model")
    End If
    Dim item As Arrow
    For Each item In model
      Me.Add(New Arrow(item))
    Next item
    Me._pane = model._pane
  End Sub

#End Region

#Region " CUSTOM COLLECTION METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <returns> A new, independent copy of the ArrowCollection. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

  ''' <summary>Deep-copy clone routine</summary>
  ''' <returns>A new, independent copy of the ArrowCollection</returns>
  Public Function Copy() As ArrowCollection
    Return New ArrowCollection(Me)
  End Function

  ''' <summary>Gets or sets reference to the drawing <see cref="isr.Drawing.Pane">Pane</see></summary>
  Private _pane As Pane

  ''' <summary>Renders all the <see cref="Arrow">Arrows</see> to the
  ''' specified <see cref="Graphics"/> device.</summary>
  ''' <param name="graphicsDevice">Reference to a graphic device to be drawn into.  This is normally 
  '''   <see cref="PaintEventArgs.Graphics"/> of the <see cref="M:Paint"/> method.</param>
  Public Sub Draw(ByVal graphicsDevice As Graphics)

    ' validate argument.
    If graphicsDevice Is Nothing Then
      Throw New ArgumentNullException("graphicsDevice")
    End If

    ' Loop for each curve
    For Each arrow As Arrow In Me
      arrow.Draw(graphicsDevice)
    Next arrow

  End Sub

#End Region

End Class
