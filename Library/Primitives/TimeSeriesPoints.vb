''' <summary>Defines a <see cref="System.Single">Single</see> time series point.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history DateTime="05/12/04" by="David" revision="1.0.1593.x">
''' Created
''' </history>
''' <history DateTime="05/28/04" by="David" revision="1.0.1609.x">
'''   Add time series point tag
''' </history>
''' <history DateTime="06/03/04" by="David" revision="1.0.1615.x">
'''   Add zero and partial copy
''' </history>
Public Structure TimeSeriesPointF

#Region " TYPES "

    ''' <summary>Returns the time from seconds since 1-1-0001</summary>
    ''' <param name="seconds">The time in seconds from 1-1-0001</param>
    ''' <returns>A <see cref="System.DateTime">Time</see> value</returns>
    Public Shared Function GetDateTime(ByVal seconds As Double) As DateTime

        Return New DateTime(Convert.ToInt64(seconds) * TimeSpan.TicksPerSecond)

    End Function

    ''' <summary>gets the rate of change between two time series points.</summary>
    ''' <param name="t1">The first time series point</param>
    ''' <param name="t2">The second time series point</param>
    ''' <returns>The slope of the line between to two time series points</returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="t")>
    Public Shared Function GetRate(ByVal t1 As TimeSeriesPointF, ByVal t2 As TimeSeriesPointF) As Double

        If t1._t.Equals(t2._t) Then
            Return 0.0F
        Else
            Return (t2._y - t1._y) / (t2.Seconds - t1.Seconds)
        End If

    End Function

    ''' <summary>Gets or sets the minimum time series point value</summary>
    ''' <value>A <see cref="TimeSeriesPointF"/> value</value>
    Public Shared ReadOnly Property MinValue() As TimeSeriesPointF
        Get
            Return New TimeSeriesPointF(DateTime.MinValue, Single.MinValue)
        End Get
    End Property

    ''' <summary>Gets or sets the zero time series point value</summary>
    ''' <value>A <see cref="TimeSeriesPointF"/> value</value>
    Public Shared ReadOnly Property Zero() As TimeSeriesPointF
        Get
            Return New TimeSeriesPointF(DateTime.MinValue, 0)
        End Get
    End Property

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a <see cref="TimeSeriesPointF">time series point</see> instance by its t, y, and tag 
    '''   values.</summary>
    ''' <param name="t">A <see cref="System.DateTime">Time</see> value</param>
    ''' <param name="y">A <see cref="System.Single">Single</see> value</param>
    ''' <param name="tag">A <see cref="isr.Drawing.TimeSeriesPointTags">time series point tag</see> value</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="t")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub New(ByVal t As DateTime, ByVal y As Single, ByVal tag As TimeSeriesPointTags)

        Me._t = t
        Me._y = y
        Me._tag = tag

    End Sub

    ''' <summary>Constructs a <see cref="TimeSeriesPointF">time series point</see> instance by its t and y 
    '''   values.</summary>
    ''' <param name="t">A <see cref="System.DateTime">DateTime</see> value</param>
    ''' <param name="y">A <see cref="System.Single">Single</see> value</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="t")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub New(ByVal t As DateTime, ByVal y As Single)

        Me._t = t
        Me._y = y
        Me._tag = TimeSeriesPointTags.None

    End Sub

    ''' <summary>Constructs a <see cref="TimeSeriesPointF">time series point</see> instance by its y 
    '''   value for the current time.</summary>
    ''' <param name="y">A <see cref="System.Single">Single</see> value</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub New(ByVal y As Single)

        Me._t = DateTime.Now
        Me._y = y
        Me._tag = TimeSeriesPointTags.None

    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The <see cref="TimeSeriesPointF">time series point</see> from which to copy</param>
    Public Sub New(ByVal model As TimeSeriesPointF)

        Me._t = model._t
        Me._y = model._y
        Me._tag = model._tag
        Me._index = model._index

    End Sub

    ''' <summary>The partial copy Constructor</summary>
    ''' <param name="model">The <see cref="TimeSeriesPointF">time series point</see> from which to copy</param>
    ''' <param name="y">The new amplitude value</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub New(ByVal model As TimeSeriesPointF, ByVal y As Single)

        Me._t = model._t
        Me._y = y
        Me._tag = model._tag
        Me._index = model._index

    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <param name="left">  Time series point f to be compared. </param>
    ''' <param name="right"> Time series point f to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As TimeSeriesPointF, ByVal right As TimeSeriesPointF) As Boolean
        Return TimeSeriesPointF.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <param name="left">  Time series point f to be compared. </param>
    ''' <param name="right"> Time series point f to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As TimeSeriesPointF, ByVal right As TimeSeriesPointF) As Boolean
        Return Not TimeSeriesPointF.Equals(left, right)
    End Operator

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <param name="left">  Time series point f to be compared. </param>
    ''' <param name="right"> Time series point f to be compared. </param>
    ''' <returns> <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>. </returns>
    Public Overloads Shared Function Equals(ByVal left As TimeSeriesPointF, ByVal right As TimeSeriesPointF) As Boolean
        Return left._T.Equals(right._T) AndAlso left._Y.Equals(right._Y)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <param name="obj"> Another object to compare to. </param>
    ''' <returns> <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean

        If obj IsNot Nothing AndAlso TypeOf obj Is TimeSeriesPointF Then
            Return Equals(CType(obj, TimeSeriesPointF))
        Else
            Return False
        End If

    End Function

    ''' <summary>Returns True if the value of the <param>compared</param> equals to the
    '''   instance value.</summary>
    ''' <param name="compared">The <see cref="TimeSeriesPointF">TimeSeriesPointF</see> to compare for 
    ''' equality with this instance.</param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    ''' <remarks>The two PlanarRanges are the same if the have the same 
    ''' <see cref="T"/> and <see cref="Y"/> ranges.</remarks>
    Public Overloads Function Equals(ByVal compared As TimeSeriesPointF) As Boolean
        Return TimeSeriesPointF.Equals(Me, compared)
    End Function

#End Region

#Region " METHODS "

    ''' <summary>Creates a unique hash code.</summary>
    ''' <returns>An <see cref="System.integer">integer</see> value</returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me._t.GetHashCode Xor Me._y.GetHashCode
    End Function

    ''' <summary>gets the rate of change between this and another time series point.</summary>
    ''' <param name="t">The first time series point</param>
    ''' <returns>The slope of the line between this and the given time series point</returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="t")>
    Public Function Rate(ByVal t As TimeSeriesPointF) As Double

        If Me._t.Equals(t._t) Then
            Return 0.0R
        Else
            Return (Me._y - t._y) / (Me.Seconds - t.Seconds)
        End If

    End Function

    ''' <summary>Returns the default string representation of the time series point.</summary>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                             "[{0},{1}]", Me._t.ToString(Application.CurrentCulture), Me._y.ToString(Application.CurrentCulture))
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets the index of the time series point in a time series array.</summary>
    Public Property Index() As Integer

    ''' <summary>Returns true if the point is a Range point.</summary>
    Public ReadOnly Property IsRangePoint() As Boolean
        Get
            Return (Me._tag And TimeSeriesPointTags.RangePoint) = TimeSeriesPointTags.RangePoint
        End Get
    End Property

    ''' <summary>gets the time value in seconds since 1/1/0001</summary>
    ''' <value>Time value in <see cref="System.Double">double precision</see> seconds</value>
    Public ReadOnly Property Seconds() As Double
        Get
            Return Me._t.Ticks * TimeSeriesPointR.SecondsPerTick
        End Get
    End Property

    ''' <summary>Gets or sets the time series <see cref="TimeSeriesPointTags">tag</see></summary>
    ''' <value>A <see cref="TimeSeriesPointTags">tag</see></value>
    Public Property Tag() As TimeSeriesPointTags

    ''' <summary>Gets or sets the <see cref="TimeSeriesPointF">time series point</see> time value.</summary>
    ''' <value>A <see cref="System.DateTime">Time</see> value</value>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="T")>
    Public Property T() As DateTime

    ''' <summary>Gets or sets the vertical <see cref="TimeSeriesPointF">time series point</see> value.</summary>
    ''' <value>A <see cref="System.Single">Single</see> value</value>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Y")>
    Public Property Y() As Single

#End Region

End Structure

''' <summary> Defines a <see cref="System.Double">Double</see> time series point. </summary>
''' <license> (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history DateTime="05/12/04" by="David" revision="1.0.1593.x"> Created. </history>
''' <history DateTime="05/28/04" by="David" revision="1.0.1609.x">     Add time series point
''' tag. </history>
''' <history DateTime="06/03/04" by="David" revision="1.0.1615.x">     Add zero and partial
''' copy. </history>
Public Structure TimeSeriesPointR

#Region " TYPES "

    ''' <summary> Returns the time from seconds since 1-1-0001. </summary>
    ''' <param name="seconds"> The time in seconds from 1-1-0001. </param>
    ''' <returns> A <see cref="System.DateTime">Time</see> value. </returns>
    Public Shared Function GetDateTime(ByVal seconds As Double) As DateTime

        Return New DateTime(Convert.ToInt64(seconds) * TimeSpan.TicksPerSecond)

    End Function

    ''' <summary>gets the amplitude range for the <see cref="isr.Drawing.TimeSeriesPointR">Time Series</see>
    '''   data array</summary>
    ''' <param name="timeSeries"><see cref="isr.Drawing.TimeSeriesPointR">Time Series</see>
    '''   data array</param>
    Public Shared Function GetAmplitudeRange(ByVal timeSeries() As TimeSeriesPointR) As isr.Drawing.RangeR

        ' return the unit range if no data
        If timeSeries Is Nothing Then
            Return isr.Drawing.RangeR.Unity
        End If

        Dim NumPoints As Integer = timeSeries.Length

        ' initialize the values to the empty range
        Dim yTemp As Double
        yTemp = timeSeries(0).Y
        Dim yMin As Double = yTemp
        Dim yMax As Double = yTemp

        ' Loop over each point in the arrays
        For i As Integer = 0 To NumPoints - 1

            yTemp = timeSeries(i).Y

            If yTemp < yMin Then
                yMin = yTemp
            ElseIf yTemp > yMax Then
                yMax = yTemp
            End If

        Next i

        Return New isr.Drawing.RangeR(yMin, yMax)

    End Function

    ''' <summary>gets the rate of change between two time series points.</summary>
    ''' <param name="t1">The first time series point</param>
    ''' <param name="t2">The second time series point</param>
    ''' <returns>The slope of the line between to two time series points</returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="t")>
    Public Shared Function GetRate(ByVal t1 As TimeSeriesPointR, ByVal t2 As TimeSeriesPointR) As Double

        If t1._T.Equals(t2._T) Then
            Return 0.0R
        Else
            Return (t2._Y - t1._Y) / (t2.Seconds - t1.Seconds)
        End If

    End Function

    ''' <summary>Gets or sets the minimum time series point value</summary>
    ''' <value>A <see cref="TimeSeriesPointF"/> value</value>
    Public Shared ReadOnly Property MinValue() As TimeSeriesPointR
        Get
            Return New TimeSeriesPointR(DateTime.MinValue, Double.MinValue)
        End Get
    End Property

    Private Const _secondsPerTick As Double = 1.0R / TimeSpan.TicksPerSecond
    ''' <summary>Gets or sets the conversion factor from ticks to seconds.</summary>
    ''' <value>A <see cref="System.Double">double precision</see> value</value>
    Public Shared ReadOnly Property SecondsPerTick() As Double
        Get
            Return TimeSeriesPointR._secondsPerTick
        End Get
    End Property

    ''' <summary>Gets or sets the zero time series point value</summary>
    ''' <value>A <see cref="TimeSeriesPointR"/> value</value>
    Public Shared ReadOnly Property Zero() As TimeSeriesPointR
        Get
            Return New TimeSeriesPointR(DateTime.MinValue, 0)
        End Get
    End Property

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a <see cref="TimeSeriesPointF">time series point</see> instance by its t, y, and tag 
    '''   values.</summary>
    ''' <param name="t">A <see cref="System.DateTime">DateTime</see> value</param>
    ''' <param name="y">A <see cref="System.Double">Double</see> value</param>
    ''' <param name="tag">A <see cref="isr.Drawing.TimeSeriesPointTags">time series point tag</see> value</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="t")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub New(ByVal t As DateTime, ByVal y As Double, ByVal tag As TimeSeriesPointTags)

        Me._T = t
        Me._Y = y
        Me._Tag = tag

    End Sub

    ''' <summary>Constructs a <see cref="TimeSeriesPointR">time series point</see> instance by its t and y 
    '''   values.</summary>
    ''' <param name="t">A <see cref="System.DateTime">DateTime</see> value</param>
    ''' <param name="y">A <see cref="System.Double">Double</see> value</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="t")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub New(ByVal t As DateTime, ByVal y As Double)

        Me._T = t
        Me._Y = y
        Me._Tag = TimeSeriesPointTags.None

    End Sub

    ''' <summary>Constructs a <see cref="TimeSeriesPointR">time series point</see> instance by its y 
    '''   value for the current time.</summary>
    ''' <param name="y">A <see cref="System.Double">Double</see> value</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub New(ByVal y As Double)

        Me._T = DateTime.Now
        Me._Y = y
        Me._Tag = TimeSeriesPointTags.None

    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The <see cref="TimeSeriesPointR">time series point</see> from which to copy</param>
    Public Sub New(ByVal model As TimeSeriesPointR)

        Me._T = model._T
        Me._Y = model._Y
        Me._Tag = model._Tag
        Me._Index = model._Index

    End Sub

    ''' <summary>The partial copy Constructor</summary>
    ''' <param name="model">The <see cref="TimeSeriesPointR">time series point</see> from which to copy</param>
    ''' <param name="y">The new amplitude value</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub New(ByVal model As TimeSeriesPointR, ByVal y As Double)

        Me._T = model._T
        Me._Y = y
        Me._Tag = model._Tag
        Me._Index = model._Index

    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <param name="left">  Time series point r to be compared. </param>
    ''' <param name="right"> Time series point r to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As TimeSeriesPointR, ByVal right As TimeSeriesPointR) As Boolean
        Return TimeSeriesPointR.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <param name="left">  Time series point r to be compared. </param>
    ''' <param name="right"> Time series point r to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As TimeSeriesPointR, ByVal right As TimeSeriesPointR) As Boolean
        Return Not TimeSeriesPointR.Equals(left, right)
    End Operator

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <param name="left">  Time series point r to be compared. </param>
    ''' <param name="right"> Time series point r to be compared. </param>
    ''' <returns> <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>. </returns>
    Public Overloads Shared Function Equals(ByVal left As TimeSeriesPointR, ByVal right As TimeSeriesPointR) As Boolean
        Return left._T.Equals(right._T) AndAlso left._Y.Equals(right._Y)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <param name="obj"> Another object to compare to. </param>
    ''' <returns> <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean

        If obj IsNot Nothing AndAlso TypeOf obj Is TimeSeriesPointR Then
            Return Equals(CType(obj, TimeSeriesPointR))
        Else
            Return False
        End If

    End Function

    ''' <summary>Returns True if the value of the <param>compared</param> equals to the
    '''   instance value.</summary>
    ''' <param name="compared">The <see cref="TimeSeriesPointR">TimeSeriesPointR</see> to compare for 
    ''' equality with this instance.</param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    ''' <remarks>The two PlanarRanges are the same if the have the same 
    ''' <see cref="T"/> and <see cref="Y"/> values.</remarks>
    Public Overloads Function Equals(ByVal compared As TimeSeriesPointR) As Boolean
        Return TimeSeriesPointR.Equals(Me, compared)
    End Function

#End Region

#Region " METHODS "

    ''' <summary>Creates a unique hash code.</summary>
    ''' <returns>An <see cref="System.integer">integer</see> value</returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me._T.GetHashCode Xor Me._Y.GetHashCode
    End Function

    ''' <summary>gets the rate of change between this and another time series point.</summary>
    ''' <param name="t">The first time series point</param>
    ''' <returns>The slope of the line between this and the given time series point</returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="t")>
    Public Function Rate(ByVal t As TimeSeriesPointR) As Double

        If Me._T.Equals(t._T) Then
            Return 0.0R
        Else
            Return (Me._Y - t._Y) / (Me.Seconds - t.Seconds)
        End If

    End Function
    ''' <summary>Returns the default string representation of the time series point.</summary>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                             "[{0},{1}]", Me._T.ToString(Application.CurrentCulture), Me._Y.ToString(Application.CurrentCulture))
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets the index of the time series point in a time series array.</summary>
    Public Property Index() As Integer

    ''' <summary>Returns true if the point is a Range point.</summary>
    Public ReadOnly Property IsRangePoint() As Boolean
        Get
            Return (Me._Tag And TimeSeriesPointTags.RangePoint) = TimeSeriesPointTags.RangePoint
        End Get
    End Property

    ''' <summary>gets the time value in seconds since 1/1/0001</summary>
    ''' <value>Time value in <see cref="System.Double">double precision</see> seconds</value>
    Public ReadOnly Property Seconds() As Double
        Get
            Return Me._T.Ticks * TimeSeriesPointR.SecondsPerTick
        End Get
    End Property

    ''' <summary>Gets or sets the time series <see cref="TimeSeriesPointTags">tag</see></summary>
    ''' <value>A <see cref="TimeSeriesPointTags">tag</see></value>
    Public Property Tag() As TimeSeriesPointTags

    ''' <summary>Gets or sets the <see cref="TimeSeriesPointF">time series point</see> time value.</summary>
    ''' <value>A <see cref="System.DateTime">Time</see> value</value>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="T")>
    Public Property T() As DateTime

    ''' <summary>Gets or sets the vertical <see cref="TimeSeriesPointR">time series point</see> value.</summary>
    ''' <value>A <see cref="System.Double">Double</see> value</value>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Y")>
    Public Property Y() As Double

#End Region

End Structure

#Region " TYPES "

''' <summary>Enumerates the available time series tags.</summary>
'''   <seealso cref="isr.Drawing.Axis.AxisType"/>
<System.Flags()>
Public Enum TimeSeriesPointTags

    ''' <summary>No tag</summary>
    None = 0

    ''' <summary>Tags a time series point as a range point.  Only points tagged as
    '''   range points as selected when setting the range for auto scale.</summary>
    RangePoint = 1

End Enum

#End Region

''' <summary>Defines a <see cref="System.Double">double precision</see> time series book mark.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history DateTime="05/12/04" by="David" revision="1.0.1593.x">
''' Created
''' </history>
Public Structure TimeSeriesBookmark

#Region " Constructors "

    ''' <summary>Constructs a <see cref="TimeSeriesBookmark"/> instance by its 
    '''   <see cref="TimeSeriesPointR">time series point</see>, index,
    '''   and caption.</summary>
    ''' <param name="mainPoint">The main book mark point <see cref="TimeSeriesPointR">time series point</see></param>
    ''' <param name="fromPoint">The starting <see cref="TimeSeriesPointR">time series point</see></param>
    ''' <param name="toPoint">The ending <see cref="TimeSeriesPointR">time series point</see></param>
    ''' <param name="caption">The book mark caption</param>
    ''' <param name="tag">The book mark tag</param>
    Public Sub New(ByVal mainPoint As TimeSeriesPointR, ByVal fromPoint As TimeSeriesPointR, ByVal toPoint As TimeSeriesPointR,
                   ByVal caption As String, ByVal tag As String)

        If String.IsNullOrWhiteSpace(caption) Then
            Throw New ArgumentNullException("caption")
        End If
        If String.IsNullOrWhiteSpace(tag) Then
            Throw New ArgumentNullException("tag")
        End If

        Me._mainPoint = mainPoint
        Me._fromPoint = fromPoint
        Me._toPoint = toPoint
        Me._tag = tag
        Me._caption = caption

    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The <see cref="TimeSeriesBookmark">time series book mark</see> from which to copy</param>
    Public Sub New(ByVal model As TimeSeriesBookmark)

        Me._mainPoint = model._mainPoint
        Me._fromPoint = model._fromPoint
        Me._toPoint = model._toPoint
        Me._caption = model._caption
        Me._tag = model._tag
        Me._serialNumber = model._serialNumber

    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <param name="left">  Time series bookmark to be compared. </param>
    ''' <param name="right"> Time series bookmark to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As TimeSeriesBookmark, ByVal right As TimeSeriesBookmark) As Boolean
        Return TimeSeriesBookmark.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <param name="left">  Time series bookmark to be compared. </param>
    ''' <param name="right"> Time series bookmark to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As TimeSeriesBookmark, ByVal right As TimeSeriesBookmark) As Boolean
        Return Not TimeSeriesBookmark.Equals(left, right)
    End Operator

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <param name="left">  Time series bookmark to be compared. </param>
    ''' <param name="right"> Time series bookmark to be compared. </param>
    ''' <returns> <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>. </returns>
    Public Overloads Shared Function Equals(ByVal left As TimeSeriesBookmark, ByVal right As TimeSeriesBookmark) As Boolean
        Return left._FromPoint.Equals(right._FromPoint) AndAlso
               left._MainPoint.Equals(right._MainPoint) AndAlso
               left._ToPoint.Equals(right._ToPoint) AndAlso
               left._Caption.Equals(right._Caption) AndAlso
               left._SerialNumber.Equals(right._SerialNumber) AndAlso
               left._Tag.Equals(right._Tag)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <param name="obj"> Another object to compare to. </param>
    ''' <returns> <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean

        If obj IsNot Nothing AndAlso TypeOf obj Is TimeSeriesBookmark Then
            Return Equals(CType(obj, TimeSeriesBookmark))
        Else
            Return False
        End If

    End Function

    ''' <summary>Returns True if the value of the <param>compared</param> equals to the
    '''   instance value.</summary>
    ''' <param name="compared">The <see cref="TimeSeriesBookmark">TimeSeriesBookmark</see> to compare for 
    ''' equality with this instance.</param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    ''' <remarks>
    ''' Time Series Marks are the same if the have the same 
    ''' <see cref="FromPoint"/> and <see cref="MainPoint"/> and <see cref="ToPoint"/> 
    ''' and <see cref="Caption"/> and <see cref="SerialNumber"/> and <see cref="Tag"/> values.
    ''' </remarks>
    Public Overloads Function Equals(ByVal compared As TimeSeriesBookmark) As Boolean
        Return TimeSeriesBookmark.Equals(Me, compared)
    End Function

#End Region

#Region " METHODS "

    ''' <summary>Creates a unique hash code.</summary>
    ''' <returns>An <see cref="System.integer">integer</see> value</returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me._MainPoint.GetHashCode Xor Me._FromPoint.GetHashCode Xor Me._ToPoint.GetHashCode Xor
               Me._Caption.GetHashCode Xor Me._SerialNumber.GetHashCode Xor Me._Tag.GetHashCode
    End Function

    ''' <summary>Returns the time series index of the bookmark.</summary>
    ''' <param name="timeSeries">The <see cref="TimeSeriesBookmark">time series point</see> array</param>
    ''' <returns>An <see cref="System.integer">integer</see> value</returns>
    Public Function FindIndex(ByVal timeSeries() As TimeSeriesPointR) As Integer

        Return Array.IndexOf(timeSeries, Me._fromPoint)

    End Function

    ''' <summary>Returns the default string representation of the time series book mark.</summary>
    Public Overloads Overrides Function ToString() As String
        If Me._caption.Length > 0 Then
            Return String.Format(Globalization.CultureInfo.CurrentCulture,
                                 "{0}: {1}", Me._caption, Me._fromPoint.T.ToString(Application.CurrentCulture))
        Else
            Return Me._fromPoint.T.ToString(Application.CurrentCulture)
        End If
    End Function

    ''' <summary>Returns the default string representation of the time series book mark.</summary>
    Public Overloads Function ToString(ByVal dateFormat As String) As String
        If String.IsNullOrWhiteSpace(dateFormat) Then
            Throw New ArgumentNullException("dateFormat")
        End If
        If String.IsNullOrWhiteSpace(Me._caption) Then
            Return Me._fromPoint.T.ToString(Application.CurrentCulture)
        Else
            Return String.Format(Globalization.CultureInfo.CurrentCulture,
                                 "{0}: {1}", Me._caption, Me._fromPoint.T.ToString(dateFormat, Application.CurrentCulture))
        End If
    End Function

#End Region

#Region " Properties"

    ''' <summary>Gets or sets the Bookmark caption.</summary>
    ''' <value>A <see cref="String"/> value</value>
    Public Property Caption() As String

    ''' <summary>Gets or sets the start <see cref="TimeSeriesPointR">time series point</see> of 
    '''   this Bookmark</summary>
    ''' <value>A <see cref="TimeSeriesPointR">time series point</see> value</value>
    Public Property FromPoint() As TimeSeriesPointR

    ''' <summary>Gets or sets the main <see cref="TimeSeriesPointR">time series point</see> of 
    '''   this Bookmark</summary>
    ''' <value>A <see cref="TimeSeriesPointR">time series point</see> value</value>
    Public Property MainPoint() As TimeSeriesPointR

    ''' <summary>Gets or sets the book mark serial number in the book mark collection.</summary>
    Public Property SerialNumber() As Integer

    ''' <summary>Gets or sets the Bookmark tag.</summary>
    ''' <value>A <see cref="String"/> value</value>
    Public Property Tag() As String

    ''' <summary>Gets or sets the end <see cref="TimeSeriesPointR">time series point</see> of 
    '''   this Bookmark</summary>
    ''' <value>A <see cref="TimeSeriesPointR">time series point</see> value</value>
    Public Property ToPoint() As TimeSeriesPointR

#End Region

End Structure