''' <summary>Provides a user interface to the <see cref="isr.Drawing.ChartPane"/> 
'''   class library.  This allows the to be installed as a control in the Visual Studio 
'''   toolbox.  You can use the control by simply dragging it onto a form in the Visual 
'''   Studio form editor.  All graph attributes are accessible via the 
'''   <see cref="isr.Drawing.ChartPane"/>
''' property.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
<Description("Chart Control - Windows Forms Custom Control"), System.Drawing.ToolboxBitmap(GetType(isr.Drawing.ChartControl))>
Public Class ChartControl

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ChartControl" /> class.
    ''' </summary>
    Public Sub New()

        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        Dim area As New RectangleF(0, 0, Me.Size.Width, Me.Size.Height)
        Me._ChartPane = New ChartPane(area)
        Me._ChartPane.Rescale()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

        ' The window message WM_ERASEBKGND is ignored, and both OnPaintBackground and OnPaint methods 
        ' are called directly from the window message WM_PAINT. This generally reduces 
        ' flicker unless other controls send the window message WM_ERASEBKGND to the control. 
        ' Should only be set if User Paint is set.
        MyBase.SetStyle(System.Windows.Forms.ControlStyles.AllPaintingInWmPaint, True)

        ' improves performance but makes it difficult to keep text synchronized
        MyBase.SetStyle(System.Windows.Forms.ControlStyles.CacheText, True)

        ' The Window.Forms framework offers support for double buffering to avoid 
        ' flickers through ControlStyles. Double buffering is a technique that attempts
        ' to reduce flicker by doing all the drawing operations on an off-screen canvas,
        ' and then exposing this canvas all at once. To turn on a control's double 
        ' buffering, requires settings of three styles: UserPaint, AllPaintingInWmPaint, and
        ' DoubleBuffer to true.
        ' If true, drawing is performed in a buffer, and after it completes, the result is 
        ' output to the screen. Double-buffering prevents flicker caused by the redrawing 
        ' of the control. To fully enable double-buffering, you must also set the 
        ' UserPaint and AllPaintingInWmPaint style bits to true.
        MyBase.SetStyle(System.Windows.Forms.ControlStyles.DoubleBuffer, True)

        ' The control is drawn opaque and the background is not painted
        MyBase.SetStyle(System.Windows.Forms.ControlStyles.Opaque, True)

        ' The control is redrawing when it is resized.
        MyBase.SetStyle(System.Windows.Forms.ControlStyles.ResizeRedraw, True)

        ' UserPaint causes the control to paints itself rather than the operating system 
        ' doing so. This style only applies to classes derived from Control.
        MyBase.SetStyle(System.Windows.Forms.ControlStyles.UserPaint, True)

        MyBase.UpdateStyles()

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the status message generated by the object.</remarks>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

    ''' <summary>Gets or sets a reference to the <see cref="isr.Drawing.ChartPane">ChartPane</see> 
    '''   instance of this control.</summary>
    ''' <value>A reference to the <see cref="isr.Drawing.ChartPane">ChartPane</see></value>
    Public Property ChartPane() As isr.Drawing.ChartPane

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>Called by the system to update the control on-screen</summary>
    ''' <param name="e">Reference to the <see cref="PaintEventArgs"/> containing the 
    '''   graphics context for this event.</param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e IsNot Nothing Then
            MyBase.OnPaint(e)

            ' draw the chart.
            Me._chartPane.Draw(e.Graphics)
        End If
    End Sub

    ''' <summary>Called when the control has been resized.</summary>
    ''' <param name="e">Reference to the <see cref="PaintEventArgs"/> containing the 
    '''   graphics context for this event.</param>
    Protected Overrides Sub OnResize(ByVal e As System.EventArgs)

        ' Me._chartPane.PaneArea = New RectangleF(0, 0, Me.Size.Width, Me.Size.Height)
        Me._chartPane.SetSize(Me.ClientRectangle)

        MyBase.OnResize(e)

    End Sub

#End Region

End Class
