Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Permissions

' general Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyCompany("Integrated Scientific Resources")> 
<Assembly: AssemblyCopyright("(c) 2004 Integrated Scientific Resources, Inc. All rights reserved.")>  
<Assembly: AssemblyTrademark("Licensed under The MIT License.")>
<Assembly: Resources.NeutralResourcesLanguage("en-US", Resources.UltimateResourceFallbackLocation.MainAssembly)> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
<Assembly: AssemblyVersion("2.2.*")> 
