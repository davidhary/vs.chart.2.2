
#Region " TYPES "

''' <summary>Enumerates the available axis types.</summary>
'''   <seealso cref="isr.Drawing.Axis.AxisType"/>
Public Enum AxisType

    ''' <summary>An ordinary horizontal Axis</summary>
    <System.ComponentModel.Description("X")> X

    ''' <summary>An ordinary vertical axis</summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Y")>
        <System.ComponentModel.Description("Y")> Y

    ''' <summary>A right vertical axis</summary>
    <System.ComponentModel.Description("Y2")> Y2

End Enum

''' <summary>Enumerates the available coordinate frame types.
'''   to use when defining the location of chart items such as <see cref="Arrow"/> 
'''   and <see cref="TextBox"/> objects. <seealso cref="TextBox.CoordinateFrame"/>
'''   <seealso cref="Arrow.CoordinateFrame"/></summary>
Public Enum CoordinateFrameType

    ''' <summary>Coordinates are specified as a fraction of the <see cref="Pane.AxisArea"/>.  
    '''   That is, for the X coordinate, 0.0 is at the left edge of the <see cref="Pane.AxisArea"/> and 1.0
    '''   is at the right edge of the AxisArea. A value less than zero is left of the 
    '''   <see cref="Pane.AxisArea"/> and a value greater than 1.0 is right of the AxisArea.</summary>
    <System.ComponentModel.Description("Axis Fraction")> AxisFraction

    ''' <summary>Coordinates are specified as a fraction of the <see cref="Pane.PaneArea"/>. 
    '''   That is, for the X coordinate, 0.0 is at the left edge of the PaneArea and 1.0
    '''   is at the right edge of the PaneArea. A value less than zero is left of the 
    '''   PaneArea and a value greater than 1.0 is right of the PaneArea.  Note that
    '''   any value less than zero or greater than 1.0 will be outside the PaneArea, and
    '''   therefore clipped.</summary>
    <System.ComponentModel.Description("Pane Fraction")> PaneFraction

    ''' <summary>Coordinates are specified according to the axis scales
    '''   for the <see cref="Pane"/> X and Y axes.</summary>
    <System.ComponentModel.Description("X Y Axis Scale")> AxisXYScale

    ''' <summary>Coordinates are specified according to the user axis scales
    '''   for the <see cref="Pane"/> X and Y2 Axes.</summary>
    <System.ComponentModel.Description("X Y2 Axis Scale")> AxisXY2Scale

End Enum

''' <summary>Enumerates the available coordinate scale types.</summary>
'''   <seealso cref="isr.Drawing.Scale.CoordinateScale"/>
Public Enum CoordinateScaleType

    ''' <summary>An ordinary, Cartesian axis</summary>
    <System.ComponentModel.Description("Linear")> Linear

    ''' <summary>A base 10 log axis</summary>
    <System.ComponentModel.Description("Logarithmic")> Log

    ''' <summary>A Cartesian axis with calendar dates or times</summary>
    <System.ComponentModel.Description("Date")> [Date]

    ''' <summary>An ordinal axis with user-defined text labels</summary>
    <System.ComponentModel.Description("Text")> [Text]

    ''' <summary>Strip chart coordinate scale has a flexible range depending on the time 
    '''   series data collected thus far.</summary>
    <System.ComponentModel.Description("Strip Chart")> StripChart

End Enum

''' <summary>Enumeration type that defines how a cord is drawn.  Cords can be drawn
'''   as ordinary lines by connecting the points directly, or in a stair-step
'''   fashion as a series of discrete, constant values.  In a stair step plot,
'''   all lines segments are either horizontal (hold) or vertical (step).  
'''   In a Linear (non-step or line) plot, the lines can be any angle.</summary>
''' <seealso cref="Cord.CordType"/>
Public Enum CordType

    ''' <summary>Draw each <see cref="Cord"/> of the <see cref="Curve"/> as a stair-step in which each
    '''   point defines the beginning (left side) of a new stair.  To connect to the
    '''   next point the line first steps and then holds.  This implies that 
    '''   points are defined at the beginning of an "event."</summary>
    <System.ComponentModel.Description("Step Hold")> StepHold

    ''' <summary>Draw each <see cref="Cord"/> of the <see cref="Curve"/> as a stair-step in which each
    '''   point defines the end (right side) of a new stair.  To connect to the
    '''   next point the line first holds and then steps.  This implies
    '''   the points are defined at the end of an "event."</summary>
    <System.ComponentModel.Description("Hold Step")> HoldStep

    ''' <summary>Draw each <see cref="Cord"/> of the <see cref="Curve"/> as an 
    '''   line, in which the points are connected directly by line segments.</summary>
    <System.ComponentModel.Description("Linear")> Linear

End Enum

''' <summary>Enumerates the available curve types.</summary>
'''   <seealso cref="isr.Drawing.Curve.CurveType"/>
''' <remarks>Update: add histogram type.</remarks>
Public Enum CurveType

    ''' <summary>An ordinary line curve</summary>
    <System.ComponentModel.Description("OXY Line")> XY

    ''' <summary>A strip chart</summary>
    <System.ComponentModel.Description("Strip Chart")> StripChart

End Enum

''' <summary>Enumerates the horizontal alignment options</summary>
Public Enum HorizontalAlignment

    ''' <summary>Position item so that its left edge is aligned with the
    '''   specified location.</summary>
    <System.ComponentModel.Description("Left")> Left

    ''' <summary>Position item so that its center is aligned (horizontally) with the
    '''   specified location.</summary>
    <System.ComponentModel.Description("Center")> Center

    ''' <summary>Position item so that its right edge is aligned with the
    '''   specified location.</summary>
    <System.ComponentModel.Description("Right")> Right

End Enum

''' <summary>Enumeration type that defines the possible legend placements</summary>
''' <seealso cref="Legend.Placement"/>
Public Enum PlacementType

    ''' <summary>Locate the <see cref="Legend"/> on the left side of the <see cref="Pane.AxisArea"/></summary>
    <System.ComponentModel.Description("Left")> Left

    ''' <summary>Locate the <see cref="Legend"/> on the right side of the <see cref="Pane.AxisArea"/></summary>
    <System.ComponentModel.Description("Right")> Right

    ''' <summary>Locate the <see cref="Legend"/> above and to the left of the 
    '''   <see cref="Pane.AxisArea"/></summary>
    <System.ComponentModel.Description("Top Left")> TopLeft

    ''' <summary>Locate the <see cref="Legend"/> above and to the right of the 
    '''   <see cref="Pane.AxisArea"/></summary>
    <System.ComponentModel.Description("Top Right")> TopRight

    ''' <summary>Locate the <see cref="Legend"/> below and to the left of the 
    '''   <see cref="Pane.AxisArea"/></summary>
    <System.ComponentModel.Description("Bottom Left")> BottomLeft

    ''' <summary>Locate the <see cref="Legend"/> below and to the right of the 
    '''   <see cref="Pane.AxisArea"/></summary>
    <System.ComponentModel.Description("Bottom Right")> BottomRight

    ''' <summary>Locate the <see cref="Legend"/> inside the <see cref="Pane.AxisArea"/> in the
    ''' top-left corner</summary>
    <System.ComponentModel.Description("Inside Top Left")> InsideTopLeft

    ''' <summary>Locate the <see cref="Legend"/> inside the <see cref="Pane.AxisArea"/> in the
    ''' top-right corner</summary>
    <System.ComponentModel.Description("Inside Top Right")> InsideTopRight

    ''' <summary>Locate the <see cref="Legend"/> inside the <see cref="Pane.AxisArea"/> in the
    ''' bottom-left corner</summary>
    <System.ComponentModel.Description("Inside Bottom Left")> InsideBottomLeft

    ''' <summary>Locate the <see cref="Legend"/> inside the <see cref="Pane.AxisArea"/> in the
    ''' bottom-right corner</summary>
    <System.ComponentModel.Description("Inside Bottom Right")> InsideBottomRight

End Enum

''' <summary>Enumerates the available shapes</summary>
'''   <seealso cref="isr.Drawing.Symbol.Filled"/>
Public Enum ShapeType

    ''' <summary>Square-shaped <see cref="isr.Drawing.Symbol"/></summary>
    <System.ComponentModel.Description("Square")> Square

    ''' <summary>Rhombus-shaped <see cref="isr.Drawing.Symbol"/></summary>
    <System.ComponentModel.Description("Diamond")> Diamond

    ''' <summary>Equilateral triangle <see cref="isr.Drawing.Symbol"/></summary>
    <System.ComponentModel.Description("Triangle")> Triangle

    ''' <summary>Uniform circle <see cref="isr.Drawing.Symbol"/></summary>
    <System.ComponentModel.Description("Circle")> Circle

    ''' <summary>"X" shaped <see cref="isr.Drawing.Symbol"/>.  This symbol cannot
    ''' be filled since it has no outline.</summary>
    <System.ComponentModel.Description("X Cross")> XCross

    ''' <summary>"+" shaped <see cref="isr.Drawing.Symbol"/>.  This symbol cannot
    ''' be filled since it has no outline.</summary>
    <System.ComponentModel.Description("Plus")> Plus

    ''' <summary>Asterisk-shaped <see cref="isr.Drawing.Symbol"/>.  This symbol
    ''' cannot be filled since it has no outline.</summary>
    <System.ComponentModel.Description("Star")> Star

    ''' <summary>Unilateral triangle <see cref="isr.Drawing.Symbol"/>, pointing
    ''' down.</summary>
    <System.ComponentModel.Description("Triangle Down")> TriangleDown

End Enum

''' <summary>Enumerates the vertical alignment options</summary>
Public Enum VerticalAlignment

    ''' <summary>Position item so that its top edge is aligned with the
    '''   specified location.</summary>
    <System.ComponentModel.Description("Top")> Top

    ''' <summary>Position item so that its center is aligned (vertically) with the
    ''' specified location.</summary>
    <System.ComponentModel.Description("Center")> Center

    ''' <summary>Position item so that its bottom edge is aligned with the
    '''   specified location.</summary>
    <System.ComponentModel.Description("Bottom")> Bottom

End Enum

#End Region

