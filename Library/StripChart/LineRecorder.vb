''' <summary>Draws a simple line strip chart.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="07/27/05" by="David" revision="1.0.2034.x">
''' Created
''' </history>
<Description("Line Recorder - Windows Forms Custom Control"),
 System.Drawing.ToolboxBitmap(GetType(isr.Drawing.LineRecorder))>
Public Class LineRecorder

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs the class initializing components
    ''' </summary>
    Public Sub New()

        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        ' onInitialize()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        Me._gridSize = 12
        Me._amplitude = 50
        Me._lowerLimit = 25
        Me._maximum = 100
        Me._refreshInterval = 1000
        Me._upperLimit = 75

        ' Add any initialization after the InitializeComponent() call
        SetStyle(System.Windows.Forms.ControlStyles.UserPaint Or
                 System.Windows.Forms.ControlStyles.AllPaintingInWmPaint Or
                 System.Windows.Forms.ControlStyles.DoubleBuffer Or
                 System.Windows.Forms.ControlStyles.CacheText Or
                 System.Windows.Forms.ControlStyles.Opaque Or
                 System.Windows.Forms.ControlStyles.ResizeRedraw, True)
        Me.UpdateStyles()

    End Sub

    ''' <summary>Cleans up managed components.</summary>
    ''' <remarks>Use this method to reclaim managed resources used by this class.</remarks>
    Private Sub onDisposeManagedResources()

        If Me._thread IsNot Nothing Then
            Me._thread.Abort()
        End If

    End Sub

#Region " UNUSED "
#If False Then
  ''' <summary>Cleans up unmanaged components.</summary>
  ''' <remarks>Use this method to reclaim unmanaged resources used by this class.</remarks>
  Private Sub onDisposeUnmanagedResources()
  End Sub

  ''' <summary>Initializes components that might be affected by resize and paint events.</summary>
  ''' <remarks>Called from the class constructor before initializing the form components 
  '''		for components that are affected by resize and paint events.</remarks>
  Private Sub onInitialize()
  End Sub

#End If
#End Region

#End Region

#Region " METHODS  and  PROPERTIES "

    Private _amplitude As Integer
    ''' <summary>The current amplitude to plot.</summary>
    <Category("Behavior"), DefaultValue(50), Description("The current amplitude to plot.")>
    Public Property Amplitude() As Integer
        Get
            Return Me._amplitude
        End Get
        Set(ByVal Value As Integer)
            If Value > maximum Then
                Me._amplitude = Me._maximum
            ElseIf Value < Me._minimum Then
                Me._amplitude = Me._minimum
            Else
                Me._amplitude = Value
            End If
        End Set
    End Property

    ''' <summary>Gets or sets the amplitudes stored.</summary>
    Private _amplitudes As New ArrayList

    ''' <summary>Returns the default size for this control</summary>
    Protected Overrides ReadOnly Property DefaultSize() As System.Drawing.Size
        Get
            Return New System.Drawing.Size(200, 100)
        End Get
    End Property

    ''' <summary>Gets or sets the vertical and horizontal step size in pixels.</summary>
    Private Shared _divisionSize As Integer = 2

    Private _gridSize As Integer
    ''' <summary>
    ''' The grid size in pixels.
    ''' </summary>
    <Category("Behavior"), DefaultValue(12), Description("The grid size in pixels.")>
    Public Property GridSize() As Integer
        Get
            Return Me._gridSize
        End Get
        Set(ByVal Value As Integer)
            If Value < 2 Then
                Value = LineRecorder._divisionSize
            End If
            Me._gridSize = Value
        End Set
    End Property

    Private _instanceName As String = "LineRecorder"
    ''' <summary>Gets or sets the instance name of the instance of this class.</summary>
    ''' <value><c>InstanceName</c> is a String property that can be read from (read only).</value>
    ''' <remarks>Use this property to identify the instance of the class.  If instance
    '''   name is not set, returns .ToString.</remarks>
    Public ReadOnly Property InstanceName() As String
        Get
            If String.IsNullOrWhiteSpace(Me._instanceName) Then
                Return Me._instanceName
            Else
                Return MyBase.ToString
            End If
        End Get
    End Property

    Private _lowerLimit As Integer
    ''' <summary>
    ''' The lower value of the normal range.
    ''' </summary>
    <Category("Behavior"), DefaultValue(25), Description("The lower value of the normal range.")>
    Public Property LowerLimit() As Integer
        Get
            Return Me._lowerLimit
        End Get
        Set(ByVal value As Integer)
            If value > upperLimit Then
                Me._lowerLimit = Me._upperLimit
            ElseIf value < Me._minimum Then
                Me._lowerLimit = Me._minimum
            Else
                Me._lowerLimit = value
            End If
            Me.Invalidate()
        End Set
    End Property

    Private _maximum As Integer
    ''' <summary>
    ''' The upper bound of the amplitude range.
    ''' </summary>
    <Category("Behavior"), DefaultValue(100), Description("The upper bound of the amplitude range.")>
    Public Property Maximum() As Integer
        Get
            Return Me._maximum
        End Get
        Set(ByVal value As Integer)
            If value < Me._upperLimit Then
                Me._maximum = Me._upperLimit
            Else
                Me._maximum = value
            End If
            Me.Invalidate()
        End Set
    End Property

    Private _minimum As Integer
    ''' <summary>
    ''' The lower bound of the amplitude range.
    ''' </summary>
    <Category("Behavior"), DefaultValue(0), Description("The lower bound of the amplitude range.")>
    Public Property Minimum() As Integer
        Get
            Return Me._minimum
        End Get
        Set(ByVal value As Integer)
            If value > lowerLimit Then
                Me._minimum = Me._lowerLimit
            Else
                Me._minimum = value
            End If
            Me.Invalidate()
        End Set
    End Property

    ''' <summary>Gets or sets the strip chart position within the grid box.</summary>
    Private _mover As Integer

    ''' <summary>Starts or resumes recording.</summary>
    Public Sub Play()

        If Me._thread Is Nothing OrElse (Me._thread.ThreadState = Threading.ThreadState.Aborted) Then
            Me._thread = New Thread(New ThreadStart(AddressOf processThread))
            Me._thread.Start()
        ElseIf (Me._thread.ThreadState = Threading.ThreadState.Suspended) Then
            Me._thread.Start()
        ElseIf (Me._thread.ThreadState = Threading.ThreadState.Stopped) Then
            Me._thread.Start()
        End If

    End Sub

    ''' <summary>Pauses the recorder.</summary>
    Public Sub Pause()

        If Me._thread IsNot Nothing AndAlso (Me._thread.ThreadState <> Threading.ThreadState.Suspended) Then
            Me._thread.Start()
        End If

    End Sub

    ''' <summary>Stops the recorder.</summary>
    Public Sub [Stop]()

        If Me._thread IsNot Nothing Then
            Me._thread.Abort()
        End If

    End Sub

    ''' <summary>Processes the data.</summary>
    Private Sub processThread()

        Do While (Me._thread.ThreadState = Threading.ThreadState.Running)

            If Me._mover >= Me._gridSize - LineRecorder._divisionSize Then
                Me._mover = 0
            Else
                Me._mover += LineRecorder._divisionSize
            End If

            If Me._amplitudes.Count < (Me.Width / LineRecorder._divisionSize) Then

                ' add a new amplitude
                Me._amplitudes.Add(Me._amplitude)

            Else

                ' shift the amplitudes
                For i As Integer = 0 To Me._amplitudes.Count - 2
                    Me._amplitudes(i) = Me._amplitudes(i + 1)
                Next
                Me._amplitudes(Me._amplitudes.Count - 1) = Me._amplitude

            End If

            ' redraw
            Me.Invalidate()

            ' wait before the next redraw.
            Thread.Sleep(Me._refreshInterval)

        Loop

    End Sub

    Private _refreshInterval As Integer
    ''' <summary>The refreshing interval of the recorder between 1 and 1000 milliseconds.</summary>
    <Category("Behavior"), DefaultValue(1000),
    Description("The refreshing interval of the recorder between 1 and 1000 milliseconds.")>
    Public Property RefreshInterval() As Integer
        Get
            Return Me._refreshInterval
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then
                value = 1
            ElseIf value > 1000 Then
                value = 1000
            End If
            Me._refreshInterval = value
        End Set
    End Property

    ''' <summary>Reference to the thread the runs the strip chart.</summary>
    Private _thread As Thread

    Private _upperLimit As Integer
    ''' <summary>
    ''' The upper value of the normal range.
    ''' </summary>
    <Category("Behavior"), DefaultValue(75), Description("The upper value of the normal range.")>
    Public Property UpperLimit() As Integer
        Get
            Return Me._upperLimit
        End Get
        Set(ByVal value As Integer)
            If value > maximum Then
                Me._upperLimit = Me._maximum
            ElseIf value < Me._lowerLimit Then
                Me._upperLimit = Me._lowerLimit
            Else
                Me._upperLimit = value
            End If
            Me.Invalidate()
        End Set
    End Property

    ''' <summary>
    ''' Hold True to highlight out of bounds values.
    ''' </summary>
    <Category("Behavior"), DefaultValue(False),
      Description("Hold True to highlight out of bounds values.")>
    Public Property UseLimits() As Boolean

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>Draw the chart and the grid.</summary>
    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)

        If e Is Nothing Then Return

        MyBase.OnPaint(e)

        Dim graphicsDevice As Graphics = e.Graphics

        ' draw the horizontal grid.
        Dim position As Integer = 0
        For i As Integer = 1 To Convert.ToInt32(Me.Height / Me._gridSize)
            position += Me._gridSize
            graphicsDevice.DrawLine(Pens.DarkGreen, 0, position, Me.Width, position)
        Next i

        ' draw the vertical grid.
        position = -_mover
        For i As Integer = 1 To Convert.ToInt32(Me.Width / Me._gridSize)
            position += Me._gridSize
            graphicsDevice.DrawLine(Pens.DarkGreen, position, 0, position, Me.Height)
        Next i

        Dim scale As Double = Me.Height / (Me._maximum - Me._minimum)
        Dim drawingPen As System.Drawing.Pen = Pens.Yellow
        Dim currentX As Integer = Me.Width - LineRecorder._divisionSize * Me._amplitudes.Count
        Dim previousX As Integer
        Dim currentY As Integer
        Dim previousY As Integer
        Dim currentAmplitude As Integer
        Dim previousAmplitude As Integer
        If Me._useLimits AndAlso (Me._maximum > upperLimit) AndAlso (Me._minimum < Me._lowerLimit) Then
            For i As Integer = 1 To Me._amplitudes.Count - 1
                previousX = currentX
                currentX += LineRecorder._divisionSize
                currentAmplitude = Convert.ToInt32(Me._amplitudes(i), Globalization.CultureInfo.CurrentCulture)
                previousAmplitude = Convert.ToInt32(Me._amplitudes(i - 1), Globalization.CultureInfo.CurrentCulture)
                currentY = Convert.ToInt32(scale * (Me._maximum - Convert.ToInt32(Me._amplitudes(i), Globalization.CultureInfo.CurrentCulture)))
                previousY = Convert.ToInt32(scale * (Me._maximum - Convert.ToInt32(Me._amplitudes(i - 1), Globalization.CultureInfo.CurrentCulture)))
                If currentAmplitude > upperLimit Or currentAmplitude < Me._lowerLimit Then
                    drawingPen = Pens.Red
                Else
                    drawingPen = Pens.Yellow
                End If
                graphicsDevice.DrawLine(drawingPen, currentX, currentY, previousX, previousY)
            Next i
        Else
            For i As Integer = 1 To Me._amplitudes.Count - 1
                currentAmplitude = Convert.ToInt32(Me._amplitudes(i), Globalization.CultureInfo.CurrentCulture)
                previousAmplitude = Convert.ToInt32(Me._amplitudes(i - 1), Globalization.CultureInfo.CurrentCulture)
                previousX = currentX
                currentX += LineRecorder._divisionSize
                currentY = Convert.ToInt32(scale * (Me._maximum - currentAmplitude), Globalization.CultureInfo.CurrentCulture)
                previousY = Convert.ToInt32(scale * (Me._maximum - previousAmplitude), Globalization.CultureInfo.CurrentCulture)
                graphicsDevice.DrawLine(drawingPen, currentX, currentY, previousX, previousY)
            Next i
        End If

        ControlPaint.DrawBorder3D(graphicsDevice, 0, 0, Width, Height, Border3DStyle.Sunken)

    End Sub

    Protected Overrides Sub OnPaintBackground(ByVal e As System.Windows.Forms.PaintEventArgs)
        If e IsNot Nothing Then
            MyBase.OnPaintBackground(e)

            If Me._useLimits AndAlso (Me._maximum > upperLimit) AndAlso (Me._minimum < Me._lowerLimit) Then
                Dim myColor As Color = Color.FromArgb(64, Color.White)
                Using myBrush As New SolidBrush(myColor)
                    Dim scale As Double = Me.Height / (Me.Maximum - Me.Minimum)
                    Dim range As New Rectangle(0, Convert.ToInt32(scale * (Me.Maximum - Me.UpperLimit)), Me.Width, Convert.ToInt32(scale * (Me.UpperLimit - Me.LowerLimit)))
                    Dim graphicsDevice As Graphics = e.Graphics
                    graphicsDevice.FillRectangle(myBrush, range)
                End Using
            End If
        End If

    End Sub

    Protected Overrides Sub OnResize(ByVal e As System.EventArgs)
        If e IsNot Nothing Then
            MyBase.OnResize(e)
            If Me._amplitudes.Count > (Me.Width / LineRecorder._divisionSize) Then
                Me._amplitudes.RemoveRange(0, Me._amplitudes.Count - Convert.ToInt32(Me.Width / LineRecorder._divisionSize))
            End If
        End If
    End Sub

#End Region

End Class
