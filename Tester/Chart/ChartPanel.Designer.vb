<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class ChartPanel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try

            If disposing Then

                ' Free managed resources when explicitly called
                If Me._graphicsBuffer IsNot Nothing Then
                    Me._graphicsBuffer.Dispose()
                    Me._graphicsBuffer = Nothing
                End If

                If Me._chartPane IsNot Nothing Then
                    Me._chartPane.Dispose()
                    Me._chartPane = Nothing
                End If

                If components IsNot Nothing Then
                    components.Dispose()
                End If

            End If

            ' Free shared unmanaged resources

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="ChartPanel")>
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        '
        'ChartPanel
        '
        Me.ClientSize = New System.Drawing.Size(584, 350)
        Me.Name = "ChartPanel"
        Me.Text = "ChartPanel"
        Me.ResumeLayout(False)

    End Sub
End Class
