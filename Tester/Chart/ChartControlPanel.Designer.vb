<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class ChartControlPanel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try

            If disposing Then

                ' Free managed resources when explicitly called
                'onDisposeManagedResources()

                If components IsNot Nothing Then
                    components.Dispose()
                End If

            End If

            ' Free shared unmanaged resources
            'onDisposeUnManagedResources()

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="ChartControlPanel")>
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Dim ChartPane1 As isr.Drawing.ChartPane = New isr.Drawing.ChartPane
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ChartControlPanel))
        Dim Frame1 As isr.Drawing.Frame = New isr.Drawing.Frame
        Me.ChartControl1 = New isr.Drawing.ChartControl
        Me.SuspendLayout()
        '
        'ChartControl1
        '
        ChartPane1.PaneArea = CType(resources.GetObject("ChartPane1.PaneArea"), System.Drawing.RectangleF)
        Frame1.FillColor = System.Drawing.Color.White
        Frame1.Filled = True
        Frame1.IsOutline = True
        Frame1.LineColor = System.Drawing.Color.Black
        Frame1.LineWidth = 1.0!
        Frame1.StatusMessage = ""
        Frame1.Visible = True
        ChartPane1.PaneFrame = Frame1
        ChartPane1.Visible = True
        Me.ChartControl1.ChartPane = ChartPane1
        Me.ChartControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ChartControl1.Location = New System.Drawing.Point(0, 0)
        Me.ChartControl1.Name = "ChartControl1"
        Me.ChartControl1.Size = New System.Drawing.Size(504, 358)
        Me.ChartControl1.TabIndex = 1
        '
        'ChartControlPanel
        '
        Me.ClientSize = New System.Drawing.Size(504, 358)
        Me.Controls.Add(Me.ChartControl1)
        Me.Name = "ChartControlPanel"
        Me.Text = "ChartControlPanel"
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents ChartControl1 As isr.Drawing.ChartControl
End Class
