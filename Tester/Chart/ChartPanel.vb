''' <summary> A panel for testing the chart control with run-time instantiation. </summary>
''' <remarks> Launch this form by calling its Show or ShowDialog method from its default instance. </remarks>
''' <license> (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="2/15/2014" by="David" revision=""> Documented. </history>
Public Class ChartPanel
    Inherits isr.Core.Diagnosis.UserFormBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Default constructor.
    ''' </summary>
    Public Sub New()

        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        CreateChart()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

#Region " UNUSED "
#If False Then
  ''' <summary>Cleans up managed components.</summary>
  ''' <remarks>Use this method to reclaim managed resources used by this class.</remarks>
  Private Sub onDisposeManagedResources()

  End Sub

  ''' <summary>Cleans up unmanaged components.</summary>
  ''' <remarks>Use this method to reclaim unmanaged resources used by this class.</remarks>
  Private Sub onDisposeUnmanagedResources()

  End Sub

#End If
#End Region

#End Region

#Region " PROPERTIES "

    ''' <summary>Returns true if an instance of the class was created and not disposed.</summary>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            Return My.Application.OpenForms.Count > 0 AndAlso
                My.Application.OpenForms.Item(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name) IsNot Nothing
        End Get
    End Property

#End Region

#Region " PROPERTIES "

    Private _graphicsBuffer As isr.Drawing.BufferedGraphics
    Private _chartPane As isr.Drawing.ChartPane

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to optionally cancel the closing of the form.
    ''' Because the form is not yet closed at this point, this is also the best 
    ''' place to serialize a form's visible properties, such as size and 
    ''' location. Finally, dispose of any form level objects especially those that
    ''' might needs access to the form and thus should not be terminated after the
    ''' form closed.
    ''' </remarks>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        My.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            ' terminate form-level objects
            ' Me.terminateObjects()
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            'Me.instantiateObjects()

            ' set the form caption
            Me.Text = My.Application.Info.BuildDefaultCaption(": CHART PANEL")

            ' set tool tips
            'initializeUserInterface()

            ' center the form
            Me.CenterToScreen()

            ' Me._graphicsBuffer.CreateDoubleBuffer(Me.CreateGraphics(), Me.ClientRectangle.Width, Me.ClientRectangle.Height)
            ' CreateChart()

            ' turn on the loaded flag
            '      loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>Occurs when the form is redrawn.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.Windows.Forms.PaintEventArgs"/></param>
    Private Sub Form_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint

        If Me._graphicsBuffer.CanDoubleBuffer() Then

            ' Fill in Background (for efficiency only the area that has been clipped)
            Using sb As New SolidBrush(SystemColors.Window)
                Me._graphicsBuffer.GraphicsDevice.FillRectangle(sb,
                                                             e.ClipRectangle.X, e.ClipRectangle.Y,
                                                             e.ClipRectangle.Width, e.ClipRectangle.Height)
            End Using


            Using sb As New SolidBrush(Color.Gray)
                ' clear the client area
                Me._graphicsBuffer.GraphicsDevice.FillRectangle(sb, Me.ClientRectangle)
            End Using

            ' Do our drawing using Me._graphicsBuffer.g instead e.Graphics
            Me._chartPane.Draw(Me._graphicsBuffer.GraphicsDevice)

            ' Render to the form
            Me._graphicsBuffer.Render(e.Graphics)

        Else
            ' if double buffer is not available, draw to e.Graphics

            ' clear
            Using sb As New SolidBrush(Color.Gray)
                e.Graphics.FillRectangle(sb, Me.ClientRectangle)
            End Using

            ' draw
            Me._chartPane.Draw(e.Graphics)

        End If

    End Sub

    ''' <summary>Paints the background of the control.  The OnPaintBackground method allows 
    '''   derived classes to handle the event without attaching a delegate. This is the
    '''   preferred technique for handling the event in a derived class.  Inheriting classes 
    '''   should override this method to handle the erase background request from windows. 
    '''   When overriding OnPaintBackground in a derived class it is not necessary to call 
    '''   the base class's OnPaintBackground method.  By overriding this method, the 
    '''   paint event of the parent class is disabled and handled by the double buffering 
    '''   method, which smooths the refreshing of the chart.</summary>
    Protected Overrides Sub OnPaintBackground(ByVal pevent As PaintEventArgs)
    End Sub 'OnPaintBackground

    ''' <summary>Occurs when the form is resized</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    Private Sub Form1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        Me._graphicsBuffer.CreateDoubleBuffer(Me.ClientRectangle.Width, Me.ClientRectangle.Height)
        Me._chartPane.SetSize(Me.ClientRectangle)
        Invalidate()
    End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

#Region " UNUSED "
#If False Then
  ''' <summary>Initializes the class objects.</summary>
  ''' <exception cref="isr.Drawing.BaseException" guarantee="strong">
  '''   failed instantiating objects.</exception>
  ''' <remarks>Called from the form load method to instantiate 
  '''   module-level objects.</remarks>
  Private Sub instantiateObjects()

  End Sub

  ''' <summary>Initializes the user interface and tool tips.</summary>
  ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
  Private Sub initializeUserInterface()
    '    tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
    '    tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")
  End Sub

  ''' <summary>Terminates and disposes of class-level objects.</summary>
  ''' <remarks>Called from the form Closing method.</remarks>
  Private Sub terminateObjects()

  End Sub

#End If
#End Region

    Private Sub CreateChart()

        Me._graphicsBuffer = New isr.Drawing.BufferedGraphics(Me.ClientRectangle.Width, Me.ClientRectangle.Height)
        Me._chartPane = New isr.Drawing.ChartPane
        Me._chartPane.CreateSampleOne(Me.ClientRectangle)

    End Sub

#End Region

End Class