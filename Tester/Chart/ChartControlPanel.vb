''' <summary> A chart control panel. </summary>
''' <remarks> Launch this form by calling its Show or ShowDialog method from its default instance. </remarks>
''' <license> (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="2/15/2014" by="David" revision=""> Documented. </history>
Public Class ChartControlPanel
    Inherits isr.Core.Diagnosis.UserFormBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ChartControlPanel" /> class.
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        ' instantiate a main context menu
        Dim mainContextMenu As ContextMenu
        mainContextMenu = New ContextMenu
        mainContextMenu.MenuItems.Add("Print", AddressOf OnPrintMenuItem)
        Me.ContextMenu = mainContextMenu

    End Sub

#Region " UNUSED "
#If False Then
  ''' <summary>Cleans up managed components.</summary>
  ''' <remarks>Use this method to reclaim managed resources used by this class.</remarks>
  Private Sub onDisposeManagedResources()

  End Sub

  ''' <summary>Cleans up unmanaged components.</summary>
  ''' <remarks>Use this method to reclaim unmanaged resources used by this class.</remarks>
  Private Sub onDisposeUnmanagedResources()

  End Sub

#End If
#End Region

#End Region

#Region " PROPERTIES "

    ''' <summary>Returns true if an instance of the class was created and not disposed.</summary>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            Return My.Application.OpenForms.Count > 0 AndAlso
                My.Application.OpenForms.Item(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name) IsNot Nothing
        End Get
    End Property

#Region " UNUSED "

#If False Then
  ''' <summary>Gets or sets the status message.</summary>
  ''' <value>A <see cref="System.String">String</see>.</value>
  ''' <remarks>Use this property to get the status message generated by the object.</remarks>
  Public ReadOnly Property StatusMessage() As String
    Get
      Return Me._statusMessage
    End Get
  End Property
#End If
#End Region

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to optionally cancel the closing of the form.
    ''' Because the form is not yet closed at this point, this is also the best 
    ''' place to serialize a form's visible properties, such as size and 
    ''' location. Finally, dispose of any form level objects especially those that
    ''' might needs access to the form and thus should not be terminated after the
    ''' form closed.
    ''' </remarks>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        My.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            ' terminate form-level objects
            ' Me.terminateObjects()
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            'Me.instantiateObjects()

            ' set the form caption
            Me.Text = My.Application.Info.BuildDefaultCaption(": CHART CONTROL PANEL")

            ' set tool tips
            'initializeUserInterface()

            ' center the form
            Me.CenterToScreen()

            ' Create the chart
            Me.ChartControl1.ChartPane.CreateSampleOne(Me.ChartControl1.ClientRectangle)

            ' turn on the loaded flag
            '      loaded = true

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

#Region " UNUSED "
#If False Then
  ''' <summary>Initializes the class objects.</summary>
  ''' <exception cref="isr.Drawing.BaseException" guarantee="strong">
  '''   failed instantiating objects.</exception>
  ''' <remarks>Called from the form load method to instantiate 
  '''   module-level objects.</remarks>
  Private Sub instantiateObjects()

  End Sub

  ''' <summary>Initializes the user interface and tool tips.</summary>
  ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
  Private Sub initializeUserInterface()
    '    tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
    '    tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")
  End Sub

  ''' <summary>Terminates and disposes of class-level objects.</summary>
  ''' <remarks>Called from the form Closing method.</remarks>
  Private Sub terminateObjects()

  End Sub

#End If
#End Region

#End Region

#Region " PRINTING "

    ' the print window area includes all the drawing objects.
    Private _printWindow As RectangleF = New RectangleF(100, 200, 650, 400)

    ' declare the print document object to handle the printing
    Private printDoc As System.Drawing.Printing.PrintDocument

    ''' <summary>Handles the print menu item delegate.</summary>
    ''' <param name="e">Reference to a <see cref="System.EventArgs"></see></param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub OnPrintMenuItem(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Me.printChart()
        Catch ex As Exception
            MessageBox.Show(String.Format(Globalization.CultureInfo.CurrentCulture,
                                          "{0} failed to print. Details: {1}.", Me.Name, ex), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Exclamation,
                                      MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary>Prints the chart.</summary>
    ''' <remarks>Use this method to print the strip chart.</remarks>
    Private Sub printChart()

        ' instantiate the print document object to handle the printing
        printDoc = New System.Drawing.Printing.PrintDocument

        ' add handler to handle printing
        AddHandler printDoc.PrintPage, AddressOf Me.printChartHandler

        ' set the cursor to wait
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        ' do the printing
        printDoc.Print()

    End Sub

    ''' <summary>Prints the chart header.</summary>
    ''' <param name="g">
    '''   specifies the graphics context for 
    '''     printing.</param>
    ''' <param name="headerHeight">
    '''     is a Single value returning the height of the header.</param>
    ''' <remarks>Prints the chart header.</remarks>
    Private Sub printChartHeader(ByVal g As Graphics, ByRef headerHeight As Single)

        Using dateFont As New Font("Ariel", 10, FontStyle.Regular)

            Dim chartDate As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                    "{0} {1}", Date.Now.ToLongDateString, Date.Now.ToLongTimeString)
            Dim dateSize As SizeF = g.MeasureString(chartDate, dateFont)

            'Dim headerFont As New Font("Ariel", 14, FontStyle.Bold)
            'Dim chartTitle As String = "Chart One"
            ' Dim titleSize As SizeF = g.MeasureString(chartTitle, headerFont)

            ' set text rendering to anti-alias
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias

            With printDoc.DefaultPageSettings

                If .Landscape Then
                    ' We cannot handle landscape printing at this time!  This requires
                    ' rotating the printing not just assuming that the printer already done
                    ' that.
                    ' print the chart date and time at the top right
                    g.DrawString(chartDate, dateFont, Brushes.Black,
                                 Me._printWindow.Right - dateSize.Width, Me._printWindow.Top - dateSize.Height)

                    headerHeight = dateSize.Height

                Else
                    ' print the chart date and time at the top right
                    g.DrawString(chartDate, dateFont, Brushes.Black,
                                 Me._printWindow.Right - dateSize.Width, Me._printWindow.Top - dateSize.Height)
                    headerHeight = dateSize.Height

                End If

            End With
        End Using

    End Sub

    ''' <summary>Prints the chart.</summary>
    ''' <param name="sender">
    '''   specifies the printing object sending 
    '''     the event message.</param>
    ''' <param name="e">
    '''   specifies an instance of the 
    '''     PrintPageEventArgs.</param>
    ''' <remarks>Serves to handle the PrintPage event of the Print 
    '''   document.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub printChartHandler(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
        Try
            OnPrintChart(e)
        Catch ex As Exception
            ' throw an exception
            MessageBox.Show(String.Format(Globalization.CultureInfo.CurrentCulture,
                                          "{0} failed to print. Details: {1}", Me.Name, ex), "Exception", MessageBoxButtons.OK,
                                      MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary>Prints the chart.</summary>
    ''' <param name="e">
    '''   specifies an instance of the 
    '''     PrintPageEventArgs.</param>
    ''' <remarks>Serves to handle the PrintPage event of the Print 
    '''   document.</remarks>
    Protected Overridable Sub OnPrintChart(ByVal e As System.Drawing.Printing.PrintPageEventArgs)

        If e Is Nothing Then Return

        Dim headerHeight As Single

        ' print the chart header
        printChartHeader(e.Graphics, headerHeight)

        ' print the chart
        Me.ChartControl1.ChartPane.Print(e.Graphics, Me._printWindow)

        ' specify that this is the last page to print
        e.HasMorePages = False

        ' restore the mouse cursor
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

#End Region

End Class