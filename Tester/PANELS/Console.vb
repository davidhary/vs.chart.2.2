Imports System
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Drawing

''' <summary> Test console. </summary>
''' <license> (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="2/15/2014" by="David" revision=""> Documented. </history>
Public Class Console
    Inherits isr.Core.Diagnosis.UserFormBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    '/ <summary>
    '/ 
    '/ </summary>
    Public Sub New()
        '
        ' Required for Windows Form Designer support
        createChart()
        InitializeComponent()
        '         Me._graphicsBuffer = New BufferedGraphics()
    End Sub 'New

#End Region

    Private _graphicsBuffer As BufferedGraphics

    Private _chartPane As ChartPane

    Private Sub createChart()

        Me._graphicsBuffer = New isr.Drawing.BufferedGraphics(Me.ClientRectangle.Width, Me.ClientRectangle.Height)
        Me._graphicsBuffer.CreateDoubleBuffer(Me.ClientRectangle.Width, Me.ClientRectangle.Height)
        Me._chartPane = New isr.Drawing.ChartPane
        Me._chartPane.CreateSampleOne(Me.ClientRectangle)

    End Sub

    Protected Overrides Sub OnPaintBackground(ByVal pevent As PaintEventArgs)
    End Sub 'OnPaintBackground

    Private Sub Form1_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint

        If Me._graphicsBuffer.CanDoubleBuffer() Then

            ' Fill in Background (for efficiency only the area that has been clipped)
            Using SB As New SolidBrush(SystemColors.Window)
                Me._graphicsBuffer.GraphicsDevice.FillRectangle(SB, e.ClipRectangle.X, e.ClipRectangle.Y, e.ClipRectangle.Width, e.ClipRectangle.Height)
            End Using

            ' clear the client area
            Using SB As New SolidBrush(SystemColors.Window)
                Me._graphicsBuffer.GraphicsDevice.FillRectangle(SB, Me.ClientRectangle)
            End Using

            ' Drawing using Me._graphicsBuffer.g 
            Me._chartPane.Draw(Me._graphicsBuffer.GraphicsDevice)

            ' Render to the form
            Me._graphicsBuffer.Render(e.Graphics)

            ' if double buffer is not available, do without it
        Else
            ' Drawing using e.Graphics

            ' clear
            Using SB As New SolidBrush(Color.Gray)
                e.Graphics.FillRectangle(SB, Me.ClientRectangle)
            End Using

            ' draw
            Me._chartPane.Draw(e.Graphics)
        End If

    End Sub 'Form1_Paint

    Private Sub Form1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        Me._graphicsBuffer.CreateDoubleBuffer(Me.ClientRectangle.Width, Me.ClientRectangle.Height)
        SetSize()
        Invalidate()
    End Sub 'Form1_Resize

    Private Sub SetSize()
        Dim PaneArea As RectangleF = RectangleF.op_Implicit(Me.ClientRectangle)
        PaneArea.Inflate(-10, -10)
        Me._chartPane.PaneArea = PaneArea
    End Sub 'SetSize

End Class