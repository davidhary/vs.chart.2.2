<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class LineRecorderPanel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try

            If disposing Then

                ' Free managed resources when explicitly called

                If components IsNot Nothing Then
                    components.Dispose()
                End If

            End If

            ' Free shared unmanaged resources

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me._LineRecorder = New isr.Drawing.LineRecorder
        Me._SpeedBar = New System.Windows.Forms.TrackBar
        Me._AmplitudeBar = New System.Windows.Forms.TrackBar
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._Layout = New System.Windows.Forms.TableLayoutPanel
        Me._IntervalNumericUpDown = New System.Windows.Forms.NumericUpDown
        CType(Me._SpeedBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._AmplitudeBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._Layout.SuspendLayout()
        CType(Me._IntervalNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_LineRecorder
        '
        Me._LineRecorder.BackColor = System.Drawing.SystemColors.WindowText
        Me._LineRecorder.Dock = System.Windows.Forms.DockStyle.Fill
        Me._LineRecorder.Location = New System.Drawing.Point(61, 10)
        Me._LineRecorder.Margin = New System.Windows.Forms.Padding(10)
        Me._LineRecorder.Name = "_LineRecorder"
        Me._LineRecorder.Size = New System.Drawing.Size(677, 302)
        Me._LineRecorder.TabIndex = 3
        '
        '_SpeedBar
        '
        Me._SpeedBar.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._SpeedBar.Location = New System.Drawing.Point(54, 325)
        Me._SpeedBar.Maximum = 1000
        Me._SpeedBar.Minimum = 10
        Me._SpeedBar.Name = "_SpeedBar"
        Me._SpeedBar.Size = New System.Drawing.Size(691, 45)
        Me._SpeedBar.TabIndex = 5
        Me._SpeedBar.TickFrequency = 100
        Me._SpeedBar.Value = 1000
        '
        '_AmplitudeBar
        '
        Me._AmplitudeBar.Dock = System.Windows.Forms.DockStyle.Left
        Me._AmplitudeBar.Location = New System.Drawing.Point(3, 3)
        Me._AmplitudeBar.Maximum = 100
        Me._AmplitudeBar.Name = "_AmplitudeBar"
        Me._AmplitudeBar.Orientation = System.Windows.Forms.Orientation.Vertical
        Me._AmplitudeBar.Size = New System.Drawing.Size(45, 316)
        Me._AmplitudeBar.TabIndex = 4
        Me._AmplitudeBar.TickFrequency = 10
        Me._AmplitudeBar.Value = 50
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 2
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.Controls.Add(Me._LineRecorder, 1, 0)
        Me._Layout.Controls.Add(Me._SpeedBar, 1, 1)
        Me._Layout.Controls.Add(Me._AmplitudeBar, 0, 0)
        Me._Layout.Controls.Add(Me._IntervalNumericUpDown, 0, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(0, 0)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 2
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._Layout.Size = New System.Drawing.Size(748, 373)
        Me._Layout.TabIndex = 6
        '
        '_IntervalNumericUpDown
        '
        Me._IntervalNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.None
        Me._IntervalNumericUpDown.Font = New Font(Me.Font, FontStyle.Bold)
        Me._IntervalNumericUpDown.Increment = New Decimal(New Integer() {10, 0, 0, 0})
        Me._IntervalNumericUpDown.Location = New System.Drawing.Point(3, 337)
        Me._IntervalNumericUpDown.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me._IntervalNumericUpDown.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._IntervalNumericUpDown.Name = "_IntervalNumericUpDown"
        Me._IntervalNumericUpDown.Size = New System.Drawing.Size(45, 20)
        Me._IntervalNumericUpDown.TabIndex = 6
        Me._IntervalNumericUpDown.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        '
        'LineRecorderPanel
        '
        Me.ClientSize = New System.Drawing.Size(748, 373)
        Me.Controls.Add(Me._Layout)
        Me.Name = "LineRecorderPanel"
        Me.Text = "Line Recorder Test Panel"
        CType(Me._SpeedBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._AmplitudeBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me._Layout.ResumeLayout(False)
        Me._Layout.PerformLayout()
        CType(Me._IntervalNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _LineRecorder As isr.Drawing.LineRecorder
    Private WithEvents _SpeedBar As System.Windows.Forms.TrackBar
    Private WithEvents _AmplitudeBar As System.Windows.Forms.TrackBar
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _Layout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _IntervalNumericUpDown As System.Windows.Forms.NumericUpDown
End Class
