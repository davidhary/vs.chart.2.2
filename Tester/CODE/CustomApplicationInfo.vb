﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.Diagnosis.TraceEventIds.IsrVisualizingChartTester

        Public Const AssemblyTitle As String = "Chart User Control Tester"
        Public Const AssemblyDescription As String = "Chart User Control Tester"
        Public Const AssemblyProduct As String = "Drawing.Chart.User.Control.Tester.2014"

    End Class

End Namespace

